<?php

class LgxFrameworkOpt{

    // logo
    public function emeet_logo(){
        global $emeet_options;
        if((isset($emeet_options['logo-up']['url'])) && !empty($emeet_options['logo-up']['url'])){
            return $emeet_options['logo-up']['url'];
        }else{
            return get_template_directory_uri()."/assets/img/logo.png";
        }
    }

    // Page Banner image 
    function emeet_page_banner(){
        global $emeet_options;

        if(!empty($emeet_options['lgx_page_banner_img']['url'])){
            return $emeet_options['lgx_page_banner_img']['url'];
        }else{
            return get_template_directory_uri()."/assets/img/inner-banner-bg.jpg";
        }
    }

    // Footer Banner image
    function emeet_footer_bg_img(){
        global $emeet_options;

        if(!empty($emeet_options['lgx_footer_bg_img']['url'])){
            return $emeet_options['lgx_footer_bg_img']['url'];
        }else{
            return get_template_directory_uri()."/assets/img/footer-bg.jpg";
        }
    }



    function emeet_pre_loader_img(){
        global $emeet_options;

        if(!empty($emeet_options['pre_loader_img']['url'])){
            return $emeet_options['pre_loader_img']['url'];
        }else{
            return get_template_directory_uri()."/assets/img/pre-loader.gif";
        }
    }


    // blog sidebar options
    public function emeet_blogSidebar(){
        global $emeet_options;
        if(isset($emeet_options['blog-sidebar']) ){
            return $emeet_options['blog-sidebar'];
        }else{
            return "right";
        }

    }
    // archive sidebar options
    public function emeet_archiveSidebar(){
        global $emeet_options;
        if(isset($emeet_options['archv-sidebar']) ){
            return $emeet_options['archv-sidebar'];
        }else{
            return "right";
        }

    }
    // archive sidebar options
    public function emeet_shopSidebar(){
        global $emeet_options;
        if(isset($emeet_options['shop-sidebar']) ){
            return $emeet_options['shop-sidebar'];
        }else{
            return "full";
        }

    }


    // search sidebar options
    public function emeet_searchSidebar(){
        global $emeet_options;
        if(isset($emeet_options['srch-sidebar']) ){
            return $emeet_options['srch-sidebar'];
        }else{
            return "right";
        }
    }

// footer


    // footer title
    public function emeet_footer_title(){
        global $emeet_options;
        if(isset($emeet_options['footer-title']) ){
            return $emeet_options['footer-title'];
        }else{
            return "";
        }
    }




    // mailchimp shortcode
    public function emeet_footer_subscribe(){
        global $emeet_options;
        if(isset($emeet_options['mailchimp-text']) ){
            return $emeet_options['mailchimp-text'];
        }else{
            return "";
        }
    }

    // Get
    public function emeet_enable_status($type) {
        global $emeet_options;

        $output = false;
        if( !empty($emeet_options[$type]) && $emeet_options[$type] == 1) {
            $output = true;
        }
        return $output;
    }


}



/**=====================================================================
 * Social icon
=====================================================================*/

function emeet_social_profile ($icon) {
    global $emeet_options;

    $output = '';
    if(!empty($emeet_options[$icon])) {
        $output = '<li><a href="'.esc_url($emeet_options[$icon]).'" target="_blank" ><i class="fa fa-'.esc_attr($icon).'" aria-hidden="true"></i></a></li>';
    }

    return $output;
}



// footer logo
function emeet_footer_logo_url(){
    global $emeet_options;

    if(!empty($emeet_options['footer-logo']['url'])){
        return $emeet_options['footer-logo']['url'];
    }else{
        return get_template_directory_uri()."/assets/img/logo.png";
    }
}

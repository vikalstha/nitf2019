<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}


add_shortcode( 'vcx_header_classic', 'vcx_header_classic_function');

/**
 *  Title Section Short Code
 * @param $atts
 * @return string
 */

function vcx_header_classic_function($atts)
{
    extract(shortcode_atts(array(
        'banner_align'         => 'left',
        'banner_height'        => 'df',
        'vcx_pre_title'        => 'Learn Anything',
        'vcx_title'            => 'Conference Meet {2}{0}{1}{9}',
        'vcx_title_before'            => '',
        'vcx_title_after'            => '',
        'vcx_title_sec'            => 'Conference Meet {2}{0}{1}{9}',
        'vcx_post_title'       => '21 King Street, Duach, United State.',
        'vcx_post_title_icon'  => 'fa-map-marker',
        'vcx_post_title2'       => '23-27 September, 2018.',
        'vcx_post_title_icon2'  => 'fa-calendar',
        'count_type'           => 'none',
        'vcx_count_date'       => '2019/12/15 0:0:0',
        'vcx_text_day'         => 'Days',
        'vcx_text_hour'        => 'Hours',
        'vcx_text_min'         => 'Minutes',
        'vcx_text_sec'         => 'Seconds',
        'vcx_typed_en'         => '',
        'header_video_id'      => 'oSPR5Go05Vg',
        'vcx_video_text'       => ' Watch Promo Video',
        'vcx_btn_text1'        => 'Register',
        'vcx_btn_url1'         => '',
        'vcx_btn_text2'        => 'Contact',
        'vcx_btn_url2'         => '',
        'count_position'       => 'top',
        'vcx_particle_en'      => '',
        'cir_bg_color'        => '#dddddd',
        'day_color'           => '#ec398b',
        'hour_color'          => '#fac400',
        'min_color'           => '#00acee',
        'sec_color'           => '#483fa1',
        'cd_text_color'       => '#ffffff',
        'count_align'         => 'left',
        'count_style'         => 'default',
        'content_width'       => 'fixed',
        'parallax_img_1'      => '',
        'parallax_img_2'      => '',
        'parallax_img_3'      => '',
        'parallax_title'      => '',
        'content_color_type'  => 'default',


    ), $atts));


    $parallax_img_dir = plugins_url() .'/vcx-theme-core/assets/img/parallax';




    $layer_1 = '';

    if(!empty ($atts['parallax_img_1'])) {
        $img_1 = wp_get_attachment_image_src($atts['parallax_img_1'], 'full');
        $image_url_1 = $img_1[0];

        $layer_1 = '<div id="object1"   class="lgx-parallax-1 bglayer1 hidden-sm hidden-xs">
                        <img src="'.$image_url_1.'" alt="'.esc_html__('Parallax Image', 'vcx-theme-core').'">
                 </div>';
    }




    $layer_2 = '';
    if(!empty ($atts['parallax_img_2'])) {
        $img_2 = wp_get_attachment_image_src($atts['parallax_img_2'], 'full');
        $image_url_2 = $img_2[0];

        $layer_2 = '<div id="object5" class="lgx-parallax-2 rightlayer1 hidden-sm hidden-xs">
                        <img src="'.$image_url_2.'" alt="'.esc_html__('Parallax Image', 'vcx-theme-core').'">
                </div>';
    }


    $layer_3 = '';
    if(!empty ($atts['parallax_img_3'])) {
        $img_3 = wp_get_attachment_image_src($atts['parallax_img_3'], 'full');
        $image_url_3 = $img_3[0];

        $layer_3 = '<div id="object2" class="lgx-parallax-3 bglayer2 hidden-sm hidden-xs">
                       <img src="'.$image_url_3.'" alt="'.esc_html__('Parallax Image', 'vcx-theme-core').'">
                </div>';
    }


    $layer_4 = '';
    if(!empty ($parallax_title)) {

        $layer_4 = ' <div id="object6" class="lgx-parallax-4 rightlayer2shade">
                            <h2 class="lgx-parallax-text">'. vcx_spilt_title($parallax_title) .'</h2>
                        </div>';
    }



    //$vcx_typed_en = false;
    $vcx_title_view = ($vcx_typed_en != 'yes') ? $vcx_title : '';
    $typed_data 	= ($vcx_typed_en == 'yes') ? '<span id="lgx-typed-string" data-title="'.$vcx_title.'"> </span>' : '';





    // Content
    $pre_title_html =(!empty ($vcx_pre_title)) ? '<h3 class="subtitle">'.esc_html($vcx_pre_title).'</h3>' : '' ;

    $title_html =(!empty ($vcx_title)) ? '<h2 class="title varieties-title">'.vcx_spilt_title($vcx_title_before) .$typed_data. ' '.vcx_spilt_title($vcx_title_view) . vcx_spilt_title($vcx_title_after).'</h2>' : '' ;


    $title_html_sec =(!empty ($vcx_title_sec)) ? '<h2 class="sm-title"> '.vcx_spilt_title($vcx_title_sec).'</h2>' : '' ;


    $location_icon = !empty($vcx_post_title_icon) ? '<i class="fa '.esc_attr($vcx_post_title_icon).'"></i>' : '';
    $location_html = !empty($vcx_post_title) ? '<h3 class="location">'.$location_icon .' '. esc_html($vcx_post_title) .'</h3>' : '';

    $date_icon = !empty($vcx_post_title_icon2) ? '<i class="fa '.esc_attr($vcx_post_title_icon2).'"></i>' : '';
    $date_html = !empty($vcx_post_title2) ? '<h3 class="location">'.$date_icon .' '. esc_html($vcx_post_title2) .'</h3>' : '';



    // CountDown
    $simple_countdown = '<div id="lgx-countdowntop" class="lgx-countdown lgx-countdowntop" ></div>';

    $circuler_coundown = '<div id="circular-countdown" class="vcx-circular-countdown"  data-date="'. esc_html($vcx_count_date) .'"  style="color: '.esc_html($cd_text_color).';" ></div>';



    $countdown_html = '';

    if( !empty($count_type)) {

        switch ($count_type) {

            case "circular":
                $count_type_html = $circuler_coundown;
                break;

            case "simple":
                $count_type_html = $simple_countdown;
                break;

            case "none":
                $count_type_html = '';
                break;

            default:
                $count_type_html = $simple_countdown;
        }

        $countdown_html = ($count_type != 'none') ? '<div id="vcx-header-countdown" class="lgx-countdown-area circular-countdown-area vcx-header-countdown banner-countdown-align-'.$count_align.' banner-countdown-style-'.$count_style.'" 
        data-date="'. esc_html($vcx_count_date) .'"
        data-vday="'.esc_html($vcx_text_day).'"
        data-vhour="'.esc_html($vcx_text_hour).'" 
        data-vmin="'.esc_html($vcx_text_min).'"
        data-vsec="'.esc_html($vcx_text_sec).'"  
        data-dcolor="'.esc_attr($day_color).'"  
        data-hcolor="'.esc_attr($hour_color).'"  
        data-mcolor="'.esc_attr($min_color).'"  
        data-scolor="'.esc_attr($sec_color).'"  
        data-cbgcolor="'.esc_attr($cir_bg_color).'"  
        data-textcolor="'.esc_attr($cd_text_color).'"  
        >'.$count_type_html.'</div>' : '';
    }


    $particle__bg_data = '<div id="lgx-particles-background" class="lgx-particles-background vcx-particles-background"
                    data-particles-number-value="80" 
                     data-particles-density-value="800"
                     data-particles-shape-type="Circle"
                     data-particles-color="#ffffff"
                     data-particles-shape-stroke-color="#000000"
                     data-particles-shape-stroke-width="0"
                     data-particles-size-value="5"                     
                     data-particles-size-random="true"
                     data-particles-size-anim-enable="false"
                     data-particles-opacity-value="0.5" 
                     data-particles-opacity-random="true"
                     data-particles-opacity-anim-enable="false" 
                     data-particles-move-enabled="true" 
                     data-particles-move-direction="none" 
                     data-particles-move-random="false" 
                     data-particles-move-straight="false" 
                     data-particles-move-speed="6" 
                     data-particles-move-out-mode="bounce" 
                     data-particles-line-linked-enable-auto="true"
                     data-particles-line-linked-distance="150" 
                     data-particles-line-linked-color="#ffffff" 
                     data-particles-line-linked-opacity="0.40"
                     data-particles-line-linked-width="1"
                     data-particles-interactivity-onhover-enable="true" 
                     data-particles-interactivity-onhover-mode="repulse"
                     data-particles-interactivity-modes-repulse-distance="180"                      
                     data-particles-compatibility-customclass=".lgx_vcrow, .vc_row, .wpb_row"
                     data-particles-compatibility-zindex="2"
                     data-particles-shape-image-src=""
                     data-particles-shape-image-width=""
                     data-particles-shape-image-height="">
                </div>';

    //Show Particle Effect
    $vcx_particle_html  = ($vcx_particle_en == 'yes') ? $particle__bg_data : '';



    //Button
    $button_html1= (!empty($vcx_btn_url1)) ? '<a class="lgx-btn" href="'.esc_url($vcx_btn_url1).'"><span>'.esc_html($vcx_btn_text1).'</span></a>' : '';
    $button_html2= (!empty($vcx_btn_url2)) ? '<a class="lgx-btn lgx-btn-red" href="'.esc_url($vcx_btn_url2).'"><span>'.esc_html($vcx_btn_text2).'</span></a>' : '';




    $video_btn_html = ( !empty( $header_video_id)) ? '<div class="lgx-videoicon-area">
                            <a href="#" class="ripple-block" id="myModalLabel" data-toggle="modal" data-target="#lgx-modal">
                                <i class="fa fa-play" aria-hidden="true"></i>
                                <div class="ripple ripple-1"></div>
                                <div class="ripple ripple-2"></div>
                                <div class="ripple ripple-3"></div>
                            </a><span class="header-video-text">'.esc_html($vcx_video_text).'</span>
                        </div>' : '';

    $video_modal_html= '<div id="lgx-modal" class="modal fade lgx-modal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </div>
                                <div class="modal-body">
                                    <iframe id="modalvideo" src="https://www.youtube.com/embed/'.$header_video_id.'" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div> ';


    $output = '<div id="lgx-parallax-banner"  class="lgx-banner lgx-banner-parallax lgx-header-color-'.$content_color_type.'">
                        
                        '.$vcx_particle_html.'
        
                   <div class="lgx-section">        
          
                        <div id="layer-wrapper" class="lgx-item-parallax-banner">                                               
                        '.$layer_1.'
                        '.$layer_2.'
                        '.$layer_3.'
                        '.$layer_4.'
                       
                        <div class="banner-content"  >
                            <div class="lgx-hover-link">
                            <div class="lgx-vertical">
                            
                               <div class="lgx-container-'.$content_width.'">              
                                     <div class=" lgx-header-inner-'.esc_attr($banner_height).'">
                                       <div class="lgx-banner-info  lgx-banner-info-'.esc_attr($banner_align).' ">     
                                                                                    
                                    '. ( ($count_position != 'bottom') ? $countdown_html : '' ).'                                             
                                    '.$pre_title_html.'                                        
                                    '.$title_html.'                                        
                                    '.$title_html_sec.'  
                                    <div class="location-area">                                    
                                    '.$location_html.'      
                                    '.$date_html.'      
                                    </div>  
                                    <div class="action-area vcx-action-area">                 
                                         <ul class="list-inline vcx-action-list">
                                            <li>'. $button_html1 . '</li>
                                            <li>'. $button_html2 . '</li>
                                            <li>'. $video_btn_html . '</li>
                                        </ul>                                    
                                        '.(!empty( $header_video_id) ? $video_modal_html : '').'
                            </div> <!-- // .ACTION -->
                            
                            '. ( ($count_position == 'bottom') ? $countdown_html : '' ).' 
                          
                          
                          </div> <!--//  banner infoo -->
                            </div><!-- // .INNER -->    
                          
                          
                           </div> <!-- // .container -->
                           
                           </div><!--link-->
                            </div><!--lgx-vertical--> 
                            </div> <!-- // .WRAPPER content -->                          
                                               
                    
                      </div> <!-- // layer -->                      
                                           
                         
                   </div>
               
                </div>';

    return $output;
}



/**
 * Visual Composer
 */

if (class_exists('WPBakeryVisualComposerAbstract')) {
    vc_map(array(
        "name" => esc_html__("Header Classic", 'vcx-theme-core'),
        "base" => "vcx_header_classic",
        "class" => "",
        "description" => esc_html__("Display Header All Content.", 'vcx-theme-core'),
        "category" => esc_html__('Emeet', 'vcx-theme-core'),
        "params" => array(

            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("Content Color Type", 'vcx-theme-core'),
                "param_name" 	=> "content_color_type",
                "value" 		=> array('Default'=>'default','Dark'=>'dark'),
            ),


            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("Banner Alignment", 'vcx-theme-core'),
                "param_name" 	=> "banner_align",
                "value" 		=> array('Left'=>'left','Center'=>'center' , 'Right'=>'right'),
            ),

            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("Content Width", 'vcx-theme-core'),
                "param_name" 	=> "content_width",
                "value" 		=> array('Fixed'=>'fixed','Fluid'=>'fluid' , 'Box'=>'box'),
            ),

            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("Banner Height", 'vcx-theme-core'),
                "param_name" 	=> "banner_height",
                "value" 		=> array('Default'=>'df', 'Medium'=>'md' , 'Small'=>'sm', 'Box'=>'box','Congested'=>'congested', 'Overflow'=>'overflow', 'Auto'=>'auto'),
            ),

            array(
                "type" 			=> "textfield",
                "heading" 		=> esc_html__("Pre Title", "vcx-theme-core"),
                "param_name" 	=> "vcx_pre_title",
                "value"         => 'Learn Anything',
            ),

            array(
                "type" 			=> "textfield",
                "admin_label"   => true,
                "heading" 		=> esc_html__("Title", "vcx-theme-core"),
                "param_name" 	=> "vcx_title",
                "value" 		=> 'Conference Meet {2}{0}{1}{9}',
                "description"   => esc_html__("To get theme style in title please follow this format: CONFERENCE MEET {2}{0}{1}{9}. To get line break use vertical bar ( | )", "vcx-theme-core"),

            ),

            array(
                "type"          => "checkbox",
                "weight"        => 10,
                "heading"       => esc_html__( "Enabled Typing Effect", "vcx-theme-core" ),
                "description"   => esc_html__("Please use  vertical bar ( | ) to separate sentence for typing effect title.  For Example: UX Conference 2019 | UI Conference 2019 | You learn Advance . *** Important: If you enable typing effect theme various color style will not working.", "vcx-theme-core"),
                "value"         => array('Yes'   => 'yes' ),
                "param_name"    => "vcx_typed_en"
            ),

            array(
                "type" 			=> "textfield",
                "admin_label"   => true,
                "heading" 		=> esc_html__(" Before Content of Title", "vcx-theme-core"),
                "param_name" 	=> "vcx_title_before",
                "value" 		=> '',
                "description"   => esc_html__("This is content will be append in before the title.", "vcx-theme-core"),

            ),

            array(
                "type" 			=> "textfield",
                "admin_label"   => true,
                "heading" 		=> esc_html__("After Content of Title", "vcx-theme-core"),
                "param_name" 	=> "vcx_title_after",
                "value" 		=> '',
                "description"   => esc_html__("This is content will be append in after the title.", "vcx-theme-core"),

            ),

            array(
                "type" 			=> "textfield",
                "admin_label"   => true,
                "heading" 		=> esc_html__("Second Title", "vcx-theme-core"),
                "param_name" 	=> "vcx_title_sec",
                "value" 		=> 'Conference Meet {2}{0}{1}{9}',
                "description"   => esc_html__("To get theme style in title please follow this format: CONFERENCE MEET {2}{0}{1}{9}. To get line break use vertical bar ( | )", "vcx-theme-core"),

            ),

            array(
                "type" 			=> "textfield",
                "admin_label"   => true,
                "heading" 		=> esc_html__("First Post Title", "vcx-theme-core"),
                "param_name" 	=> "vcx_post_title",
                "value" 		=> '21 King Street, Duach, United State.',
                "description"   => esc_html__("Add Second Post title Here", "vcx-theme-core"),

            ),

            array(
                "type" 			=> "textfield",
                "admin_label"   => true,
                "heading" 		=> esc_html__("First Post Title Icon", "vcx-theme-core"),
                "param_name" 	=> "vcx_post_title_icon",
                "value" 		=> 'fa-map-marker',
                "description"   => esc_html__(" Add Font Awesome Icon Class name Here. e.g. fa-map-marker", "vcx-theme-core"),

            ),

            array(
                "type" 			=> "textfield",
                "admin_label"   => true,
                "heading" 		=> esc_html__("Second Post Title", "vcx-theme-core"),
                "param_name" 	=> "vcx_post_title2",
                "value" 		=> '23-27 September, 2018.',
                "description"   => esc_html__("Add Second Post title Here", "vcx-theme-core"),

            ),

            array(
                "type" 			=> "textfield",
                "admin_label"   => true,
                "heading" 		=> esc_html__("Second Post Title Icon", "vcx-theme-core"),
                "param_name" 	=> "vcx_post_title_icon2",
                "value" 		=> 'fa-calender',
                "description"   => esc_html__(" Add Font Awesome Icon Class name Here. e.g. fa-calender", "vcx-theme-core"),

            ),

            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("Countdown Type", 'vcx-theme-core'),
                "param_name" 	=> "count_type",
                "value" 		=> array('None'=>'none','Simple'=>'simple', 'Circular'=>'circular'),
            ),

            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("Countdown Alignment", 'vcx-theme-core'),
                "param_name" 	=> "count_align",
                "value" 		=> array('Left'=>'left', 'Center'=>'center', 'Right'=>'right'),
            ),

            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("Countdown Style", 'vcx-theme-core'),
                "param_name" 	=> "count_style",
                "value" 		=> array('Default'=>'default', 'Border'=>'border', 'Dot'=>'dot'),
            ),

            array(
                "type" 			=> "textfield",
                "admin_label"   => true,
                "heading" 		=> esc_html__("Countdown Date", "vcx-theme-core"),
                "description"   => esc_html__("Important: Date Format Must be: Y/m/d (Year/ Month/ Date). For Example: 2020/10/5 0:0:0", "vcx-theme-core"),
                "param_name" 	=> "vcx_count_date",
                "value" 		=> '2019/12/15 0:0:0',
            ),



            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("Countdown Position", 'vcx-theme-core'),
                "param_name" 	=> "count_position",
                "value" 		=> array('Top'=>'top','Bottom'=>'bottom'),
            ),



            array(
                "type" 			=> "textfield",
                "admin_label"   => true,
                "heading" 		=> esc_html__("Countdown Day Text", "vcx-theme-core"),
                "param_name" 	=> "vcx_text_day",
                "value" 		=> 'Days',
            ),

            array(
                "type"          => "colorpicker",
                "heading"       => esc_html__("Day Color", "vcx-theme-core"),
                "param_name"    => "day_color",
                "value"         => "#ec398b",
            ),

            array(
                "type" 			=> "textfield",
                "admin_label"   => true,
                "heading" 		=> esc_html__("Countdown Hour Text", "vcx-theme-core"),
                "param_name" 	=> "vcx_text_hour",
                "value" 		=> 'Hours',
            ),

            array(
                "type"          => "colorpicker",
                "heading"       => esc_html__("Hour Color", "vcx-theme-core"),
                "param_name"    => "hour_color",
                "value"         => "#fac400",
            ),

            array(
                "type" 			=> "textfield",
                "admin_label"   => true,
                "heading" 		=> esc_html__("Countdown Minute Text", "vcx-theme-core"),
                "param_name" 	=> "vcx_text_min",
                "value" 		=> 'Minutes',
            ),

            array(
                "type"          => "colorpicker",
                "heading"       => esc_html__("Minute Color", "vcx-theme-core"),
                "param_name"    => "min_color",
                "value"         => "#00acee",
            ),

            array(
                "type" 			=> "textfield",
                "admin_label"   => true,
                "heading" 		=> esc_html__("Countdown Seconds Text", "vcx-theme-core"),
                "param_name" 	=> "vcx_text_sec",
                "value" 		=> 'Seconds',
            ),


            array(
                "type"          => "colorpicker",
                "heading"       => esc_html__("Seconds Color", "vcx-theme-core"),
                "param_name"    => "sec_color",
                "value"         => "#483fa1",
            ),

            array(
                "type"          => "colorpicker",
                "heading"       => esc_html__("Countdown Text Color", "vcx-theme-core"),
                "param_name"    => "cd_text_color",
                "value"         => "#ffffff",
            ),


            array(
                "type"          => "colorpicker",
                "heading"       => esc_html__("Circular BG Color Color", "vcx-theme-core"),
                "param_name"    => "cir_bg_color",
                "value"         => "#dddddd",
            ),


            array(
                "type"          => "textfield",
                "heading"       => esc_html__("First Button Text", "vcx-theme-core"),
                "param_name"    => "vcx_btn_text1",
                "value"         => "Become Volunteer",
            ),

            array(
                "type"          => "textfield",
                "heading"       => esc_html__("First Button URL", "vcx-theme-core"),
                "param_name"    => "vcx_btn_url1",
                "value"         => '',
            ),

            array(
                "type"          => "textfield",
                "heading"       => esc_html__("Second Button Text", "vcx-theme-core"),
                "param_name"    => "vcx_btn_text2",
                "value"         => "Donate Us",
            ),

            array(
                "type"          => "textfield",
                "heading"       => esc_html__("Second Button URL", "vcx-theme-core"),
                "param_name"    => "vcx_btn_url2",
                "value"         => '',
            ),

            array(
                "type" 			=> "textfield",
                "heading" 		=> esc_html__("Youtube Video ID", "vcx-theme-core"),
                "description" => esc_html__("Please add Youtube Video ID", 'vcx-theme-core'),
                "param_name" 	=> "header_video_id",
                "value" 		=> "oSPR5Go05Vg",
            ),

            array(
                "type"          => "textfield",
                "heading"       => esc_html__("Video Button Text", "vcx-theme-core"),
                "param_name"    => "vcx_video_text",
                "value"         => "Watch Promo Video",
            ),

            array(
                "type"          => "checkbox",
                "weight"        => 10,
                "heading"       => esc_html__( "Enabled Particle Effect", "vcx-theme-core" ),
                "value"         => array('Yes'   => 'yes' ),
                "param_name"    => "vcx_particle_en"
            ),


            array(
                "type" 			=> "attach_image",
                "heading" 		=> esc_html__("Upload Parallax Top Image", "vcx-theme-core"),
                "param_name" 	=> "parallax_img_1",
                "value" 		=> "",
            ),


            array(
                "type" 			=> "attach_image",
                "heading" 		=> esc_html__("Upload Parallax Bottom", "vcx-theme-core"),
                "param_name" 	=> "parallax_img_2",
                "value" 		=> "",
            ),


            array(
                "type" 			=> "attach_image",
                "heading" 		=> esc_html__("Upload Parallax Pattern Image", "vcx-theme-core"),
                "param_name" 	=> "parallax_img_3",
                "value" 		=> "",
            ),

            array(
                "type" 			=> "textfield",
                "heading" 		=> esc_html__("Parallax Title", "vcx-theme-core"),
                "param_name" 	=> "parallax_title",
                "value"         => 'CONFERENCE 2019',
            ),


        )
    ));
}

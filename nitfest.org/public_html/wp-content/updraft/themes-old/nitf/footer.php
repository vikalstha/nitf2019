

</div> <!-- End Main #content -->







<!--FOOTER-->



   <!--GOOGLE MAP-->



    <div class="innerpage-section">



        <div class="lgxmapcanvas map-canvas-default" id="map_canvas"> </div>



    </div>



    <!--GOOGLE MAP END-->











<!--FOOTER-->



<footer>



    <div id="lgx-footer" class="lgx-footer lgx-footer-black"> <!--lgx-footer-white-->



        <div class="lgx-inner-footer">



            <div class="container">



                <div class="lgx-footer-area lgx-footer-area-center">



                    <div class="lgx-footer-single">



                        <h3 class="footer-title">Social Connection</h3>



                        <p class="text">



                            You should connect social area <br> for Any update



                        </p>



                        <ul class="list-inline lgx-social-footer">



                            <li><a href="https://www.facebook.com/NITFest2019/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>



                            <li><a href="https://www.twitter.com/nitfest_2019/"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>



                            <li><a href="https://www.instagram.com/nitfest2019/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>



                            <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>



                        </ul>



                    </div>



                    <div class="lgx-footer-single">



                        <h3 class="footer-title">Venue Location </h3>



                        <h4 class="date">



                            25th February- 4th March 2019



                        </h4>



                        <address>



                            <span id="lgx-typed-string-footer-theatre">Mandala Theatre, Anamnagar <br> Kathmandu, Nepal</span><br>



                        </address>



                        <a id="myModalLabel2" data-toggle="modal" data-target="#lgx-modal-map" class="map-link" href="#"><i class="fa fa-map-marker" aria-hidden="true"></i> View Map location</a>



                    </div>



                    <div class="lgx-footer-single">



                        <h2 class="footer-title">Contact Us</h2>



                        <div id="contact">



                            <ul>



                                <li><i class="fa fa-envelope" aria-hidden="true"></i> Email: <a href="mailto:festivalcell@nitfest.org">festivalcell@nitfest.org</a></li>



                                <li><i class="fa fa-phone" aria-hidden="true"></i> Phone: 9849296461</li>



                            </ul>



                        </div>



                    </div>



                </div>



                <!-- Modal-->



                <div id="lgx-modal-map" class="modal fade lgx-modal">



                    <div class="modal-dialog">



                        <div class="modal-content">



                            <div class="modal-header">



                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>



                            </div>



                            <div class="modal-body">



                                <div class="lgxmapcanvas map-canvas-default" id="map_canvas"> </div>



                            </div>



                        </div>



                    </div>



                </div> <!-- //.Modal-->







                <div class="lgx-footer-bottom">



                    <div class="lgx-copyright">



                        <p> <span>©</span> NIT Fest. 2019 is powered by <a href="http://nitfest.org/">NIT Fest.</a> All product names, logos, and brands are property of their respective owners. <br> 



                        Web-page Themed and Powered By : <a href="http://magicsquare.us">MagicSquare |  <img src="/assets/img/mantra.png" class="img-fluid"></a>



                        </p>



                    </div>



                </div>







            </div>



            <!-- //.CONTAINER -->



        </div>



        <!-- //.footer Middle -->



    </div>



</footer>



<!--FOOTER END-->











</div><!-- Main Page #lgxpage -->







<?php wp_footer(); ?>







<!-- if load google maps then load this api, change api key as it may expire for limit cross as this is provided with any theme -->



<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyDQ0V5g22ODce1dIwc_KRmeJWc8hdPcKoE"></script>







<!-- CUSTOM GOOGLE MAP -->



<script> 

jQuery( document ).ready(function( $ ) {

    $.fn.googleMap = function(params) {



        params = $.extend( {



            zoom : 12,



            coords : [27.121212, 85.11111],



            type : "ROADMAP",



            debug : false,



            langage : "english",



            mouseZoom: true



        }, params);



        



        switch(params.type) {



            case 'ROADMAP':



            case 'SATELLITE':



            case 'HYBRID':



            case 'TERRAIN':



                params.type = google.maps.MapTypeId[params.type];



                break;



            default:



                params.type = google.maps.MapTypeId.ROADMAP;



                break;



        }



        



        this.each(function() {



                        



            var map = new google.maps.Map(this, {



                zoom: params.zoom,



                center: new google.maps.LatLng(params.coords[0], params.coords[1]),



                mapTypeId: params.type,



                scrollwheel: params.mouseZoom



            });



            



            $(this).data('googleMap', map);



            $(this).data('googleLang', params.langage);



            $(this).data('googleDebug', params.debug);



            $(this).data('googleMarker', new Array());



            $(this).data('googleBound', new google.maps.LatLngBounds());



        });



    



        return this;



    }







    $.fn.addMarker = function(params) {



        params = $.extend( {



            coords : false,



            address : false,



            url : false,



            id : false,



            icon : false,



            draggable : false,



            title : "",



            text : "",



            success : function() {}



        }, params);



        



        this.each(function() {



            $this = $(this);



            



            if(!$this.data('googleMap')) {



                if($this.data('googleDebug'))



                    console.error("jQuery googleMap : Unable to add a marker where there is no map !");



                return false;



            }



            



            if(!params.coords && !params.address) {



                if($this.data('googleDebug'))



                    console.error("jQuery googleMap : Unable to add a marker if you don't tell us where !");



                return false;



            }



                        



            if(params.address && typeof params.address == "string") {



                geocoder = new google.maps.Geocoder();



                



                geocoder.geocode({



                    address : params.address,



                    bounds : $this.data('googleBound'),



                    language : $this.data('googleLang')



              }, function(results, status) {



                    if (status == google.maps.GeocoderStatus.OK) {



                        $this.data('googleBound').extend(results[0].geometry.location);



                        



                        if(params.icon) {



                            var marker = new google.maps.Marker({



                                map: $this.data('googleMap'),



                                position: results[0].geometry.location,



                                title: params.title,



                                icon: params.icon,



                                draggable: params.draggable



                            });



                        } else {



                            var marker = new google.maps.Marker({



                                map: $this.data('googleMap'),



                                position: results[0].geometry.location,



                                title: params.title,



                                draggable: params.draggable



                            });



                        }



                        



                        if(params.draggable) {



                            google.maps.event.addListener(marker, 'dragend', function() {



                                var location = marker.getPosition();



                                



                                var coords = {};



                                



                                if(typeof location.d != "undefined") {



                                    coords.lat = location.d;



                                    coords.lon = location.e;



                                } else {



                                    if($this.data('googleDebug'))



                                        console.error("jQuery googleMap : Wrong google response", location);



                                }



                                



                                params.success(coords);



                            });



                        }



                        



                        if(params.title != "" && params.text != "" && !params.url) {



                            var infowindow = new google.maps.InfoWindow({



                                content: "<h4>"+params.title+"</h4>"+params.text



                            });



                            



                            var map = $this.data('googleMap');



                            



                            google.maps.event.addListener(marker, 'click', function() {



                                infowindow.open(map, marker);



                            });



                        } else if(params.url) {



                            google.maps.event.addListener(marker, 'click', function() {



                                document.location = params.url;



                            });



                        }



                        



                        if(!params.id) {



                            $this.data('googleMarker').push(marker);



                        } else {



                            $this.data('googleMarker')[params.id] = marker;



                        }



                        



                        if($this.data('googleMarker').length == 1) {



                            $this.data('googleMap').setCenter(results[0].geometry.location);



                            $this.data('googleMap').setZoom($this.data('googleMap').getZoom());



                        } else {



                            $this.data('googleMap').fitBounds($this.data('googleBound'));



                        }



                        



                        var coords = {};



                        



                        if(typeof results[0].geometry.location.d != "undefined") {



                            coords.lat = results[0].geometry.location.d;



                            coords.lon = results[0].geometry.location.e;



                        } else {



                            if($this.data('googleDebug'))



                                console.error("jQuery googleMap : Wrong google response", results[0].geometry.location);



                        }



                                                  



                        params.success(coords);



            



                    } else {



                        if($this.data('googleDebug'))



                            console.error("jQuery googleMap : Unable to find the place asked for the marker ("+status+")");



                    }



                });



            } else {



                $this.data('googleBound').extend(new google.maps.LatLng(params.coords[0], params.coords[1]));



        



        if(params.icon) {



                    var marker = new google.maps.Marker({



                        map: $this.data('googleMap'),



                        position: new google.maps.LatLng(params.coords[0], params.coords[1]),



                        title: params.title,



                        icon: params.icon



                    });



                } else {



                    var marker = new google.maps.Marker({



                        map: $this.data('googleMap'),



                        position: new google.maps.LatLng(params.coords[0], params.coords[1]),



                        title: params.title



                    });



                }



                



        if(params.title != "" && params.text != "" && !params.url) {



          var infowindow = new google.maps.InfoWindow({



                        content: "<h3>"+params.title+"</h3>"+params.text



                    });



                    



                    var map = $this.data('googleMap');



              



            google.maps.event.addListener(marker, 'click', function() {



                infowindow.open(map, marker);



            });



                } else if(params.url) {



          google.maps.event.addListener(marker, 'click', function() {



              document.location = params.url;



          });



                }



                



        if(!params.id) {



            $this.data('googleMarker').push(marker);



        } else {



          $this.data('googleMarker')[params.id] = marker;



        }







                if($this.data('googleMarker').length == 1) {



                  $this.data('googleMap').setCenter(new google.maps.LatLng(params.coords[0], params.coords[1]));



                  $this.data('googleMap').setZoom($this.data('googleMap').getZoom());



                } else {



                    $this.data('googleMap').fitBounds($this.data('googleBound'));



                }



                



                params.success({



                    lat: params.coords[0],



                    lon: params.coords[1]



                });



            }



        });



        



        return this;



    }



    



    $.fn.removeMarker = function(id) {



        this.each(function() {



            var $this = $(this);



            



        if(!$this.data('googleMap')) {



            if($this.data('googleDebug'))



            console.log("jQuery googleMap : Unable to delete a marker where there is no map !");



        return false;



        }



        



        var $markers = $this.data('googleMarker');



                    



        if(typeof $markers[id] != 'undefined') {



            $markers[id].setMap(null);



        if($this.data('googleDebug'))



            console.log('jQuery googleMap : marker deleted');



        } else {



        if($this.data('googleDebug'))



            console.error("jQuery googleMap : Unable to delete a marker if it not exists !");



        return false;



        }



        });



    }







    $.fn.addWay = function(params) {



        params = $.extend( {



            start : false,



            end : false,



            step : [],



            route : false,



            langage : 'english'



        }, params);



        



        var direction = new google.maps.DirectionsService({



            region: "fr"



        });



        



        var way = new google.maps.DirectionsRenderer({



            draggable: true,



            map: $(this).data('googleMap'),



            panel: document.getElementById(params.route),



            provideTripAlternatives: true



        });



        



        var waypoints = [];



        



    for(var i in params.step) {



        var step;



      if(typeof params.step[i] == "object") {



        step = new google.maps.LatLng(params.step[i][0], params.step[i][1]);



      } else {



        step = params.step[i];



      }



      



      waypoints.push({



        location: step,



        stopover: true



      });



    }



                



        if(typeof params.end != "object") {



            var geocoder = new google.maps.Geocoder();



            



        geocoder.geocode({



            address  : params.end,



            bounds   : $(this).data('googleBound'),



            language : params.langage



        }, function(results, status) {



        if (status == google.maps.GeocoderStatus.OK) {



        



            var request = {



            origin: params.start,



            destination: results[0].geometry.location,



            travelMode: google.maps.DirectionsTravelMode.DRIVING,



            region: "fr",



            waypoints: waypoints



            };



    



            direction.route(request, function(response, status) {



            if (status == google.maps.DirectionsStatus.OK) {



              way.setDirections(response);



            } else {



              if($(this).data('googleDebug'))



                    console.error("jQuery googleMap : Unable to find the place asked for the route ("+response+")");



            }



            });







        } else {



          if($(this).data('googleDebug'))



            console.error("jQuery googleMap : Address not found");



        }



        });



        } else {



            var request = {



                origin: params.start,



                destination: new google.maps.LatLng(params.end[0], params.end[1]),



                travelMode: google.maps.DirectionsTravelMode.DRIVING,



                region: "fr",



                waypoints: waypoints



            };



    



            direction.route(request, function(response, status) {



                if (status == google.maps.DirectionsStatus.OK) {



                    way.setDirections(response);



                } else {



                    if($(this).data('googleDebug'))



            console.error("jQuery googleMap : Address not found");



                }



            });



        }



        



        return this;



    }

});



jQuery(function($) {







 /*=========================================================================



         ===  Typed Animation START



         ========================================================================== */



        if($('#lgx-typed-string').length){



            $('#lgx-typed-string').typed({



                strings: ["Theatre for Social Transformation: An Artistic Voyage","8 Days - 4 Venues - 7 Organizers", "Theatre for Social Transformation: An Artistic Voyage"],



                // typing speed



                typeSpeed: 10,



                // time before typing starts



                startDelay: 0,



                // backspacing speed



                backSpeed: 0,



                // shuffle the strings



                shuffle: false,



                // time before backspacing



                backDelay: 500,



                // loop



                loop: true,



                // false = infinite



                loopCount: false,



                // show cursor



                showCursor: true,



                // character for cursor



                cursorChar: "|",



                // either html or text



                contentType: 'html'



            });



        }



        if($('#lgx-typed-string-footer-theatre').length){



            $('#lgx-typed-string-footer-theatre').typed({



                strings: ["Shilpee Theatre, Baneshwor <br> Kathmandu, Nepal"," Katha Ghera, Guna Kamdev Marg<br> Kathmandu, Nepal" , "Theatre Mall, Kirtipur <br> Kathmandu, Nepal","Mandala Theatre, Anamnagar <br> Kathmandu, Nepal"],



                // typing speed



                typeSpeed: 8,



                // time before typing starts



                startDelay: 1000,



                // backspacing speed



                backSpeed: 0,



                // shuffle the strings



                shuffle: false,



                // time before backspacing



                backDelay: 900,



                // loop



                loop: true,



                // false = infinite



                loopCount: false,



                // show cursor



                showCursor: true,



                // character for cursor



                cursorChar: "|",



                // either html or text



                contentType: 'html'



            });



        }







        /*=========================================================================



         ===  GOOGLE MAP



         ========================================================================== */



        if (typeof google != 'undefined') {



            //for Default  map



            if ($('.map-canvas-default').length)



            {







                $(".map-canvas-default").googleMap({



                    zoom: 12, // Initial zoom level (optiona



                    coords: [27.696197, 85.3240626], // Map center (optional)



                    type: "ROADMAP", // Map type (optional),



                    mouseZoom: false



                });







                // done



                $(".map-canvas-default").addMarker({



                    coords: [27.6962017,85.3262513], // GPS coords



                    title: 'Mandala Theatre Nepal',



                    text: 'Anamnagar',



                });







                $(".map-canvas-default").addMarker({



                    coords: [27.6696372,85.2685225], // GPS coords



                    title: 'Theatre Mall',



                    text: 'Kirtipur',



                });







                $(".map-canvas-default").addMarker({



                    coords: [27.7125607,85.2980052], // GPS coords



                    title: 'Vajra Hotel (Naga Theatre)',



                    text: 'Swayambhu',



                });







                // $(".map-canvas-default").addMarker({



                //     coords: [40.7127, 74.0059], // GPS coords



                //     title: 'One World Theatre',



                //     text: 'One World Theatre',



                // });







               



                // done



                $(".map-canvas-default").addMarker({



                    coords: [27.697858,85.3022785], // GPS coords



                    title: 'Katha Ghera',



                    text: 'Teku, Kathmandu',



                });







                // done



                $(".map-canvas-default").addMarker({



                    coords: [27.7087497,85.312365], // GPS coords



                    title: 'Rastriya Nachghar',



                    text: 'Jamal,Kathmandu',



                });







                $(".map-canvas-default").addMarker({



                    coords: [27.7044567,85.3388993], // GPS coords



                    title: 'Shilpee Theatre Nepal',



                    text: 'Battisputali, Kathmandu',



                });



                



            }



        }











        /*=========================================================================



         ===  //GOOGLE MAP END



         ========================================================================== */







});



</script>







</body>



</html>




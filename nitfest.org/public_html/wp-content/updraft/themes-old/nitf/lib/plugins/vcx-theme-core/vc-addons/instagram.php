<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

// instagram
add_shortcode( 'vcx_instagram', 'vcx_instagram_function');

/**
 *  Title Section Short Code
 * @param $atts
 * @return string
 */


function vcx_instagram_function($atts)
{
    extract(shortcode_atts(array(
        'vcx_title'     => 'Instagram Feed',
        'user_id'       => ' ',
        'client_id'     => ' ',
        'token'         => ' ',
        'limit'         => '8',

    ), $atts));


    ob_start(); ?>

    <div id="lgx-instagram" class="lgx-instagram">
        <div class="lgx-inner">
            <div class="lgx-instagram-area">
                <div class="insta-text">
                    <div class="lgx-hover-link">
                        <div class="lgx-vertical">
                            <h2 class="text"><?php echo esc_html($vcx_title);?></h2>
                        </div>
                    </div>
                </div>
                <div id="instafeed" class="vcx-instagram-feed"
                     data-user="<?php echo esc_html($user_id);?>"
                     data-client="<?php echo esc_html($client_id);?>"
                     data-token="<?php echo esc_html($token);?>"
                     data-limit="<?php echo esc_html($limit);?>"
                >
                </div>
            </div>
        </div><!--l//lgx-inner-->
    </div>

    <?php
    return ob_get_clean();
}




/**
 * Visual Composer
 */

if (class_exists('WPBakeryVisualComposerAbstract')) {
    vc_map(array(
        "name" => esc_html__("Instagram Feed", 'vcx-theme-core'),
        "base" => "vcx_instagram",
        "class" => "",
        "description" => esc_html__("Display Instagram Feed Here.", 'vcx-theme-core'),
        "category" => esc_html__('Emeet', 'vcx-theme-core'),
        "params" => array(

            array(
                "type" 			=> "textfield",
                "admin_label"   => true,
                "heading" 		=> esc_html__("Title", "vcx-theme-core"),
                "param_name" 	=> "vcx_title",
                "value" 		=> 'Instagram Feed',
                "description"   => esc_html__("Instagram Area Title.", "vcx-theme-core"),

            ),

            array(
                "type" 			=> "textfield",
                "admin_label"   => true,
                "heading" 		=> esc_html__("User ID", "vcx-theme-core"),
                "param_name" 	=> "user_id",
                "value" 		=> '',
                "description"   => esc_html__("Add Your Instagram User ID Here.", "vcx-theme-core"),

            ),


            array(
                "type" 			=> "textfield",
                "admin_label"   => true,
                "heading" 		=> esc_html__("Client ID", "vcx-theme-core"),
                "param_name" 	=> "client_id",
                "value" 		=> '',
                "description"   => esc_html__("Add Your Instagram Client ID Here", "vcx-theme-core"),

            ),


            array(
                "type" 			=> "textfield",
                "admin_label"   => true,
                "heading" 		=> esc_html__("Access Token", "vcx-theme-core"),
                "param_name" 	=> "token",
                "value" 		=> '',
                "description"   => esc_html__("Add Your Instagram Access Token Here", "vcx-theme-core"),
            ),


            array(
                "type" 			=> "textfield",
                "admin_label"   => true,
                "heading" 		=> esc_html__("Limits", "vcx-theme-core"),
                "param_name" 	=> "limit",
                "value" 		=> '8',
                "description"   => esc_html__("Instagram Feed Limit.", "vcx-theme-core"),
            ),

        )
    ));
}


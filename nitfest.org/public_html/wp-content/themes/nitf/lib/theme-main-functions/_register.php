<?php
/**
 * Created by PhpStorm.
 * User: DCL network
 * Time: 1:28 PM
 */

/************************************************************************
 * Register widget area.
 *************************************************************************/


function emeet_widgets_init() {

    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar', 'emeet' ),
        'id'            => 'sidebar-1',
        'description'   => esc_html__( 'Add widgets here.', 'emeet' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );




}
add_action( 'widgets_init', 'emeet_widgets_init' );



/*-------------------------------------------------------
*           Include the TGM Plugin Activation class
*-------------------------------------------------------*/

add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}
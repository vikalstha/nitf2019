<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

add_shortcode( 'vcx_event_about', 'vcx_event_about_function');

/**
 *  Title Section Short Code
 * @param $atts
 * @return string
 */

function vcx_event_about_function($atts)
{

    extract(shortcode_atts(array(
        'about_title'         => 'UI CONFERENCE 2030', // Section Lead
        'lead_text'           => 'Why User Interface Conference ?', // Section Lead
        'about_page_content'  => '', // Section Lead
        'buy_url'             => '', // Buy Button URL
        'buy_urlr'            => '', // Buy Button URL
        'buy_text'            => 'Buy Ticket', // Buy Button URL
        'buy_textr'           => 'More About', // Buy Button URL
        'text_align'          => 'left', // Buy Button URL
        'counter_number'      => '29',
        'counter_title'       => 'NOVEMBER 2019',
        'counter_post_text'   => 'King Street, Dhaka, Bangladesh',
        'color_type'          => 'default',
    ), $atts));

    $lead_text_one = (!empty($about_title) ? '<h2 class="heading">'.vcx_spilt_title($about_title).'</h2>' : '');
    $lead_text_html = (!empty($lead_text) ? '<h3 class="subheading ">'.esc_html($lead_text).'</h3>' : '');

    $left_btn_html = (!empty($buy_url) ? '<a class="lgx-btn" href="'.esc_attr($buy_url).'"><span>'.esc_html($buy_text).'</span></a>' : '');
    $right_btn_html = (!empty($buy_urlr) ? '<a class="lgx-btn lgx-btn-red" href="'.esc_attr($buy_urlr).'"><span>'.esc_html($buy_textr).'</span></a>' : '');

    $counter_number_html= !empty($counter_number) ?  '<div class="about-date-area"> <h4 class="date"><span class="lgx-counter">'.esc_html($counter_number).'</span></h4><p><span>'.esc_html($counter_title).'</span>'.esc_html($counter_post_text).'</p></div>' : '';


    $output = '<div class="lgx-about-content-area lgx-about-content-area-'.$text_align.' about-color-'.$color_type.'">
                        <div class="lgx-heading">
                            '.$lead_text_one.'
                            '.$lead_text_html.'
                        </div>
                        <div class="lgx-about-content">
                            <p class="text">'.$about_page_content.'</p>  
                                                     
                            '.$counter_number_html.'     
                                                  
                            <div class="section-btn-area">
                             '.$left_btn_html.'
                             '.$right_btn_html.'
                            </div>                            
                        </div>
                     </div>';
    return $output;
}



/**
 * Visual Composer
 */

if (class_exists('WPBakeryVisualComposerAbstract')) {
    vc_map(array(
        "name" => esc_html__("About Section", 'vcx-theme-core'),
        "base" => "vcx_event_about",
        "class" => "",
        "description" => esc_html__("Display About Section Content.", 'vcx-theme-core'),
        "category" => esc_html__('Emeet', 'vcx-theme-core'),
        "params" => array(


            array(
                "type"          => "textfield",
                "heading"       => esc_html__("About Title", "vcx-theme-core"),
                "param_name"    => "about_title",
                "value"         => "HAPPY NEW YEAR 2019",
            ),

            array(
                "type" 			=> "textfield",
                "heading" 		=> esc_html__("Section Post Title", "vcx-theme-core"),
                "param_name" 	=> "lead_text",
                "value" 		=> "Why Happy New Year 2019 ?",
            ),
            array(
                "type"          => "textarea",
                "heading"       => esc_html__("Section Short Content", 'vcx-theme-core'),
                "param_name"    => "about_page_content",
                "description" => esc_html__("Write Section Short Description Here.", 'vcx-theme-core'),
                "value"         => '',
            ),

            array(
                "type"          => "textfield",
                "heading"       => esc_html__("Left Button URL", "vcx-theme-core"),
                "param_name"    => "buy_url",
                "value"         => "",
            ),

            array(
                "type"          => "textfield",
                "heading"       => esc_html__("Left Button Text", "vcx-theme-core"),
                "param_name"    => "buy_text",
                "value"         => "Buy  Ticket",
            ),
            array(
                "type"          => "textfield",
                "heading"       => esc_html__("Right Button URL", "vcx-theme-core"),
                "param_name"    => "buy_urlr",
                "value"         => "",
            ),

            array(
                "type" 			=> "textfield",
                "heading" 		=> esc_html__("Right Button Text", "vcx-theme-core"),
                "param_name" 	=> "buy_textr",
                "value" 		=> "More About",
            ),

            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("Text Align", 'vcx-theme-core'),
                "param_name" 	=> "text_align",
                "value" 		=> array('Left'=>'left',  'Center'=>'center','Right'=>'right'),
            ),

            array(
                "type" 			=> "textfield",
                "admin_label"   => true,
                "heading" 		=> esc_html__("Counter Up Number", "vcx-theme-core"),
                "param_name" 	=> "counter_number",
                "value" 		=> '29',
            ),

            array(
                "type" 			=> "textfield",
                "admin_label"   => true,
                "heading" 		=> esc_html__("Counter Title", "vcx-theme-core"),
                "param_name" 	=> "counter_title",
                "value" 		=> 'NOVEMBER 2019',
            ),

            array(
                "type" 			=> "textfield",
                "admin_label"   => true,
                "heading" 		=> esc_html__("Counter Post Text", "vcx-theme-core"),
                "param_name" 	=> "counter_post_text",
                "value" 		=> 'King Street, Dhaka, Bangladesh',
            ),

            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("Content Color", 'vcx-theme-core'),
                "param_name" 	=> "color_type",
                "value" 		=> array('Default'=>'default','White'=>'white'),
            )

        )

    ));
}
��    A      $      ,      ,     -     J  	   Y     c     k     x  
   �     �     �     �  	   �     �     �     �     �     �  4   �  �        �  
   �     �     �     �     �     �  
   �                    '     -     <     R  
   W     b     h     u     �     �     �     �     �     �     �     �     �     �       �        �     �  	   �  �   �     d     l     x  &   �  +   �     �     �  	   �  -   �  0   "	  0   S	    �	  *   �
     �
  	   �
  	   �
     �
                *  	   1     ;  	   I     S     Y     ]     o     v  7   �  �   �     c  
   z     �     �     �     �     �     �  	   �  	   �     �     �  !        '  
   ?     J     W     `     r     �     �     �     �     �  #   �     �     �     �            �   #            
     �        �  
   �     �  5   �     &     -     =     J  
   R     ]     n   "%s" or empty or set to zero Active Filters Afternoon All Day Autocomplete Available Filters Checkboxes City Closed Collapse Filters Cost (%s) Country Day Dropdown Evening Event Category Events are considered free when cost field is: %s %s Expand an active filter to edit the label and choose from a subset of input types (dropdown, select, range slider, checkbox and radio). Featured Events Filter Bar Filters Filters Default State Filters Layout Free Friday Horizontal Monday Morning Narrow Your Results Night No items match Only when set to zero Open Organizers Other Range Slider Reset Filters Saturday Select Select an Item Show Advanced Filters Show Filters Show featured events only State/Province Submit Sunday Tags The Events Calendar The settings below allow you to enable or disable front-end event filters. Uncheck the box to hide the filter. Drag and drop active filters to re-arrange them. Thursday Time Title: %s To begin using The Events Calendar: Filter Bar, please install the latest version of <a href="%s" class="thickbox" title="%s">The Events Calendar</a>. Tuesday Type: %s %s Type: %s %s %s Used as the identifier for free Events Used as the identifier for free EventsFree Venues Vertical Wednesday additional fields filter logic settingLogic: additional fields filter logic settingMatch all additional fields filter logic settingMatch any PO-Revision-Date: 2017-02-13 04:23:56+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: GlotPress/1.0-alpha-1100
Project-Id-Version: Filter Bar
 "%s" nebo prázdné nebo nastaveno na nulu Aktivní filtry Odpoledne Celý den Automatické dokončování Dostupné filtry Zaškrtávací políčka Město Zavřené Skrýt filtry Cena (%s) Země Den Rozbalovací menu Večer Rubrika akce Akce se považují za zdarma, pokud pole cena je: %s %s Rozbalením aktivního filtru můžete změnit jeho název a vybrat typ vstupního pole (rozbalovací menu, posuvník rozsahu, zaškrtávací políčko a přepínač). Doporučené události Filter Bar Filtry Výchozí stav filtrů Rozvržení filtrů Zdarma Pátek Horizontálně Pondělí Dopoledne Filtrování výběru V noci Nebyly nalezeny žádné položky Nastaveno pouze na nulu Otevřené Pořadatelé Ostatní Posuvník rozsahu Zrušit filtry Sobota Vybrat Vyberte položku Zobrazit pokročilé filtry Zobrazit filtry Zobrazit jen doporučené události Kraj Potvrdit Neděle Štítky The Events Calendar Pomocí níže uvedených nastavení můžete návštěvníkům webu povolit či zakázat filtrování akcí. Odškrtnutím políčka skryjete daný filtr. Přetáhnutím aktivních filtrů změníte jejich uspořádání. Čtvrtek Čas Název: %s Než začnete používat plugin The Events Calendar: Filter Bar, nainstalujte si prosím aktuální verzi pluginu <a href="%s" class="thickbox" title="%s">The Events Calendar</a>. Úterý Typ: %s %s Typ: %s %s %s Používá se jako identifikátor pro bezplatné akce Zdarma Místa konání Vertikálně Středa Podmínka: Odpovídá všem Odpovídá některým 
<?php

/**
 * Location Area Widget
 *
 * @link       http://logichunt.com
 * @since      1.0.0
 *
 * @package    Lgx_Social_Area
 * @subpackage Lgx_Social_Area/widget
 *
 * @author     LogicHunt <info.logichunt@gmail.com>
 */


class Vcx_location_Widget extends WP_Widget {


	public function __construct() {
		parent::__construct('Vcx_Location_Widget', 'Emeet: Location', array('description' => __('Widget for display Location ', 'vcx-theme-core')));
	}



	public function form($instance) {

		// Default Value
		$title           = isset($instance['title']) ? $instance['title'] : __('Venue Location', 'vcx-theme-core');
		$sub_title       = isset($instance['sub_title']) ? $instance['sub_title'] : __('85 Golden Street, Darlinghurst ERP 2019, United States', 'vcx-theme-core');
		$date            = isset($instance['date']) ? $instance['date'] : __('18 - 21 DECEMBER, 2019');
		$icon_url        = isset($instance['icon_url']) ? esc_url($instance['icon_url']) : '';
		$icon_text       = isset($instance['icon_text']) ? $instance['icon_text'] : __('View Map location', 'vcx-theme-core');



		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'vcx-theme-core'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
		</p>

        <p>
            <label for="<?php echo $this->get_field_id('date'); ?>"><?php _e('Date:', 'vcx-theme-core'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('date'); ?>" name="<?php echo $this->get_field_name('date'); ?>" type="text" value="<?php echo esc_attr($date); ?>" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('sub_title'); ?>"><?php _e('Lead Title:', 'vcx-theme-core'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('sub_title'); ?>" name="<?php echo $this->get_field_name('sub_title'); ?>" type="text" value="<?php echo esc_attr($sub_title); ?>" />
        </p>


        <p>
            <label for="<?php echo $this->get_field_id('icon_url'); ?>"><?php _e('URL:', 'vcx-theme-core'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('icon_url'); ?>" name="<?php echo $this->get_field_name('icon_url'); ?>" type="text" value="<?php echo esc_attr($icon_url); ?>" />
        </p>



        <p>
            <label for="<?php echo $this->get_field_id('icon_text'); ?>"><?php _e('Icon Text:', 'vcx-theme-core'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('icon_text'); ?>" name="<?php echo $this->get_field_name('icon_text'); ?>" type="text" value="<?php echo esc_attr($icon_text); ?>" />
        </p>


        <?php
	}



	public function update($new_instance, $old_instance) {

		// Set Instance
		$instance                   = array();
		$instance['title']          = strip_tags($new_instance['title']);
		$instance['date']           = strip_tags($new_instance['date']);
		$instance['sub_title']      = strip_tags($new_instance['sub_title']);
		$instance['icon_text']      = strip_tags($new_instance['icon_text']);
		$instance['icon_url']       = esc_url($new_instance['icon_url']);

		return $instance;
	}


	public function widget($args, $instance) {
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);



		// Widget Output
		echo $before_widget;

		echo (!empty($title) ? $before_title . $title . $after_title : '');

        $date = !empty($instance['date']) ? '<h4 class="date">'.$instance['date'] .'</h4>': '';
        $sub_title = !empty($instance['sub_title']) ? '<p class="text footer-lead">'.$instance['sub_title'] .'</p>': '';

        $icon_url = !empty($instance['icon_url']) ? '<p class="location_url"><i class="fa fa-map-marker" aria-hidden="true"></i> <a class="map-link" href="'.esc_url($instance['icon_url']).'" target="_blank"> '.$instance['icon_text'].'</a></p>': '';


       echo $date . $sub_title .$icon_url;

		echo $after_widget;
	}
}



// Register Foo_Widget widget
add_action( 'widgets_init', function() { register_widget( 'Vcx_location_Widget' ); } );
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: NITF\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-10-30 05:44+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/"

#: post-format/content-single-audio.php:39
#: post-format/content-single-gallery.php:54
#: post-format/content-single-link.php:41
#: post-format/content-single-quote.php:49
#: post-format/content-single-video.php:43 post-format/content-single.php:41
msgid "% Comments"
msgstr ""

#: lib/theme-main-functions/_helpers.php:71
#: lib/theme-main-functions/_helpers.php:157
#: lib/theme-main-functions/_helpers.php:173
#, php-format
msgid "%s"
msgstr ""

#: lib/theme-main-functions/_helpers.php:348
msgid "(optional)"
msgstr ""

#. used between list items, there is a space after the comma
#: inc/template-tags.php:50 inc/template-tags.php:56
msgid ", "
msgstr ""

#: post-format/content-single-audio.php:38
#: post-format/content-single-gallery.php:53
#: post-format/content-single-link.php:40
#: post-format/content-single-quote.php:48
#: post-format/content-single-video.php:42 post-format/content-single.php:40
msgid "1 Comment"
msgstr ""

#: lib/theme-main-functions/_helpers.php:194
msgid "404 Error"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:290
#: lib/theme-config/config-redux/_theme-options_old.php:290
msgid "<p>This is the sidebar content, HTML is allowed.</p>"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:268
#: lib/theme-config/config-redux/_theme-options.php:278
#: lib/theme-config/config-redux/_theme-options_old.php:268
#: lib/theme-config/config-redux/_theme-options_old.php:278
msgid "<p>This is the tab content, HTML is allowed.</p>"
msgstr ""

#: lib/theme-main-functions/_helpers.php:227
msgid "Add Menu"
msgstr ""

#: logichunt/plugin-admin.php:42
msgid "Add New"
msgstr ""

#: logichunt/plugin-admin.php:41
msgid "Add New Carousel Item"
msgstr ""

#: lib/theme-main-functions/_register.php:18
#: lib/theme-main-functions/_register.php:29
#: lib/theme-main-functions/_register.php:41
msgid "Add widgets here."
msgstr ""

#: logichunt/plugin-admin.php:39
msgid "All Carousel"
msgstr ""

#: footer.php:71
msgid "All Rights Reserved."
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:623
#: lib/theme-config/config-redux/_theme-options_old.php:558
msgid "Archive Page"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:640
#: lib/theme-config/config-redux/_theme-options_old.php:575
msgid "Archive Post"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:652
#: lib/theme-config/config-redux/_theme-options_old.php:587
msgid "Archive Sidebar"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:638
#: lib/theme-config/config-redux/_theme-options_old.php:573
msgid "Archive Title"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:515
#: lib/theme-config/config-redux/_theme-options.php:720
#: lib/theme-config/config-redux/_theme-options_old.php:450
#: lib/theme-config/config-redux/_theme-options_old.php:747
msgid "Background Color"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:507
#: lib/theme-config/config-redux/_theme-options.php:712
#: lib/theme-config/config-redux/_theme-options_old.php:442
#: lib/theme-config/config-redux/_theme-options_old.php:739
msgid "Background Image"
msgstr ""

#: lib/theme-config/config-redux/_theme-options_old.php:688
msgid "Behance Url"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:317
#: lib/theme-config/config-redux/_theme-options_old.php:317
msgid "Best Size is (115 X 38) px"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:384
#: lib/theme-config/config-redux/_theme-options_old.php:384
msgid "Best Size is (150 X 150) px"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:425
msgid "Black Transparent"
msgstr ""

#: lib/theme-main-functions/_helpers.php:140
msgid "Blog"
msgstr ""

#. Name of the template
msgid "Blog Full Width"
msgstr ""

#. Name of the template
msgid "Blog Left Sidebar"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:526
#: lib/theme-config/config-redux/_theme-options_old.php:461
msgid "Blog Page"
msgstr ""

#. Name of the template
msgid "Blog Right Sidebar"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:553
#: lib/theme-config/config-redux/_theme-options_old.php:488
msgid "Blog Sidebar"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:539
#: lib/theme-config/config-redux/_theme-options_old.php:474
msgid "Blog Title"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:444
msgid "Box"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:356
#: lib/theme-config/config-redux/_theme-options_old.php:356
msgid "Brand Button Color"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:349
#: lib/theme-config/config-redux/_theme-options_old.php:349
msgid "Brand Color"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:427
msgid "Brand Transparent"
msgstr ""

#: template-parts/post-loop/content.php:29
msgid "By"
msgstr ""

#: logichunt/plugin-admin.php:38 logichunt/plugin-admin.php:53
msgid "Carousel Slider"
msgstr ""

#: logichunt/plugin-admin.php:36
msgctxt "Carousel Slider"
msgid "Carousel Slider"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:430
msgid "Classic"
msgstr ""

#. Name of the template
msgid "Coming Soon"
msgstr ""

#: comments.php:53 comments.php:74
msgid "Comment navigation"
msgstr ""

#: comments.php:91
msgid "Comments are closed."
msgstr ""

#: post-format/content-single-audio.php:41
#: post-format/content-single-gallery.php:56
#: post-format/content-single-link.php:43
#: post-format/content-single-quote.php:51
#: post-format/content-single-video.php:45 post-format/content-single.php:43
msgid "Comments Closed"
msgstr ""

#. 1: comment count number, 2: title.
#: comments.php:41
#, php-format
msgctxt "comments title"
msgid "%1$s thought on &ldquo;%2$s&rdquo;"
msgid_plural "%1$s thoughts on &ldquo;%2$s&rdquo;"
msgstr[0] ""
msgstr[1] ""

#: lib/theme-main-functions/_helpers.php:254
msgid "Company Name"
msgstr ""

#: lib/theme-main-functions/_helpers.php:262
msgid "Company Url"
msgstr ""

#: lib/theme-config/config-tgm/tgm-config.php:97
msgid "Contact Form 7"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:442
msgid "Container"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:729
#: lib/theme-config/config-redux/_theme-options_old.php:887
msgid "Copyright Area"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:735
#: lib/theme-config/config-redux/_theme-options_old.php:893
msgid "Copyright Text"
msgstr ""

#: lib/theme-config/config-redux/_theme-options_old.php:405
msgid "Custom CSS"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:473
msgid "Dark"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:423
msgid "Default"
msgstr ""

#: lib/theme-config/config-redux/_theme-options_old.php:694
msgid "Dribbble Url"
msgstr ""

#. %s: Name of current post
#: inc/template-tags.php:72
#, php-format
msgid "Edit %s"
msgstr ""

#: logichunt/plugin-admin.php:43
msgid "Edit Carousel Item"
msgstr ""

#. Name of the theme
msgid "NITF"
msgstr ""

#: lib/theme-main-functions/_helpers.php:115
msgid "Enter Comment Here..."
msgstr ""

#: lib/theme-config/config-redux/_theme-options_old.php:787
msgid "Event Address"
msgstr ""

#: lib/theme-config/config-redux/_theme-options_old.php:780
msgid "Event Date"
msgstr ""

#: lib/theme-main-functions/_helpers.php:241
msgid "Extra profile information"
msgstr ""

#: lib/theme-config/config-redux/_theme-options_old.php:624
msgid "Facebook Url"
msgstr ""

#: template-parts/post-loop/content-audio.php:25
#: template-parts/post-loop/content-link.php:28
#: template-parts/post-loop/content-quote.php:36
#: template-parts/post-loop/content-video.php:29
#: template-parts/post-loop/content.php:36
#: template-parts/post-loop/content_old.php:40
msgid "Featured"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:458
msgid "Fixed"
msgstr ""

#: lib/theme-config/config-redux/_theme-options_old.php:665
msgid "Flicker Url"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:443
msgid "Fluid"
msgstr ""

#: lib/theme-main-functions/_helpers.php:270
msgid "Follow"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:685
#: lib/theme-config/config-redux/_theme-options_old.php:712
msgid "Footer Logo"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:676
#: lib/theme-config/config-redux/_theme-options_old.php:703
msgid "Footer Options"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:694
#: lib/theme-config/config-redux/_theme-options_old.php:721
msgid "Footer Settings"
msgstr ""

#: lib/theme-main-functions/_register.php:39
msgid "Footer Widget Bottom"
msgstr ""

#: lib/theme-main-functions/_register.php:27
msgid "Footer Widget Top"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:300
#: lib/theme-config/config-redux/_theme-options_old.php:300
msgid "General Setting"
msgstr ""

#: lib/theme-config/config-redux/_theme-options_old.php:637
msgid "Google Plus Url"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:493
#: lib/theme-config/config-redux/_theme-options_old.php:428
msgid "Header Banner Background Type"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:428
msgid "Header Container"
msgstr ""

#: header/blog.php:12
msgid "Home"
msgstr ""

#. Name of the template
msgid "Home Page Default"
msgstr ""

#. URI of the theme
msgid "#"
msgstr ""

#. Author URI of the theme
msgid "#"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:642
#: lib/theme-config/config-redux/_theme-options_old.php:577
msgid ""
"If you put empty this field, it will display Category,Tag, Date as Archive "
"Title"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:543
#: lib/theme-config/config-redux/_theme-options_old.php:478
msgid "If you put empty this field, it will display Default Value"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:592
#: lib/theme-config/config-redux/_theme-options_old.php:527
msgid "If you put empty this field, it will display Search text"
msgstr ""

#: lib/theme-config/config-redux/_theme-options_old.php:655
msgid "Instagram Url"
msgstr ""

#: 404.php:24
msgid ""
"It looks like nothing was found at this location. Maybe try one of the links "
"below or a search?"
msgstr ""

#: template-parts/content-none.php:32
#: template-parts/post-loop/content-none.php:32
msgid ""
"It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps "
"searching can help."
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:541
#: lib/theme-config/config-redux/_theme-options_old.php:476
msgid "Latest Blog"
msgstr ""

#. %s: post title
#: inc/template-tags.php:65
#, php-format
msgid "Leave a Comment<span class=\"screen-reader-text\"> on %s</span>"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:557
#: lib/theme-config/config-redux/_theme-options.php:605
#: lib/theme-config/config-redux/_theme-options.php:656
#: lib/theme-config/config-redux/_theme-options_old.php:492
#: lib/theme-config/config-redux/_theme-options_old.php:540
#: lib/theme-config/config-redux/_theme-options_old.php:591
msgid "Left"
msgstr ""

#: post-format/content-single-gallery.php:25
msgid "lgx gallery"
msgstr ""

#: lib/theme-config/config-redux/_theme-options_old.php:643
msgid "Linkedin Url"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:394
#: lib/theme-config/config-redux/_theme-options_old.php:394
msgid "Loader Image Width"
msgstr ""

#: lib/theme-config/config-redux/_theme-options_old.php:756
msgid "Location"
msgstr ""

#: lib/theme-config/config-redux/_theme-options_old.php:803
msgid "Location Map Icon Name"
msgstr ""

#: lib/theme-config/config-redux/_theme-options_old.php:811
msgid "Location Map URL"
msgstr ""

#: lib/theme-config/config-redux/_theme-options_old.php:773
msgid "Location Title"
msgstr ""

#: lib/theme-config/config-redux/_theme-options_old.php:795
msgid "Location URL Text"
msgstr ""

#. Author of the theme
msgid "MagicSquare"
msgstr ""

#: lib/theme-config/config-tgm/tgm-config.php:83
msgid "Logo Slider WP"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:327
#: lib/theme-config/config-redux/_theme-options_old.php:327
msgid "Logo Width"
msgstr ""

#: lib/theme-config/config-tgm/tgm-config.php:103
msgid "Mailchimp For WP"
msgstr ""

#: lib/theme-config/config-redux/_theme-options_old.php:878
msgid "Mailchimp Shortcode"
msgstr ""

#: lib/theme-main-functions/_enqueue-assets.php:38
msgid "Main Menu"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:409
msgid "Main Menu Settings. You Can Override all options from Page settings."
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:468
msgid "Menu Color Type"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:429
msgid "Menu Container"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:454
msgid "Menu Position Type"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:408
msgid "Menu Settings"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:419
msgid "Menu Style"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:438
msgid "Menu Width"
msgstr ""

#: comments.php:57 comments.php:78
msgid "Newer Comments"
msgstr ""

#: post-format/content-single-audio.php:69
#: post-format/content-single-gallery.php:84
#: post-format/content-single-link.php:71
#: post-format/content-single-quote.php:79
#: post-format/content-single-speaker.php:91
#: post-format/content-single-video.php:73 post-format/content-single.php:71
msgid "Next"
msgstr ""

#: post-format/content-single-audio.php:70
#: post-format/content-single-gallery.php:85
#: post-format/content-single-link.php:72
#: post-format/content-single-quote.php:80
#: post-format/content-single-speaker.php:92
#: post-format/content-single-video.php:74 post-format/content-single.php:72
msgid "Next post:"
msgstr ""

#: logichunt/plugin-admin.php:46
msgid "No Carousel items found"
msgstr ""

#: logichunt/plugin-admin.php:47
msgid "No Carousel items found in trash"
msgstr ""

#: post-format/content-single-audio.php:37
#: post-format/content-single-gallery.php:52
#: post-format/content-single-link.php:39
#: post-format/content-single-quote.php:47
#: post-format/content-single-video.php:41 post-format/content-single.php:39
msgid "No Comment"
msgstr ""

#: template-parts/content-none.php:23
#: template-parts/post-loop/content-none.php:23
msgid "Nothing Found"
msgstr ""

#: comments.php:56 comments.php:77
msgid "Older Comments"
msgstr ""

#. 1: title.
#: comments.php:35
#, php-format
msgid "One thought on &ldquo;%1$s&rdquo;"
msgstr ""

#: 404.php:20
msgid "Oops! That page can&rsquo;t be found."
msgstr ""

#: logichunt/plugin-admin.php:54
msgid "OWL Carousel Slider Post Type"
msgstr ""

#: template-parts/content-page.php:24
msgid "Page"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:484
#: lib/theme-config/config-redux/_theme-options_old.php:419
msgid "Page Banner"
msgstr ""

#. Name of the template
msgid "Page Left Sidebar"
msgstr ""

#. Name of the template
msgid "Page Right Sidebar"
msgstr ""

#: post-format/content-single-audio.php:53
#: post-format/content-single-gallery.php:68
#: post-format/content-single-link.php:55
#: post-format/content-single-quote.php:63
#: post-format/content-single-speaker.php:75
#: post-format/content-single-video.php:57 post-format/content-single.php:55
#: template-parts/content-page.php:20
msgid "Pages:"
msgstr ""

#: post-format/content-single-audio.php:64
#: post-format/content-single-gallery.php:79
#: post-format/content-single-link.php:66
#: post-format/content-single-quote.php:74
#: post-format/content-single-speaker.php:86
#: post-format/content-single-video.php:68 post-format/content-single.php:66
msgctxt "Parent post link"
msgid ""
"<span class=\"meta-nav\">Published in</span><span class=\"post-title\">"
"%title</span>"
msgstr ""

#: lib/theme-config/config-redux/_theme-options_old.php:406
msgid "Paste your CSS code here."
msgstr ""

#: lib/theme-config/config-redux/_theme-options_old.php:660
msgid "Pinterest Url"
msgstr ""

#: lib/theme-main-functions/_helpers.php:258
msgid "Please enter your company name here."
msgstr ""

#: lib/theme-main-functions/_helpers.php:266
msgid "Please enter your company url here."
msgstr ""

#: lib/theme-main-functions/_helpers.php:250
msgid "Please enter your position here."
msgstr ""

#: lib/theme-main-functions/_helpers.php:274
msgid "Please enter your social url here."
msgstr ""

#: lib/theme-main-functions/_helpers.php:246
msgid "Position"
msgstr ""

#: inc/template-tags.php:33
#, php-format
msgctxt "post author"
msgid "by %s"
msgstr ""

#: inc/template-tags.php:28
#, php-format
msgctxt "post date"
msgid "Posted on %s"
msgstr ""

#: inc/template-tags.php:52
#, php-format
msgid "Posted in %1$s"
msgstr ""

#. Description of the theme
msgid "Premium Event, Conference and Meetup WordPress Theme"
msgstr ""

#: post-format/content-single-audio.php:72
#: post-format/content-single-gallery.php:87
#: post-format/content-single-link.php:74
#: post-format/content-single-quote.php:82
#: post-format/content-single-speaker.php:94
#: post-format/content-single-video.php:76 post-format/content-single.php:74
msgid "Previous"
msgstr ""

#: post-format/content-single-audio.php:73
#: post-format/content-single-gallery.php:88
#: post-format/content-single-link.php:75
#: post-format/content-single-quote.php:83
#: post-format/content-single-speaker.php:95
#: post-format/content-single-video.php:77 post-format/content-single.php:75
msgid "Previous post:"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:396
#: lib/theme-config/config-redux/_theme-options_old.php:396
msgid "Put a numeric value with px. Such As: 150px "
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:329
#: lib/theme-config/config-redux/_theme-options_old.php:329
msgid "Put a numeric value with px. Such As: 190px "
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:736
#: lib/theme-config/config-redux/_theme-options_old.php:894
msgid "Put Copyright Text Here"
msgstr ""

#: lib/theme-config/config-redux/_theme-options_old.php:879
msgid "Put mailchimp shortcode here"
msgstr ""

#: template-parts/post-loop/content.php:33
#: template-parts/post-loop/content_old.php:37
msgid "Read More"
msgstr ""

#: template-parts/post-loop/content-audio.php:29
#: template-parts/post-loop/content-link.php:32
#: template-parts/post-loop/content-quote.php:40
#: template-parts/post-loop/content-video.php:33
msgid "Read More ..."
msgstr ""

#: template-parts/content-none.php:18
#: template-parts/post-loop/content-none.php:18
#, php-format
msgid ""
"Ready to publish your first post? <a href=\"%1$s\">Get started here</a>."
msgstr ""

#: lib/theme-config/config-tgm/tgm-config.php:77
msgid "Redux Framework"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:559
#: lib/theme-config/config-redux/_theme-options.php:607
#: lib/theme-config/config-redux/_theme-options.php:658
#: lib/theme-config/config-redux/_theme-options_old.php:494
#: lib/theme-config/config-redux/_theme-options_old.php:542
#: lib/theme-config/config-redux/_theme-options_old.php:593
msgid "Right"
msgstr ""

#: logichunt/plugin-admin.php:45
msgid "Search Carousel"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:573
#: lib/theme-config/config-redux/_theme-options_old.php:508
msgid "Search Page"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:590
#: lib/theme-config/config-redux/_theme-options_old.php:525
msgid "Search Post"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:601
#: lib/theme-config/config-redux/_theme-options_old.php:536
msgid "Search Sidebar"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:588
#: lib/theme-config/config-redux/_theme-options_old.php:523
msgid "Search Title"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:350
#: lib/theme-config/config-redux/_theme-options.php:357
#: lib/theme-config/config-redux/_theme-options_old.php:350
#: lib/theme-config/config-redux/_theme-options_old.php:357
msgid "Select a color you like for your theme.."
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:469
msgid "Set Default Menu Color of Header Menu."
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:455
msgid "Set Default Menu Type of Header Menu."
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:439
msgid "Set Default Menu Width of Header Menu."
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:420
msgid "Set Default Style of Header Menu."
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:424
msgid "Shadow"
msgstr ""

#: post-format/content-single-video.php:86
msgid "Share"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:365
#: lib/theme-config/config-redux/_theme-options_old.php:365
msgid "Show Loader Icon"
msgstr ""

#: lib/theme-config/config-redux/_theme-options_old.php:762
msgid "Show Location"
msgstr ""

#: lib/theme-config/config-redux/_theme-options_old.php:827
msgid "Show Social Icon"
msgstr ""

#: lib/theme-config/config-redux/_theme-options_old.php:860
msgid "Show Subscription"
msgstr ""

#: lib/theme-main-functions/_register.php:16
msgid "Sidebar"
msgstr ""

#: logichunt/plugin-admin.php:37
msgctxt "Slider Items"
msgid "Slider Item"
msgstr ""

#: lib/theme-config/config-tgm/tgm-config.php:60
msgid "Slider Revolution"
msgstr ""

#: lib/theme-config/config-redux/_theme-options_old.php:845
msgid "Social Area Text"
msgstr ""

#: lib/theme-config/config-redux/_theme-options_old.php:838
msgid "Social Area Title"
msgstr ""

#: lib/theme-config/config-redux/_theme-options_old.php:618
msgid "Social Icon"
msgstr ""

#: lib/theme-config/config-redux/_theme-options_old.php:607
msgid "Social Media"
msgstr ""

#: lib/theme-config/config-redux/_theme-options_old.php:821
msgid "Social Media Settings"
msgstr ""

#: template-parts/content-none.php:26
#: template-parts/post-loop/content-none.php:26
msgid ""
"Sorry, but nothing matched your search terms. Please try again with some "
"different keywords."
msgstr ""

#: lib/theme-config/config-redux/_theme-options_old.php:649
msgid "SoundCloud Url"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:459
msgid "Static"
msgstr ""

#: lib/theme-main-functions/_helpers.php:124
msgid "Submit"
msgstr ""

#: lib/theme-config/config-redux/_theme-options_old.php:854
msgid "Subscription Area"
msgstr ""

#: lib/theme-config/config-redux/_theme-options_old.php:871
msgid "Subscription Title"
msgstr ""

#: inc/template-tags.php:58
#, php-format
msgid "Tagged %1$s"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:266
#: lib/theme-config/config-redux/_theme-options_old.php:266
msgid "Theme Information 1"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:276
#: lib/theme-config/config-redux/_theme-options_old.php:276
msgid "Theme Information 2"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:35
#: lib/theme-config/config-redux/_theme-options.php:37
#: lib/theme-config/config-redux/_theme-options_old.php:35
#: lib/theme-config/config-redux/_theme-options_old.php:37
msgid "Theme Options"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:440
#: lib/theme-config/config-redux/_theme-options.php:456
#: lib/theme-config/config-redux/_theme-options.php:470
msgid "This Settings Can Override from Page."
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:700
#: lib/theme-config/config-redux/_theme-options_old.php:727
msgid "Top Footer Background Type"
msgstr ""

#: lib/theme-config/config-redux/_theme-options_old.php:670
msgid "Tumblr Url"
msgstr ""

#: lib/theme-config/config-redux/_theme-options_old.php:631
msgid "Twitter Url"
msgstr ""

#: logichunt/plugin-admin.php:44
msgid "Update Carousel Item"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:508
#: lib/theme-config/config-redux/_theme-options.php:713
#: lib/theme-config/config-redux/_theme-options_old.php:443
#: lib/theme-config/config-redux/_theme-options_old.php:740
msgid "Upload Image for Page banner. Default image size is 1920*613"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:315
#: lib/theme-config/config-redux/_theme-options_old.php:315
msgid "Upload Logo"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:382
#: lib/theme-config/config-redux/_theme-options_old.php:382
msgid "Upload Pre Loader Image"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:338
#: lib/theme-config/config-redux/_theme-options_old.php:338
msgid "Use Custom Brand and Button Color"
msgstr ""

#: lib/theme-config/config-tgm/tgm-config.php:70
msgid "VCX Theme Core"
msgstr ""

#: logichunt/plugin-admin.php:40
msgid "View Item"
msgstr ""

#: lib/theme-config/config-redux/_theme-options_old.php:682
msgid "Vimeo Url"
msgstr ""

#: lib/theme-main-functions/_helpers.php:365
msgid "Website"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:472
msgid "White"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:426
msgid "white Transparent"
msgstr ""

#: lib/theme-config/config-tgm/tgm-config.php:109
msgid "Widget Importer Exporter"
msgstr ""

#: lib/theme-config/config-tgm/tgm-config.php:89
msgid "WP Counter Up"
msgstr ""

#: lib/theme-config/config-tgm/tgm-config.php:49
msgid "WPBakery Visual Composer"
msgstr ""

#: lib/theme-config/config-redux/_theme-options.php:421
msgid "You Can Override This options from Page settings."
msgstr ""

#: lib/theme-main-functions/_helpers.php:359
msgid "Your Email"
msgstr ""

#: lib/theme-main-functions/_helpers.php:353
msgid "Your Name"
msgstr ""

#: lib/theme-config/config-redux/_theme-options_old.php:676
msgid "Youtube Url"
msgstr ""

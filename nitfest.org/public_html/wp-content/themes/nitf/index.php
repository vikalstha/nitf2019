<?php

$emeet_opt = new LgxFrameworkOpt();
$emeet_blogbar = $emeet_opt->emeet_blogSidebar();

get_header();

?>

<?php get_template_part('header/blog'); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" >

            <div class="lgx-inner lgx-page-wrapper lgx-page-main lgx-page-<?php echo is_active_sidebar( 'sidebar-1' ) ? 'right-sidebar' : 'blog-full' ; ?>">

                <div class="container">
                    <div class="blog-area">
                        <div class="row">

                            <div class="col-sm-<?php echo is_active_sidebar( 'sidebar-1' ) ? 8 : 12 ; ?> ">
                                <div class="lgx-card-wrapper">
                                    <div id="lgx-masonry-wrapper" class="lgx-masonry-area">
                                        <?php
                                        if ( have_posts() ) :
                                            /* Start the Loop */
                                            while ( have_posts() ) : the_post();
                                                /*
                                                 * Include the Post-Format-specific template for the content.
                                                 * If you want to override this in a child theme, then include a file
                                                 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                                 */
                                                get_template_part( 'template-parts/post-loop/content', get_post_format() );
                                            endwhile;
                                            ?>

                                        <?php

                                        else :
                                            get_template_part( 'template-parts/post-loop/content', 'none' );
                                        endif; ?>
                                    </div>
                                </div>

                                <div class="lgx-pagination clearfix">
                                    <?php emeet_pagination(); ?>
                                </div>
                            </div>
                            <?php if ( is_active_sidebar( 'sidebar-1' )): ?>
                            <div class="col-sm-4">
                                <!-- Start: Sidebar -->
                                <?php get_sidebar(); ?>
                                <!-- End: Sidebar -->
                            </div>
                            <?php endif; ?>

                        </div>
                    </div>
                </div><!-- //.CONTAINER -->


            </div><!-- //.INNER -->



        </main><!-- #main -->
    </div><!-- #primary -->

<?php get_footer();     ?>


<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

add_shortcode( 'vcx_countdown', 'vcx_countdown_function');



/**
 * Short Code
 * @param $atts
 * @return string
 */

function vcx_countdown_function($atts) {
    extract(shortcode_atts(array(
        'count_type'           => 'simple',
        'vcx_count_date'       => '2019/12/15 0:0:0',
        'vcx_text_day'         => 'Days',
        'vcx_text_hour'        => 'Hours',
        'vcx_text_min'         => 'Minutes',
        'vcx_text_sec'         => 'Seconds',
        'cir_bg_color'        => '#dddddd',
        'day_color'           => '#ec398b',
        'hour_color'          => '#fac400',
        'min_color'           => '#00acee',
        'sec_color'           => '#483fa1',
        'cd_text_color'       => '#ffffff',
        'count_align'         => 'left',
        'count_style'         => 'default',
    ), $atts));


    // CountDown
    $simple_countdown = '<div id="vcx-countdown-simple" class="lgx-countdown lgx-countdown-section " ></div>';

    $circuler_coundown = '<div id="vcx-countdown-circular" class="lgx-countdown lgx-countdown-section" data-date="'. esc_html($vcx_count_date) .'" style="color: '.esc_html($cd_text_color).';" ></div>';



    $countdown_html = '';

    if( !empty($count_type)) {

        switch ($count_type) {

            case "circular":
                $count_type_html = $circuler_coundown;
                break;

            case "simple":
                $count_type_html = $simple_countdown;
                break;

            case "none":
                $count_type_html = '';
                break;

            default:
                $count_type_html = $simple_countdown;
        }

        $class_name = ($count_type == "circular" ) ?  'circular' : 'simple';


        $countdown_html = ($count_type != 'none') ? '<div id="vcx-section-countdown" class="vcx-section-countdown lgx-countdown-'.$class_name.' section-countdown-align-'.$count_align.' section-countdown-style-'.$count_style.'  "
        data-date="'. esc_html($vcx_count_date) .'"
        data-vday="'.esc_html($vcx_text_day).'"
        data-vhour="'.esc_html($vcx_text_hour).'" 
        data-vmin="'.esc_html($vcx_text_min).'"
        data-vsec="'.esc_html($vcx_text_sec).'"  
        data-dcolor="'.esc_attr($day_color).'"  
        data-hcolor="'.esc_attr($hour_color).'"  
        data-mcolor="'.esc_attr($min_color).'"  
        data-scolor="'.esc_attr($sec_color).'"  
        data-cbgcolor="'.esc_attr($cir_bg_color).'" 
        data-textcolor="'.esc_attr($cd_text_color).'"   
        >'.$count_type_html.'</div>' : '';

    }



    return $countdown_html;
 
}


// VC Addons goes to theme
/**
 * Visual Composer
 */

if (class_exists('WPBakeryVisualComposerAbstract')) {
    vc_map(array(
        "name" => esc_html__("Section Countdown", 'vcx-theme-core'),
        "base" => "vcx_countdown",
        // 'icon' => 'icon_travel_info',
        "class" => "",
        "description" => esc_html__("Display Cicrcle Countdown", 'vcx-theme-core'),
        "category" => esc_html__('Emeet', 'vcx-theme-core'),
        "params" => array(

            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("Countdown Type", 'vcx-theme-core'),
                "param_name" 	=> "count_type",
                "value" 		=> array('Simple'=>'simple', 'Circular'=>'circular', 'None'=>'none'),
            ),

            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("Alignment", 'vcx-theme-core'),
                "param_name" 	=> "count_align",
                "value" 		=> array('Left'=>'left', 'Center'=>'center', 'Right'=>'right'),
            ),


            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("Style", 'vcx-theme-core'),
                "param_name" 	=> "count_style",
                "value" 		=> array('Default'=>'default', 'Border'=>'border', 'Dot'=>'dot','Square'=>'squre'),
            ),


            array(
                "type" 			=> "textfield",
                "admin_label"   => true,
                "heading" 		=> esc_html__("Countdown Date", "vcx-theme-core"),
                "description"   => esc_html__("Important: Date Format Must be: Y/m/d (Year/ Month/ Date). For Example: 2019/10/5 0:0:0", "vcx-theme-core"),
                "param_name" 	=> "vcx_count_date",
                "value" 		=> '2019/12/15 0:0:0',
            ),

            array(
                "type" 			=> "textfield",
                "admin_label"   => true,
                "heading" 		=> esc_html__("Day Text", "vcx-theme-core"),
                "param_name" 	=> "vcx_text_day",
                "value" 		=> 'Days',
            ),

            array(
                "type"          => "colorpicker",
                "heading"       => esc_html__("Day Color", "vcx-theme-core"),
                "param_name"    => "day_color",
                "value"         => "#ec398b",
            ),

            array(
                "type" 			=> "textfield",
                "admin_label"   => true,
                "heading" 		=> esc_html__("Hour Text", "vcx-theme-core"),
                "param_name" 	=> "vcx_text_hour",
                "value" 		=> 'Hours',
            ),

            array(
                "type"          => "colorpicker",
                "heading"       => esc_html__("Hour Color", "vcx-theme-core"),
                "param_name"    => "hour_color",
                "value"         => "#fac400",
            ),

            array(
                "type" 			=> "textfield",
                "admin_label"   => true,
                "heading" 		=> esc_html__("Minute Text", "vcx-theme-core"),
                "param_name" 	=> "vcx_text_min",
                "value" 		=> 'Minutes',
            ),

            array(
                "type"          => "colorpicker",
                "heading"       => esc_html__("Minute Color", "vcx-theme-core"),
                "param_name"    => "min_color",
                "value"         => "#00acee",
            ),

            array(
                "type" 			=> "textfield",
                "admin_label"   => true,
                "heading" 		=> esc_html__("Seconds Text", "vcx-theme-core"),
                "param_name" 	=> "vcx_text_sec",
                "value" 		=> 'Seconds',
            ),


            array(
                "type"          => "colorpicker",
                "heading"       => esc_html__("Seconds Color", "vcx-theme-core"),
                "param_name"    => "sec_color",
                "value"         => "#483fa1",
            ),

            array(
                "type"          => "colorpicker",
                "heading"       => esc_html__("Text Color", "vcx-theme-core"),
                "param_name"    => "cd_text_color",
                "value"         => "#ffffff",
            ),

            array(
                "type"          => "colorpicker",
                "heading"       => esc_html__("Circular BG Color", "vcx-theme-core"),
                "param_name"    => "cir_bg_color",
                "value"         => "#dddddd",
            ),
        )
    ));
}
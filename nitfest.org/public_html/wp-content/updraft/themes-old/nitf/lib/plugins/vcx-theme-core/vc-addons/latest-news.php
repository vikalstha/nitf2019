<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

add_shortcode( 'vcx_latest_news', 'vcx_latest_news_function');


/**
 * Short Code
 * @param $atts
 * @return string
 */

function vcx_latest_news_function($atts)
{
    $category 	= '';
    $number 	= 3;
    $order_by	= 'date';
    $order		= 'DESC';

    extract(shortcode_atts(array(
        'category' 		=> '',
        'number' 		=>  3,
        'order_by'		=> 'date',
        'order'			=> 'DESC',
        'style'			=> 'grid',
    ), $atts));

    global $post;
    global $wpdb;


    // Basic Query
    $args = array(
        'post_status'		=> 'publish',
        'posts_per_page'	=> esc_attr($number),
        'order'				=> $order,
        'orderby'			=> $order_by
    );

    // Category Add
    if( ( $category != '' )){
        $args2 = array(
            'tax_query' => array(
                array(
                    'taxonomy' => 'category',
                    'field'    => 'id',
                    'terms'    => $category,
                ),
            ),
        );
        $args = array_merge( $args,$args2 );
    }

    ob_start(); ?>

    <div class="lgx-news-section">
        <div class="lgx-wrapper vcx-news-style-<?php echo esc_attr($style) ?> ">
            <div class="vcx-news-row">
                <?php
                $data = new WP_Query($args);
                if ( $data->have_posts() ) {
                    while ( $data->have_posts() ) {
                        $data->the_post();



                        $thumb_url = '';
                        if ( has_post_thumbnail( $post->ID ) ) {

                            update_post_thumbnail_cache();
                            $thumb_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'eplano-blog-medium', true);
                            $thumb_url = $thumb_url[0];
                        }


                        ?>
                        <div class="vcx-news-col">
                            <div class="lgx-single-news">
                                <figure>
                                    <a href="<?php echo get_the_permalink(); ?>" target="_blank">
                                        <img src="<?php echo esc_url( $thumb_url); ?>" alt="<?php echo get_the_title(); ?>"/>
                                    </a>
                                </figure>
                                <div class="single-news-info">
                                    <div class="meta-wrapper">
                                        <span class="vcx-news-date"><?php echo date_i18n(get_option('date_format'), false, false);; ?></span>
                                        <span class="vcx-post-author"><?php esc_html_e('By', 'vcx-theme-core') ?> <?php the_author(); ?></span>
                                    </div>
                                    <h2 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                                    <div class="vcx-excerpt"><?php the_excerpt(); ?></div>
                                    <a class="lgx-btn lgx-btn-white lgx-btn-sm" href="<?php the_permalink(); ?>"><span><?php esc_html_e('Read More', 'vcx-theme-core') ?></span></a>
                                </div>
                            </div>
                        </div><!--// Single -->

                    <?php      }
                }
                wp_reset_postdata();// Restore original Post Data
                ?>
            </div><!-- //row  -->
        </div>
    </div>

    <?php
    return ob_get_clean();
}

/**
 * Visual Composer
 */



if (class_exists('WPBakeryVisualComposerAbstract')) {
    vc_map(array(
        "name" => esc_html__("Latest News", 'vcx-theme-core'),
        "base" => "vcx_latest_news",
        // 'icon' => 'icon_travel_info',
        "class" => "",
        "description" => esc_html__("Display Latest News.", 'vcx-theme-core'),
        "category" => esc_html__('Emeet', 'vcx-theme-core'),
        "params" => array(
            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("Category Filter", 'vcx-theme-core'),
                "param_name" 	=> "category",
                "value" 		=> vcx_get_post_category(),
            ),

            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("Style", 'vcx-theme-core'),
                "param_name" 	=> "style",
                "value" 		=> array('Grid'=>'grid','List'=>'list'),
            ),

            array(
                "type" 			=> "textfield",
                "heading" 		=> esc_html__("Number of items", 'vcx-theme-core'),
                "param_name" 	=> "number",
                "value" 		=> 3,
            ),

            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("OderBy", 'vcx-theme-core'),
                "param_name" 	=> "order_by",
                "value" 		=> array('Select'=>'','Date'=>'date','Title'=>'title','Modified'=>'modified','Author'=>'author','Random'=>'rand'),
            ),

            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("Order", 'vcx-theme-core'),
                "param_name" 	=> "order",
                "value" 		=> array('Select'=>'','DESC'=>'DESC','ASC'=>'ASC'),
            ),
        )

    ));
}
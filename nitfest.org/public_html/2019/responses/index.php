<?php


if(isset($_GET['token'])){
    $token = $_GET['token'];
    $key='manaatakgarchu';
    if($token != $key){
       die('Invalid Token. Please Ask admin for token');
    }
    else{
        

require 'vendor/autoload.php';

$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
$reader->setReadDataOnly(true);
$spreadsheet = $reader->load("Application Form (Responses).xlsx");

$sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
unset($sheetData[1]);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Registration List of Google Form</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="css/style.min.css" rel="stylesheet">
</head>

<body class="grey lighten-3">

    <!--Main Navigation-->
    <header>

        <!-- Navbar -->
        <nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
            <div class="container">

                <!-- Brand -->
                <a class="navbar-brand waves-effect" href="" target="_blank">
                    <!-- <img src="https://nitfest.org/2019/assets/img/logo.png" class="img-fluid" alt=""> -->
                    <strong class="blue-text">NITFEST 2019 Play Registrations Entries </span></strong>
                </a>

                  

            </div>
        </nav>
        <!-- Navbar -->

    </header>
    <!--Main Navigation-->

    <!--Main layout-->
    <main class="mt-5 pt-5">
        <div class="container">

            <!--Section: Post-->
            <section class="mt-4">

                <!--Grid row-->
                <div class="row">

                    <!--Grid column-->
                    <div class="col-md-12 mb-4">

                        <!--Card : Dynamic content wrapper-->
                        <div class="card mb-4 text-center wow fadeIn">

                            <div class="card-header">
                                <?= count($sheetData) ?> Entries till 01/01/2019 00:00:00
                            </div>
                            
                            <!--Card content-->
                            <div class="card-body">
                            	<div class="list-group-flush text-left">
                                
                                <?php foreach ($sheetData as $k => $r) { ?>
                                   
								  <div class="list-group-item pl-0">
								    <p class="mb-0"><i class="far fa-image mr-2 grey p-1 white-text rounded-circle " aria-hidden="true"></i> <a target="_blank" href="response.php?token=manaatakgarchu&response_id=<?=$k?>"><?= $k-1 ?> : <?=$r['C']?> </a></p>
								  </div>
								<?php } ?>

								</div>
                            </div>

                        </div>
                        <!--/.Card : Dynamic content wrapper-->


                    </div>
                    <!--Grid column-->

                </div>
                <!--Grid row-->

            </section>
            <!--Section: Post-->

        </div>
    </main>
    <!--Main layout-->

    <!--Footer-->
    <footer class="page-footer text-center font-small mdb-color darken-2 mt-4 wow fadeIn">

        <hr class="my-4">

        <!-- Social icons -->
        <div class="pb-4">
            <a href="" target="_blank">
                <i class="fab fa-facebook-f mr-3"></i>
            </a>

            <a href="" target="_blank">
                <i class="fab fa-twitter mr-3"></i>
            </a>

            <a href="" target="_blank">
                <i class="fab fa-youtube mr-3"></i>
            </a>

            <a href="" target="_blank">
                <i class="fab fa-google-plus-g mr-3"></i>
            </a>

            <a href="" target="_blank">
                <i class="fab fa-dribbble mr-3"></i>
            </a>

            <a href="" target="_blank">
                <i class="fab fa-pinterest mr-3"></i>
            </a>

            <a href="" target="_blank">
                <i class="fab fa-github mr-3"></i>
            </a>

            <a href="" target="_blank">
                <i class="fab fa-codepen mr-3"></i>
            </a>
        </div>
        <!-- Social icons -->

        <!--Copyright-->
        <div class="footer-copyright py-3">
            © 2018/19 Copyright:
            <a href="" target="_blank"> Nitfest 2019 </a>
        </div>
        <!--/.Copyright-->

    </footer>
    <!--/.Footer-->

    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb.min.js"></script>
    <!-- Initializations -->
    <script type="text/javascript">
        // Animations initialization
        new WOW().init();
    </script>
</body>

</html>

<?php
  } 
}
?>




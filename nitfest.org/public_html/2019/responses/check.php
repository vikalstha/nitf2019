<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Check Your Registration : Nitfest 2019 </title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css">
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="css/mdb.min.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="css/style.min.css" rel="stylesheet">
  <style type="text/css">
    html,
    body,
    header,
    .view {
      height: 100%;
    }

    @media (max-width: 740px) {
      html,
      body,
      header,
      .view {
        height: 1000px;
      }
    }

    @media (min-width: 800px) and (max-width: 850px) {
      html,
      body,
      header,
      .view {
        height: 650px;
      }
    }
    @media (min-width: 800px) and (max-width: 850px) {
              .navbar:not(.top-nav-collapse) {
                  background: #1C2331!important;
              }
          }
  </style>
</head>

<body>

  <!-- Navbar -->
  <nav class="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar">
    <div class="container">

      <!-- Brand -->
      <a class=" black-text navbar-brand" href="" target="_blank">
        <strong>Nitfest 2019</strong>
      </a>
      

    </div>
  </nav>
  <!-- Navbar -->

  <!-- Full Page Intro -->
  <div class="view full-page-intro">

    <!-- Mask & flexbox options-->
    <div class="mask d-flex justify-content-center align-items-center">

      <!-- Content -->
      <div class="container">

        <!--Grid row-->
        <div class="row wow fadeIn">

          <!--Grid column-->
          <div class="col-md-6 mb-4 text-center text-md-left">

            <h1 class="display-4 font-weight-bold">Nitfest 2019 : Registration Entry Check</h1>

            <hr class="hr-light">

            <p>
              <strong>Let us know, if anything in your registration seems missing, or incorrect. Or if you wish to withdraw your entry. <br><span class="red-text">Please, also make sure  links you have provided in your form are publicly accessible. For instance, if its a google drive file, make sure its permission is set to public Or if its a youtube url, please make sure its publicly available. Dead / Private links entry may void your registration.</span>
              
              <br>Mail us your query at <a href="mailto:festivalcell@nitfest.org">festivalcell@nitfest.org</a></strong>
            </p>

          </div>
          <!--Grid column-->

          <!--Grid column-->
          <div class="col-md-6 col-xl-5 mb-4">

            <!--Card-->
            <div class="card">

              <!--Card content-->
              <div class="card-body">

                <!-- Form -->
                <form action="check-registration.php" method="GET">
                  <!-- Heading -->
                  <h3 class="dark-grey-text text-center">
                    <strong>Registration Check:</strong><br>
                    <small style="font-size: 12px;">Enter the email you used while registering.</small>
                  </h3>
                  <hr>
                  <div class="md-form">
                    <i class="fas fa-envelope prefix grey-text"></i>
                    <input type="email" name="email" id="form2" class="form-control" required>
                    <label for="form2">Your email</label>
                  </div>

                  <div class="text-center">
                    <button type="submit" class="btn btn-indigo">Check</button>
                    <hr>
                  
                  </div>

                </form>
                <!-- Form -->

              </div>

            </div>
            <!--/.Card-->

          </div>
          <!--Grid column-->

        </div>
        <!--Grid row-->

      </div>
      <!-- Content -->

    </div>
    <!-- Mask & flexbox options-->

  </div>
  <!-- Full Page Intro -->

  <!--Footer-->
  <footer class="page-footer text-center font-small mdb-color darken-2 wow fadeIn">

        <!--Copyright-->
        <div class="footer-copyright py-3">
            © 2018/19 Copyright:
            <a href="" target="_blank"> Nitfest 2019 </a>
        </div>
        <!--/.Copyright-->

    </footer>
  <!--/.Footer-->

  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="js/mdb.min.js"></script>
  <!-- Initializations -->
</body>

</html>

��    2      �      <      <     =  	   L     V     ^  
   p     {     �     �  	   �     �     �     �     �     �  4   �  �        �     �     �     �     �  
   �     �     �     �     �     �       
             $     1     ?     H     O     e     r     y     �     �  �   �     9     B  	   G     Q     Y     e     l  	   u  �        s     �  
   �     �     �     �     �     �     �     �     �     �  	          I   &  �   p     (	      0	     Q	     j	     r	  
   ~	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
  	   
     %
     @
     P
     Y
  	   a
     k
  �   }
     ?     L     Q     ]     j     v     }     �   Active Filters Afternoon All Day Available Filters Checkboxes City Closed Collapse Filters Cost (%s) Country Day Dropdown Evening Event Category Events are considered free when cost field is: %s %s Expand an active filter to edit the label and choose from a subset of input types (dropdown, select, range slider, checkbox and radio). Filters Filters Default State Filters Layout Free Friday Horizontal Monday Morning Narrow Your Results Night Only when set to zero Open Organizers Other Range Slider Reset Filters Saturday Select Show Advanced Filters Show Filters Submit Sunday Tags The Events Calendar The settings below allow you to enable or disable front-end event filters. Uncheck the box to hide the filter. Drag and drop active filters to re-arrange them. Thursday Time Title: %s Tuesday Type: %s %s Venues Vertical Wednesday PO-Revision-Date: 2017-01-27 16:27:27+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/1.0-alpha-1100
Project-Id-Version: Filter Bar
 Filtros Ativos Tarde Todo o dia Filtros disponíveis Caixas de verificação Cidade Fechado Fechar filtros Preço (%s) País Dia Caixa de seleção Anoitecer Categoria de eventos Os eventos são definidos como grátis quando o campo do preço é: %s %s Clique para expandir um filtro ativo e editar o nome ou escolher a partir de um conjunto de campos (caixas de seleção, caixas de verificação, botões 'radio' e barras deslizantes) Filtros Estado pré-definido dos filtros Disposição dos filtros Grátis sexta-feira Horizontal segunda-feira Manhã Refine a sua pesquisa Noite Apenas quando definido a zero Aberto Organizadores Outro Barra deslizante Limpar filtros sábado Selecione Mostrar filtros avançados Mostrar filtros Submeter domingo Etiquetas Agenda de Eventos As definições abaixo permitem ativar ou desativar filtros de eventos na área pública do site. Desmarque a caixa para esconder o filtro. Arraste e largue os filtros ativos para os reordenar. quinta-feira Hora Título: %s terça-feira Tipo: %s %s Locais Vertical quarta-feira 
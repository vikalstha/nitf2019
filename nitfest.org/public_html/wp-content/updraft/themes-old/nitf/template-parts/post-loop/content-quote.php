<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Emeet
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('lgx-post-loop'); ?>>
    <div class="single-news single-news-list lgx-single-card lgx-single-news">
        <?php $lgx_post_quote = esc_html(get_post_meta(get_the_ID(),'__vcx__post-format-quote',true)); ?>
        <?php $lgx_post_quote_author = esc_html(get_post_meta(get_the_ID(),'__vcx__post-format-quote-author',true)); ?>

        <?php if(!empty($lgx_post_quote)): ?>
            <div class="lgx-featured-wrap lgx-post-quote">
                <blockquote>
                    <p><?php echo esc_html($lgx_post_quote); ?></p>

                    <?php if(!empty($lgx_post_quote_author)): ?>

                        <footer><?php echo esc_html($lgx_post_quote_author); ?></footer>

                    <?php endif; ?>

                </blockquote>
            </div>
        <?php endif; ?>


        <div class="news-content">
            <h2 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            <?php   if ( is_sticky() ) {
                printf( '<span class="featured-post">%s</span>', esc_html__( 'Featured', 'emeet' ) );
            } ?>

            <?php the_excerpt(); ?>
            <a class="readmore" href="<?php the_permalink(); ?>"><?php esc_html_e('Read More ...', 'emeet'); ?> </a>
        </div>
    </div>
</article>

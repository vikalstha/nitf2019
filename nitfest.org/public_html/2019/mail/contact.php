<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <title>Qualtosoft</title>
    
  
   <link rel="apple-touch-icon" sizes="57x57" href="img/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="img/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="img/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="img/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="img/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="img/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192" href="img/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="img/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
<link rel="manifest" href="img/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#000">
<meta name="msapplication-TileImage" content="img/favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
<!--Carousel files-->
<link rel="stylesheet" type="text/css" href="slick/slick.css">
<link rel="stylesheet" type="text/css" href="slick/slick-theme.css">
<!-- Custom styles for Qualtosoft -->
<link rel="stylesheet" href="css/animate.css">
<link href="css/hover-min.css" rel="stylesheet" media="all">
<link rel="stylesheet" type="text/css" href="css/bootstrap-inputs-min.css" />
<link rel="stylesheet" type="text/css" href="css/javascript.fullPage.css" />
<link href="css/style.css" rel="stylesheet">
<!--Font Styling-->
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Abel" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<script src="js/ie-emulation-modes-warning.js"></script>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  
    <link href="css/parsley.css" rel="stylesheet">
    
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/parsley.min.js"></script>
    
</head>

<body id="contact">
    <?php
        include ('inc/header.php');
    ?>
        <!-- End Navigation-->
        <section class="heading content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Contact Us</h1>
                        <ol class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li class="active">Contact Us</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="contact content">
            <div class="container">

        <?php if($_GET['msg']=='success'){ ?>
                    <div class="alert alert-success">
                          <strong>Thankyou!</strong> Your message has been recieved. We will reply you shortly. Keep exploring our services.
                    </div>

        <?php }
        if($_GET['msg']=='error'){ ?>
            <div class="alert alert-danger">
              <strong>Error!</strong> Your message couldnt be recieved. Can you recheck your form entry?.
            </div>

        <?php }
        ?>
                <div class="row">
                    <div class="col-sm-5">
                        <div class="material-form contentBox">
                       <h3>Get in Contact</h3>
                            <div class="form-inner">
                                <form method="POST" action="contact_mail.php" id="contactform">
                                    <div class="group">
                                        <input type="text" name="name" required> <span class="highlight"></span> <span class="bar"></span>
                                        <label>Name</label>
                                    </div>
                                    <div class="group">
                                        <input type="email" name="email" data-parsley-trigger="change" data-parsley-error-message="<small style='color:red'>Is it a valid email ID?</small>" required> <span class="highlight"></span> <span class="bar"></span>
                                        <label>Email</label>
                                    </div>
                                    <div class="group">
                                        <input type="text" name="subject" required> <span class="highlight"></span> <span class="bar"></span>
                                        <label>Subject</label>
                                    </div>
                                    <div class="group">
                                        <textarea name="message" type="text" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="500" data-parsley-minlength-message="<small style='color:red'>Come on! You can send us more message than this..</small>" data-parsley-validation-threshold="10" required></textarea> <span class="highlight"></span> <span class="bar"></span>
                                        <label>Message</label>
                                    </div>
                                    <div class="group">
                                    <script src='https://www.google.com/recaptcha/api.js'></script>
					<div class="g-recaptcha" data-callback="recaptchaCallback" data-sitekey="6LdCKBMUAAAAAIoKBoX0rymPMi1y6Wg8AkypJ3ke"></div>
				    </div>
                                    <div class="group">
                                        <input id="submitBtn" class="btn" type="submit" class="submit" value="Send Message" disabled> </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="contentBox">
                        <h3>Connect with us</h3>
                            <a href="#!"><i class="fa fa-facebook fa-2x"></i></a>
                            <a href="#!"><i class="fa fa-twitter fa-2x"></i></a>
                            <a href="#!"><i class="fa fa-google-plus fa-2x"></i></a>
                        </div>
                        <div class="contentBox">
                        <h3>Other Way</h3>
                           <ul>
                               <li class="location">B.No.79, 1st Floor Radhe Marg, Dilli Bazar Kathmandu (Nepal)</li>
                               <li class="phone">01-4430950 / 4434202</li>
                               <li class="email">info@qualtosoft.com / qualtosoft@gmail.com</li>
                           </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <script>
         $('#contactform').parsley();
         
function recaptchaCallback() {
   $('#submitBtn').removeAttr('disabled');
};

</script>

      <?php
        include ('inc/footer.php');
        
    ?>
     
   
    
    <script src="js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            var menu = $('.navbar');
            var origOffsetY = menu.offset().top;

            function scroll() {
                if ($(window).scrollTop() >= origOffsetY) {
                    $('.navbar').addClass('navbar-fixed-top');
                }
                else {
                    $('.navbar').removeClass('navbar-fixed-top');
                }
            }
            document.onscroll = scroll;
        });
    </script>
    
    <script src="js/inputs-min.js" type="text/javascript"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
    <script src="slick/slick.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript">
        $(document).on('ready', function () {
            $(".regular").slick({
                dots: true
                , infinite: true
                , centerMode: false
                , slidesToShow: 8
                , slidesToScroll: 1
                , autoplay: true
                , autoplaySpeed: 2000
                , arrows: false
                , responsive: [
                    {
                        breakpoint: 1024
                        , settings: {
                            slidesToShow: 9
                            , slidesToScroll: 3
                            , infinite: true
                            , dots: true
                        }
    }
                        , {
                        breakpoint: 600
                        , settings: {
                            slidesToShow: 4
                            , slidesToScroll: 2
                            , centerMode: true
                        , }
    }
                        , {
                        breakpoint: 480
                        , settings: {
                            slidesToShow: 3
                            , slidesToScroll: 1
                            , centerMode: true
                        , }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
            });
        });
    </script>

    
    
</body>

</html>
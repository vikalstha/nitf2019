<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

add_shortcode( 'vcx_video_embed', 'vcx_video_embed_function');


/**
 * Short Code
 * @param $atts
 * @return string
 */

function vcx_video_embed_function($atts) {
    extract(shortcode_atts(array(
        'video_id' 	=>	'oSPR5Go05Vg',
    ), $atts));


    $output = '<div class="lgx-video-area">
                       <div class="lgx-videoicon-area">
                            <a href="#" class="ripple-block" id="myModalLabel2" data-toggle="modal" data-target="#lgx-modal2">
                                <i class="fa fa-play" aria-hidden="true"></i>
                                <div class="ripple ripple-1"></div>
                                <div class="ripple ripple-2"></div>
                                <div class="ripple ripple-3"></div>
                            </a>
                        </div>
            
                <!-- Modal-->
                <div id="lgx-modal2" class="modal fade lgx-modal2 lgx-modal">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <iframe id="modalvideo2" src="https://www.youtube.com/embed/'.$video_id.'" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div> <!-- //.Modal-->
            </div>';


    return $output;

}


/**
 * Visual Composer
 */

if (class_exists('WPBakeryVisualComposerAbstract')) {
    vc_map(array(
        "name" => esc_html__("Video Embed", 'vcx-theme-core'),
        "base" => "vcx_video_embed",
        // 'icon' => 'icon_travel_info',
        "class" => "",
        "description" => esc_html__("Display Youtube Video", 'vcx-theme-core'),
        "category" => esc_html__('Emeet', 'vcx-theme-core'),
        "params" => array(

            array(
                "type" 			=> "textfield",
                "heading" 		=> esc_html__("Youtube Video ID", "vcx-theme-core"),
                "description" => esc_html__("Please add Youtube Video ID", 'vcx-theme-core'),
                "param_name" 	=> "video_id",
                "value" 		=> "oSPR5Go05Vg",
            ),
        )

    ));
}
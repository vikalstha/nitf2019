<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

add_shortcode( 'vcx_hurry_up', 'vcx_hurry_up_function');

/**
 *  Title Section Short Code
 * @param $atts
 * @return string
 */

function vcx_hurry_up_function($atts)
{
    extract(shortcode_atts(array(
        'title'         => 'Hurry Up!', // Section Title
        'sub_title'     => 'Get Ready For The Event', // Section Sub Title
        'date_text'     => '23-27 September, 2019',
    ), $atts));

    $title = !empty($title) ? '<h2 class="title">'.$title.'</h2>' : '';
    $sub_title= !empty($sub_title) ? '<h3 class="subtitle">'.$sub_title.'</h3>' : '';
    $date_text = !empty($date_text) ? '<p class="date">'.$date_text.'</p>' : '';


    $output = ' <div class="vcx-hurry-up countdown-left-info">
                    '.$title.'
                    '.$sub_title.'
                    '.$date_text.'
               </div>';

    return $output;
}


/**
 * Visual Composer
 */

if (class_exists('WPBakeryVisualComposerAbstract')) {
    vc_map(array(
        "name" => esc_html__("Hurry Up", 'vcx-theme-core'),
        "base" => "vcx_hurry_up",
        "class" => "",
        "description" => esc_html__("Display Section Title", 'vcx-theme-core'),
        "category" => esc_html__('Emeet', 'vcx-theme-core'),
        "params" => array(

            array(
                "type" 			=> "textfield",
                "heading" 		=> esc_html__("Title", "vcx-theme-core"),
                "param_name" 	=> "title",
                "value" 		=> "Hurry Up!",
                "admin_label"   => true,
            ),

            array(
                "type" 			=> "textfield",
                "heading" 		=> esc_html__("Post Title", "vcx-theme-core"),
                "param_name" 	=> "sub_title",
                "value" 		=> "Get Ready For The Event",
            ),
            array(
                "type" 			=> "textfield",
                "heading" 		=> esc_html__("Date", "vcx-theme-core"),
                "param_name" 	=> "date_text",
                "value" 		=> "23-27 September, 2019",
            )
        )

    ));
}
<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

/************************************************************************
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 *************************************************************************/

if ( ! function_exists( 'emeet_setup' ) ) :

    function emeet_setup() {

        //Text Domain
        load_theme_textdomain( 'emeet', get_template_directory() . '/languages' );

        add_theme_support( 'title-tag' );

        add_theme_support( 'post-thumbnails' );

        add_image_size( 'emeet-thumbnails', 200, 200, true );

        add_image_size( 'emeet-blog-medium', 480, 295, true );

        add_image_size( 'emeet-gallery-medium', 640, 470, true );

        add_image_size( 'emeet-blog', 1140, 565, true );


        // This theme uses wp_nav_menu() in one location.
        register_nav_menus( array(
            'mainmenu' => esc_html__( 'Main Menu', 'emeet' ),
        ) );

        add_theme_support( 'post-formats', array( 'audio','gallery','image','link','quote','video'));
        add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form'));
        add_theme_support( 'automatic-feed-links' );

        add_editor_style('');

        if ( ! isset( $content_width ) ) {
            $content_width = 660;
        }


        // Set up the WordPress core custom background feature.
        add_theme_support( 'custom-background', apply_filters( 'emeet_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        ) ) );


    }
endif;
add_action( 'after_setup_theme', 'emeet_setup' );


/**
 * Enqueue scripts and styles.
 */
function emeet_scripts() {
    global $emeet_options;
    $emeet_opt = new LgxFrameworkOpt();

    // LOAD FONTS
    if(!isset($emeet_options['body-font'])) {
        wp_enqueue_style('emeet-fonts', emeet_fonts_url(), array(), '1.0.1');
    }


    //LOAD Vendor STYLE SHEET
    wp_enqueue_style( 'bootstrap', EMEET_URI . 'assets/vendor/bootstrap/css/bootstrap.min.css' );
    wp_enqueue_style( 'font-awesome', EMEET_URI . 'assets/vendor/font-awesome/css/font-awesome.min.css' );
    wp_enqueue_style( 'animate', EMEET_URI . 'assets/css/animate.css' );
    wp_enqueue_style( 'jquery-smartmenus-bootstrap', EMEET_URI . 'assets/css/jquery.smartmenus.bootstrap.css' );

    //LOAD Main STYLE SHEET
    wp_enqueue_style( 'emeet-main-style', EMEET_URI . 'assets/css/main-style.min.css', array(), '1.0.1');
    wp_enqueue_style( 'emeet-style', get_stylesheet_uri() );

    //LOAD SCRIPT
    wp_enqueue_script( 'modernizr', EMEET_URI . 'assets/js/modernizr-2.8.3.min.js', array('jquery'), '1.0', false );
    wp_enqueue_script( 'bootstrap', EMEET_URI . 'assets/js/bootstrap.min.js', array('jquery', 'masonry'), '1.0', false );
    wp_enqueue_script( 'jquery-smartmenus', EMEET_URI . 'assets/js/jquery.smartmenus.min.js', array('jquery'), '1.0', true );
    wp_enqueue_script( 'jquery-smartmenus-bootstrap', EMEET_URI . 'assets/js/jquery.smartmenus.bootstrap.min.js', array('jquery'), '1.0', true );
    wp_enqueue_script( 'TimeCircles', EMEET_URI . 'assets/js/TimeCircles.js', array('jquery'), '1.0', true );
    wp_enqueue_script( 'jquery-easing', EMEET_URI . 'assets/js/jquery.easing.min.js', array('jquery'), '1.0', true );
    wp_enqueue_script( 'jquery-smooth-scroll', EMEET_URI . 'assets/js/jquery.smooth-scroll.min.js', array('jquery'), '1.0', true );
    wp_enqueue_script( 'emeet-slider', EMEET_URI . 'assets/js/slider.js', array('jquery'), '1.0', true );

    if ( is_rtl() ) {
        wp_enqueue_script( 'rtl', EMEET_URI . 'assets/js/rtl.js', array('jquery'), '1.0', true );
    }

    wp_enqueue_script( 'emeet-script', EMEET_SCRIPT . 'theme-main-script.js', array('bootstrap'), '1.0.0', true );


    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }

    // Custom Style  Start
    $emeet_custom_css = "";

    // Logo Settings
    if(!empty($emeet_options['logo-width'])){
        $emeet_logo_wd = $emeet_options['logo-width'];
        $emeet_custom_css .= "
            .lgx-header .lgx-navbar .lgx-logo{
                max-width:{$emeet_logo_wd};
            }
            
        ";
    }

    if(!empty($emeet_options['logo-width-onscroll'])){

        $emeet_logo_wd_onscroll = $emeet_options['logo-width-onscroll'];
        $emeet_custom_css .= "
            .lgx-header .menu-onscroll .lgx-navbar .lgx-logo{
                max-width:{$emeet_logo_wd_onscroll};
            }
            
        ";
    }

    // Page Banner background
    if(isset($emeet_options['opt_page_banner_bg_type'])) {
        $header_banner_style = 'background:' . $emeet_options['lgx_page_banner_color'] . ';';

        if ($emeet_options['opt_page_banner_bg_type']) {
            $header_banner_style = 'background: url(' . $emeet_opt->emeet_page_banner() . ')center bottom / cover no-repeat;';
        }

        $emeet_custom_css .= ' .lgx-banner-page {'. $header_banner_style . '}';
    }



    // Footer background
    if(isset($emeet_options['pre_loader_en']) && $emeet_options['pre_loader_en']) {

        $emeet_custom_css .= '#vcx-preloader {
                            background:url('.$emeet_opt->emeet_pre_loader_img().') no-repeat #FFFFFF 50%;
                            -moz-background-size: '.$emeet_options['pre_loader_width']. ' '.$emeet_options['pre_loader_width'].';
                            -o-background-size: '.$emeet_options['pre_loader_width']. ' '.$emeet_options['pre_loader_width'].';
                            -webkit-background-size: '.$emeet_options['pre_loader_width']. ' '.$emeet_options['pre_loader_width'].';
                            background-size: '.$emeet_options['pre_loader_width']. ' '.$emeet_options['pre_loader_width'].';
                           
        }';
    }


    // Pre Loader

    if(isset($emeet_options['opt_footer_bg_type'])) {
        $footer_banner_style = 'background:' . $emeet_options['lgx_footer_bg_color'] . ';';

        if ($emeet_options['opt_footer_bg_type']) {
            $footer_banner_style = 'background: url(' . $emeet_opt->emeet_footer_bg_img() . ') top center repeat;';
        }

        $emeet_adv_css = (isset($emeet_options['custom_css'])) ? $emeet_options['custom_css'] : '' ;
        $emeet_custom_css .= "{$emeet_adv_css}";

        $emeet_custom_css .= ' .lgx-footer-wrapper {'. $footer_banner_style . '}';
    }

    wp_add_inline_style( 'emeet-style', $emeet_custom_css );





    $emeet_adv_js = 'jQuery(window).load(function() {
            jQuery("#vcx-preloader").delay(250).fadeOut("slow");
            setTimeout(vcx_remove_preloader, 2000);
            function vcx_remove_preloader() {
                jQuery("#vcx-preloader").remove();
            }
        });';

    $emeet_custom_js = (isset($emeet_options['pre_loader_en']) && $emeet_options['pre_loader_en']==1) ? $emeet_adv_js : '' ;


    $emeet_custom_js .= "{$emeet_adv_js}";

    wp_add_inline_script( 'emeet-script', $emeet_custom_js );

}
add_action( 'wp_enqueue_scripts', 'emeet_scripts' );


/************************************************************************
 * // Remove the Open Sans From Frontend & Backend
 *************************************************************************/

if (!function_exists('remove_wp_open_sans')) :
    function remove_wp_open_sans() {
        wp_deregister_style( 'open-sans' );
        wp_register_style( 'open-sans', false );
    }
    add_action('wp_enqueue_scripts', 'remove_wp_open_sans');
    // Uncomment below to remove from admin
    // add_action('admin_enqueue_scripts', 'remove_wp_open_sans');
endif;

/************************************************************************
 * Default Fonts Enqueue
 *************************************************************************/


if (!function_exists('emeet_fonts_url'))  {
    function emeet_fonts_url() {

        $emeet_font = '';

        $font_families = array();
        $font_families[] = 'Playfair+Display:400,700,900';
        $font_families[] = 'Lato:300,400,700,900';

        $query_args = array(
            'family' => urlencode( implode( '|', $font_families ) ),
            //'subset' => urlencode( 'latin,latin-ext' )
        );

        //$emeet_font = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
        $emmet_font = 'https://fonts.googleapis.com/css?family=Quicksand:300,400,500,600';

        return esc_url_raw( $emmet_font );
        //return '';
    }
}


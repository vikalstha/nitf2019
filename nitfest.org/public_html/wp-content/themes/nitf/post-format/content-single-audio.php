<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Emeet
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('lgx-single-post'); ?>>
    <header>

        <div class="text-area">

            <?php $lgx_post_audio = get_post_meta(get_the_ID(),'__vcx__post-format-audio',true); ?>

            <?php if(!empty($lgx_post_audio)): ?>
                <div class="lgx-featured-wrap lgx-post-audio">
                    <?php echo balanceTags(wp_kses_post($lgx_post_audio)); ?>
                </div>
            <?php endif; ?>


            <h1 class="title"><?php the_title(); ?></h1>
            <div class="hits-area">
                <div class="date">
                    <a href="javascript:void(0)"><i class="fa fa-user"></i> <?php the_author(); ?></a>
                    <?php
                    $catList = get_the_category_list(', ');
                    echo (!empty($catList) ? '<a href="javascript:void(0)"><i class="fa fa-folder" aria-hidden="true"></i>'.$catList.'</a>' : '' );
                    ?>
                    <a href="javascript:void(0)"><i class="fa fa-calendar"></i> <?php echo date_i18n(get_option('date_format'), false, false);; ?></a>
                    <a href="javascript:void(0)"><i class="fa fa-comment"></i>
                        <?php comments_popup_link(
                            esc_html__( 'No Comment', 'emeet' ),
                            esc_html__( '1 Comment', 'emeet' ),
                            esc_html__( '% Comments', 'emeet' ),
                            '',
                            esc_html__( 'Comments Closed', 'emeet' )
                        ); ?>
                    </a>
                </div>
            </div>
        </div>

    </header>

    <section>
        <?php the_content();
        wp_link_pages( array(
            'before'      => '<div class="page-links clearfix"><span class="page-links-title">' . esc_html__( 'Pages:', 'emeet' ) . '</span>',
            'after'       => '</div>',
            'link_before' => '<span class="page-number">',
            'link_after'  => '</span>',
        ) );

        global $numpages;
        if ( is_singular() && $numpages > 1 ) {
            if ( is_singular( 'attachment' ) ) {
                // Parent post navigation.
                the_post_navigation( array(
                    'prev_text' => _x( '<span class="meta-nav">Published in</span><span class="post-title">%title</span>', 'Parent post link', 'emeet' ),
                ) );
            } elseif ( is_singular( 'post' ) ) {
                // Previous/next post navigation.
                the_post_navigation( array(
                    'next_text' => '<span class="meta-nav" aria-hidden="true">' . esc_html__( 'Next', 'emeet' ) . '</span> ' .
                        '<span class="screen-reader-text">' . esc_html__( 'Next post:', 'emeet' ) . '</span> ' .
                        '<span class="post-title">%title</span>',
                    'prev_text' => '<span class="meta-nav" aria-hidden="true">' . esc_html__( 'Previous', 'emeet' ) . '</span> ' .
                        '<span class="screen-reader-text">' . esc_html__( 'Previous post:', 'emeet' ) . '</span> ' .
                        '<span class="post-title">%title</span>',
                ) );
            }
        }
        ?>
    </section>

    <footer>

        <?php do_action("emeet_single_post_footer");  ?>

        <?php
        // If comments are open or we have at least one comment, load up the comment template.
        if ( comments_open() || get_comments_number() ) :
            comments_template();
        endif;
        ?>
    </footer>

</article>


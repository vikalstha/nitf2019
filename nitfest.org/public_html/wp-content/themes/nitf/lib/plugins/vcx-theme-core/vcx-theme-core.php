<?php
/*
*  Theme Core Plugin By LogicHunt
*
* Plugin Name: Emeet Theme Core
* Plugin URI: http://logichunt.com/vcx-theme-core
* Author: LogicHunt
* Author URI: http://logichunt.com
* License - GNU/GPL V2 or Later
* Description: Emeet Theme Core is is Core required plugin for Emeet WordPress Event Theme.
* Version: 1.4.0
*************************************************************************/

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}


/************************************************************************
 * vcx Theme core Plugin Language
 *************************************************************************/


function vcx_theme_core_language_load(){
    $plugin_dir = basename(dirname(__FILE__))."/languages/";
    load_plugin_textdomain( 'vcx-theme-core', false, $plugin_dir );
}
add_action( 'init', 'vcx_theme_core_language_load' );


/************************************************************************
 * Important Helpers Function
 *************************************************************************/
include_once( 'helpers/helpers.php' );


/*****************************************
 *  Post Type Declaration
 *****************************************/

include_once( 'post-type/post-speaker.php');
include_once( 'post-type/post-schedule.php');
include_once( 'post-type/post-pricing.php');
include_once( 'post-type/post-testimonial.php');
include_once( 'post-type/post-gallery.php');


/************************************************************************
 * Metabox Include & Configuration
 *************************************************************************/

include_once( 'meta-data/metabox-config.php' );


/************************************************************************
 * Register Global Shortcode
 *************************************************************************/

//Add Here .....................

/************************************************************************
 *   widget
 *************************************************************************/

include_once( 'widget/widget-instagram.php' );
include_once( 'widget/widget-location.php' );
include_once( 'widget/widget-social.php' );



/************************************************************************
 * Register Theme Shortcode
 *************************************************************************/
include_once( 'vc-addons/header-banner.php' );
include_once( 'vc-addons/header-classic.php' );
include_once( 'vc-addons/header_counter.php' );
include_once( 'vc-addons/header_registration.php' );
include_once( 'vc-addons/title.php');
include_once( 'vc-addons/brand-btn.php' );
include_once( 'vc-addons/about.php');
include_once( 'vc-addons/speaker.php' );
include_once( 'vc-addons/schedule.php' );
include_once( 'vc-addons/sponsors.php');
include_once( 'vc-addons/get-ticket.php' );
include_once( 'vc-addons/testimonial.php' );
include_once( 'vc-addons/latest-news.php' );
include_once( 'vc-addons/travel-info.php');
include_once( 'vc-addons/video-embed.php' );
include_once( 'vc-addons/photo-gallery.php' );
include_once( 'vc-addons/countdown.php' );
include_once( 'vc-addons/address-box.php' );
include_once( 'vc-addons/hurry_up.php' );
include_once( 'vc-addons/sp_image.php' );
include_once( 'vc-addons/logoslider.php' );
include_once( 'vc-addons/newsletter.php' );
include_once( 'vc-addons/milestone.php' );
include_once( 'vc-addons/instagram.php' );



/************************************************************************
 * Important Helpers Function
 *************************************************************************/

include_once( 'one-click-demo/init-one.php' );


/************************************************************************
 * Enqueue Assets
 *************************************************************************/

include_once( 'assets/assets.php');



/**
 *  Initialising Visual shortcode editor
 */


if (class_exists('WPBakeryVisualComposerAbstract')) {

	function vcx_theme_core_requireVcExtend(){
		include_once( 'extend_vc/extend_vc.php');
	}
	add_action('init', 'vcx_theme_core_requireVcExtend',2);
}


/************************************************************************
 * Custom Color
 *************************************************************************/

include_once( 'assets/custom-color.php' );
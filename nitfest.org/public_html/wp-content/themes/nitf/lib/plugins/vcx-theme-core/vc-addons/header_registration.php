<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}


add_shortcode( 'vcx_header_registration', 'vcx_header_registration_function');

/**
 *  Title Section Short Code
 * @param $atts
 * @return string
 */


function vcx_header_registration_function($atts)
{
    extract(shortcode_atts(array(

        'vcx_form_id'   => '',
        'vcx_form_type' => 'banner',
        'vcx_title'     => 'Registration',
        'bg_type'       => 'none',
        'bg_color'      => '#f2f2f2',
        'bg_image'      => '',

    ), $atts));

    $bg_style = '';

    if($bg_type == 'color'){

        $bg_style = 'style = "background: '.esc_attr($bg_color).' ;"';
    }

    $image_url = '';
    if(!empty ($atts['bg_image'])) {
        $img = wp_get_attachment_image_src($atts['bg_image'], true);
        $image_url = $img[0];

        $bg_style = 'style = "background: url('.esc_url($image_url).') ;"';
    }




    $output = '<div class="vcx-registration-area vcx-registration-type-'.$vcx_form_type.'" '.$bg_style.'>
                '.((!empty($vcx_title)) ? '<h3 class="lgx-form-title">'.$vcx_title.'</h3>' : '' ).'
                <div class="vcx-registration-form">
                 ' .( (!empty($vcx_form_id)) ? do_shortcode('[contact-form-7 id="'.$vcx_form_id.'" ]' ) : '' ).'
                </div>
            </div>';

    return $output;
}



/**
 * Visual Composer
 */

if (class_exists('WPBakeryVisualComposerAbstract')) {
    vc_map(array(
        "name" => esc_html__("Registration Form", 'vcx-theme-core'),
        "base" => "vcx_header_registration",
        "class" => "",
        "description" => esc_html__("Display Header Registration Form.", 'vcx-theme-core'),
        "category" => esc_html__('Emeet', 'vcx-theme-core'),
        "params" => array(
            array(
                "type"          => "dropdown",
                "heading"       => esc_html__("Select form", 'csx-food-king'),
                "param_name"    => 'vcx_form_id',
                "dsc"         => esc_html__('Choose previously created contact form from the drop down list.', 'vcx-theme-core'),
                "value"         => vcx_theme_core_post_list('wpcf7_contact_form')

            ),

            array(
                "type" 			=> "textfield",
                "admin_label"   => true,
                "heading" 		=> esc_html__("Title", "vcx-theme-core"),
                "param_name" 	=> "vcx_title",
                "value" 		=> 'Registration',
                "description"   => esc_html__("Add Registration Area Title Here.", "vcx-theme-core"),

            ),

            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("Form Type", 'vcx-theme-core'),
                "param_name" 	=> "vcx_form_type",
                "value" 		=> array('Banner'=>'banner','About'=>'about' ),
            ),


            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("Background Type", 'vcx-theme-core'),
                "param_name" 	=> "bg_type",
                "value" 		=> array('None'=>'none', 'Color'=>'color','Image'=>'image' ),
            ),

            array(
                "type" 			=> "attach_image",
                "heading" 		=> esc_html__("Upload Background Image", "lgx-themential"),
                "param_name" 	=> "bg_image",
                "value" 		=> "",
                'dependency' => array(
                    'element' => 'bg_type',
                    'value' =>array('image'),
                ),
            ),

            array(
                "type"          => "colorpicker",
                "heading"       => esc_html__("Select Background Color", "vcx-theme-core"),
                "param_name"    => "bg_color",
                "value"         => "#f2f2f2",
                'dependency' => array(
                    'element' => 'bg_type',
                    'value' =>array('color'),
                ),
            ),

        )
    ));
}


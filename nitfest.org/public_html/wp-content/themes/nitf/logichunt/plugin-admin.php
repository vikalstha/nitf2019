<?php
/**
 * Created by PhpStorm.
 * User: Vaskar Jewel
 * Date: 08-Jan-18
 * Time: 4:25 PM
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

// Don't duplicate me!
if ( ! class_exists( 'LogicHuntPluginExtendedAdmin' ) ) {


    class LogicHuntPluginExtendedAdmin
    {


        /**
         *  Output function of the milestone counter
         * @param null
         * @return array
         */

        public function lgx_owl_carousel_admin_post_args()


        {



            $labels = array(
                'name'               => _x('Carousel Slider', 'Carousel Slider', 'emeet'),
                'singular_name'      => _x('Slider Item', 'Slider Items', 'emeet'),
                'menu_name'          => __('Carousel Slider', 'emeet'),
                'all_items'          => __('All Carousel', 'emeet'),
                'view_item'          => __('View Item', 'emeet'),
                'add_new_item'       => __('Add New Carousel Item', 'emeet'),
                'add_new'            => __('Add New', 'emeet'),
                'edit_item'          => __('Edit Carousel Item', 'emeet'),
                'update_item'        => __('Update Carousel Item', 'emeet'),
                'search_items'       => __('Search Carousel', 'emeet'),
                'not_found'          => __('No Carousel items found', 'emeet'),
                'not_found_in_trash' => __('No Carousel items found in trash', 'emeet')
            );


            //custom post type setup
            $args_lgxcarousel = array(
                'label'               => __('Carousel Slider', 'emeet'),
                'description'         => __('OWL Carousel Slider Post Type', 'emeet'),
                'labels'              => $labels,
                'supports'            => array('title', 'thumbnail'),
                'taxonomies'          => array(''),
                'hierarchical'        => false,
                'public'              => true,
                'show_ui'             => true,
                'show_in_menu'        => true,
                'menu_icon'           => plugins_url('/lgx-owl-carousel/admin/assets/img/owl-logo.png'),
                'show_in_nav_menus'   => true,
                'show_in_admin_bar'   => true,
                'menu_position'       => 25,
                'can_export'          => true,
                'has_archive'         => true,
                'exclude_from_search' => false,
                'publicly_queryable'  => true,
                'capability_type'     => 'page',
            );



            // Return Output
            return $args_lgxcarousel;

        } //End MileStone

    }
}
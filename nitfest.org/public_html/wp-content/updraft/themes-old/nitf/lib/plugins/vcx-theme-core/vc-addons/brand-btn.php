<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

add_shortcode( 'vcx_brand_btn', 'vcx_brand_btn_function');


/**
 * Short Code
 * @param $atts
 * @return string
 */

function vcx_brand_btn_function($atts) {
    extract(shortcode_atts(array(
        'btn_alignment' =>'center', // Any Text
        'text' 	    =>	'Brand Button', // Any Text
        'size'      =>	'btn-df', // small or large
        'style'     =>	'lgx-btn-brand', // small or large
        'url' 	    =>	'#', //url
        'target'    =>	'_blank', // )_blank, _self
        'text2' 	=>	'Brand Button 2', // Any Text
        'size2'     =>	'btn-df', // small or large
        'style2'    =>	'lgx-btn-brand2', // small or large
        'url2' 	    =>	'#', //url
        'target2'   =>	'_blank', // )_blank, _self

    ), $atts));



    $btn_one_html = !empty(trim($url)) ? '<a class="lgx-btn '.esc_attr($size).'  '.esc_attr($style).'" href="'.$url.'" target="'.$target.'"><span>'.$text.'</span></a>' : '';
    $btn_two_html = !empty(trim($url2)) ? '<a class="lgx-btn '.esc_attr($size2).'  '.esc_attr($style2).'" href="'.$url2.'" target="'.$target2.'"><span>'.$text2.'</span></a>' : '';


    $output = '<div class="lgx-brand-btn-area text-'.$btn_alignment.'">
                    '.$btn_one_html.'
                    '.$btn_two_html.'
               
                </div>';
    return $output;

}


/**
 * Visual Composer
 */

if (class_exists('WPBakeryVisualComposerAbstract')) {
    vc_map(array(
        "name" => esc_html__("Brand Button", 'vcx-theme-core'),
        "base" => "vcx_brand_btn",
        "class" => "",
        "description" => esc_html__("Add Brand Button", 'vcx-theme-core'),
        "category" => esc_html__('Emeet', 'vcx-theme-core'),
        "params" => array(
            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("Button Alignment", 'vcx-theme-core'),
                "param_name" 	=> "btn_alignment",
                "value" 		=> array('Center'=>'center', 'Left'=>'left','Right' => 'right'),
            ),

            array(
                "type" 			=> "textfield",
                "heading" 		=> esc_html__("First Button Text", "vcx-theme-core"),
                "param_name" 	=> "text",
                "value" 		=> "Brand Button",
                "admin_label"   => true,
            ),

            array(
                "type" 			=> "textfield",
                "heading" 		=> esc_html__("First Button URL", "vcx-theme-core"),
                "param_name" 	=> "url",
                "value" 		=> "#",
            ),

            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("First Button Size", 'vcx-theme-core'),
                "param_name" 	=> "size",
                "value" 		=> array('Default'=>'lgx-btn-df','Large'=>'lgx-btn-big'),
            ),

            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("First Button Style", 'vcx-theme-core'),
                "param_name" 	=> "style",
                "value" 		=> array('Brand'=>'lgx-btn-brand','Brand Two'=>'lgx-btn-brand2', 'Brand Three'=>'lgx-btn-brand3', 'Vivid'=>'lgx-btn-highlight'),
            ),

            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("First Button Target Type", 'vcx-theme-core'),
                "param_name" 	=> "target",
                "value" 		=> array('New Window'=>'_blank','Current Window'=>'_self'),
            ),
            array(
                "type" 			=> "textfield",
                "heading" 		=> esc_html__("Second Button Text", "vcx-theme-core"),
                "param_name" 	=> "text2",
                "value" 		=> "Brand Button 2",
                "admin_label"   => true,
            ),

            array(
                "type" 			=> "textfield",
                "heading" 		=> esc_html__("Second Button URL", "vcx-theme-core"),
                "param_name" 	=> "url2",
                "value" 		=> "#",
            ),

            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("Second Button Size", 'vcx-theme-core'),
                "param_name" 	=> "size2",
                "value" 		=> array('Default'=>'btn-df','Large'=>'lgx-btn-lg'),
            ),

            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("Second Button Style", 'vcx-theme-core'),
                "param_name" 	=> "style2",
                "value" 		=> array('Brand Two'=>'lgx-btn-brand2', 'Brand'=>'lgx-btn-brand','Brand Three'=>'lgx-btn-brand3', 'Vivid'=>'lgx-btn-highlight'),
            ),

            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("Second Button Target Type", 'vcx-theme-core'),
                "param_name" 	=> "target2",
                "value" 		=> array('New Window'=>'_blank','Current Window'=>'_self'),
            )
        )

    ));
}
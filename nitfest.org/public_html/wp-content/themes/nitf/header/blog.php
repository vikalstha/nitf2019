<section>
    <div class="lgx-banner lgx-banner-inner lgx-banner-page">
        <div class="lgx-page-inner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="lgx-heading-area">
                            <div class="lgx-heading lgx-heading-white">
                                <h2 class="heading"><?php emeet_breadcrumb(); ?></h2>
                            </div>
                            <ul class="breadcrumb">
                                <li><a href="<?php echo esc_url(home_url('/')); ?>"><i class="icon-home6"></i><?php esc_html_e('Home','emeet'); ?></a></li>
                                <li class="active"><?php emeet_breadcrumb(); ?></li>
                            </ul>
                        </div>
                    </div>
                </div><!--//.ROW-->
            </div><!-- //.CONTAINER -->
        </div><!-- //.INNER -->
    </div>
</section> <!--//.Banner Inner-->
<?php
/**
 * Template Name: Blog Left Sidebar
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Emeet
 */


get_header(); ?>

<?php get_template_part('header/blog'); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            <div id="lgx-page-wrapper" class="lgx-page-wrapper lgx-page-left-sidebar">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <!-- Start: Sidebar -->
                            <?php get_sidebar(); ?>
                            <!-- End: Sidebar -->
                        </div>
                        <div class="col-sm-8">
                            <div class="blog-area">

                                <div class="lgx-card-wrapper">
                                    <div id="lgx-masonry-wrapper" class="lgx-masonry-area">

                                        <?php
                                        $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
                                        $args  = array(
                                            'posts_per_page' => get_option( 'posts_per_page' ),
                                            'post_type'      => 'post',
                                            'post_status'    => 'publish',
                                            'paged'          => $paged
                                        );
                                        $query = new WP_Query( $args );
                                        ?>

                                        <?php if ( $query->have_posts() ) : ?>

                                            <?php while ( $query->have_posts() ) : $query->the_post(); ?>

                                                <?php get_template_part( 'template-parts/post-loop/content', get_post_format() ); ?>

                                            <?php endwhile; ?>

                                        <?php
                                        else :
                                            get_template_part( 'template-parts/post-loop/content', 'none' );
                                        endif; ?>

                                    </div> <!-- masonry wrapper -->

                                    <div class="row">
                                        <div class="col-xs-12 text-center clearfix lgx-pagination">
                                            <?php themearth_posts_pagination(); ?>
                                        </div>
                                    </div>

                                    <?php wp_reset_postdata(); ?>

                                </div> <!--// lgx-card-wrapper-->

                            </div><!-- //. Area  -->
                        </div>

                    </div>
                </div>
            </div>
        </main>
    </div>

<?php get_footer();
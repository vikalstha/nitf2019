<?php


if(isset($_GET['token'])){
    $token = $_GET['token'];
    $key='manaatakgarchu';
    if($token != $key){
       die('Invalid Token. Please Ask admin for token');
    }
    else{  


$id= $_GET['response_id'];
if($id==null){
	die('no such data. ');
}

require 'vendor/autoload.php';

$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
$reader->setReadDataOnly(true);
$spreadsheet = $reader->load("Application Form (Responses).xlsx");

$sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

// echo "<pre>";
// print_r($sheetData[$id]['AC']);
// $a=explode(', ',$sheetData[$id]['AC']);
// echo "<br>";
// print_r($a);
// die();
function linkifyYouTubeURLs($text) {
    $text = preg_replace('~(?#!js YouTubeId Rev:20160125_1800)
        # Match non-linked youtube URL in the wild. (Rev:20130823)
        https?://          # Required scheme. Either http or https.
        (?:[0-9A-Z-]+\.)?  # Optional subdomain.
        (?:                # Group host alternatives.
          youtu\.be/       # Either youtu.be,
        | youtube          # or youtube.com or
          (?:-nocookie)?   # youtube-nocookie.com
          \.com            # followed by
          \S*?             # Allow anything up to VIDEO_ID,
          [^\w\s-]         # but char before ID is non-ID char.
        )                  # End host alternatives.
        ([\w-]{11})        # $1: VIDEO_ID is exactly 11 chars.
        (?=[^\w-]|$)       # Assert next char is non-ID or EOS.
        (?!                # Assert URL is not pre-linked.
          [?=&+%\w.-]*     # Allow URL (query) remainder.
          (?:              # Group pre-linked alternatives.
            [\'"][^<>]*>   # Either inside a start tag,
          | </a>           # or inside <a> element text contents.
          )                # End recognized pre-linked alts.
        )                  # End negative lookahead assertion.
        [?=&+%\w.-]*       # Consume any URL (query) remainder.
        ~ix', '<iframe width="560" height="315" src="https://www.youtube.com/embed/$1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>',
        $text);
    return $text;
}
									
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Registration of <?=$sheetData[$id]['C']?> - <?=$sheetData[$id]['B']?></title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="css/style.min.css" rel="stylesheet">
    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>

</head>

<body class="grey lighten-3">

    <!--Main Navigation-->
    <header>

        <!-- Navbar -->
        <nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
            <div class="container">

                <!-- Brand -->
                <a class="navbar-brand waves-effect" href="" target="_blank">
                    <!-- <img src="https://nitfest.org/2019/assets/img/logo.png" class="img-fluid" alt=""> -->
                    <strong class="blue-text">NITFEST 2019 Play Registrations Entry of <span class="text-danger"> <?=$sheetData[$id]['C']?> </span> by <span class="text-success"> <?=$sheetData[$id]['P']?> </span></strong>
                </a>

                             

            </div>
        </nav>
        <!-- Navbar -->

    </header>
    <!--Main Navigation-->

    <!--Main layout-->
    <main class="mt-5 pt-5">
        <div class="container">

            <!--Section: Post-->
            <section class="mt-4">

                <!--Grid row-->
                <div class="row">

                    <!--Grid column-->
                    <div class="col-md-8 mb-4">

               

                        <!--Card-->
                        <div class="card mb-4 wow fadeIn">

                            <!--Card content-->
                            <div class="card-body text-center">

                                <p class="h3 my-4"><?=$sheetData[$id]['C']?></p>
                                <hr class="my-4">
								
                                <p class="my-4">Synopsis of the Play</p>
										

                                <blockquote class="blockquote">
                                    <p class="mb-0"><?=$sheetData[$id]['U']?></p>
                                    <!-- <footer class="blockquote-footer">Someone famous in
                                        <cite title="Source Title">Source Title</cite>
                                    </footer> -->
                                </blockquote>
                                <hr>

                            </div>

                        </div>
                        <!--/.Card-->

                        <!--Card-->
                        <div class="card mb-4 wow fadeIn">

                            <!--Card content-->
                            <div class="card-body">

                                

                                <p class="h5 my-4"> Summary of the Play </p>

                                <p><?=$sheetData[$id]['V']?></p>

                                <hr>

                                <p class="h5 my-4"> Director's Note </p>

                                <p><?=$sheetData[$id]['W']?></p>
                                <hr>

                                <p class="h5 my-4"> Director's Resume </p>

                                <p><?=$sheetData[$id]['X']?></p>
                                <hr>

                                <p class="h5 my-4"> Playwright's Resume </p>

                                <p><?=$sheetData[$id]['Y']?></p>
                                <hr>

                                <p class="h5 my-4"> Translator's Resume </p>

                                <p><?=$sheetData[$id]['Z']?></p>
                                <hr>

                                <p class="h5 my-4"> What is/are the socio-political relevance/s of your production in local or global scenario? : </p>

                                <p><?=$sheetData[$id]['AA']?></p>
                                <hr>

                                <p class="h5 my-4"> Press reviews of the production (if any) :  </p>

                                <p><?=$sheetData[$id]['AB']?>

                                	<hr>

                                	<?php 

									preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $sheetData[$id]['AB'], $match);

									// echo "<pre>";
									// print_r($match[0]); 
									// echo "</pre>";

									foreach ($match[0] as $kv => $video) {
									

											if (strpos($video, 'yout') !== false) {
												echo linkifyYouTubeURLs($video);
											}
											else{

									?>
									<br>
									<hr style="width: 60px;">
									<h5>Other links Included here:</h5>
									<a target="_blank" href="<?=$video?>" class="btn btn-sm btn-primary btn-rounded">Open Other link in new window</a> <br>

										
									<?php }
									}

									?>
                                	
                                </p>

                                <hr>
								
								<p class="h5 my-4"> Name List of Cast and Crew :   </p>

                                <p><?=$sheetData[$id]['AE']?></p>
                                <hr>
                                


                            </div>

                        </div>
                        <!--/.Card-->

                        <!--Card-->
                        <div class="card mb-4 wow fadeIn">

                            <div class="card-header font-weight-bold">
                                <span>Related Links / Images / Videos</span>
                                
                            </div>

                            <!--Card content-->
                            <div class="card-body">

                                <div class="media d-block d-md-flex mt-3">
                                   
                                    <div class="media-body text-center text-md-left ml-md-3 ml-0">
                                      
                                        <p class="h5 my-4"> 10 copies high resolution photographs of the play :   </p>

                                <p>
									<?php 
                                	$highimg = explode(', ',$sheetData[$id]['AC']);
									
									foreach ($highimg as $kimg => $img) { 
										$i = str_replace('https://drive.google.com/open?id', 'https://drive.google.com/uc?export=view&id', $img); ?>

										<a target="_blank" href="<?=$i?>" class="btn btn-sm btn-primary btn-rounded">See Image</a> <br>
									<?php } ?>							
                                		
                                </p>
                                <hr>

                                <p class="h5 my-4"> High resolution portrait photographs of playwright and director :   </p>

                                <p>

                                	<?php 
                                	$highimg = explode(', ',$sheetData[$id]['AD']);
									
									foreach ($highimg as $kimg => $img) { 
										if(!empty($img)){
										$i = str_replace('https://drive.google.com/open?id', 'https://drive.google.com/uc?export=view&id', $img); ?>

										<a target="_blank" href="<?=$i?>" class="btn btn-sm btn-primary btn-rounded">See Image</a> <br>
									<?php }} ?>	
								</p>
                                <hr>

                                

                                <p class="h5 my-4"> A clear recording of the production video (rehearsal video needed if production is under process) :   </p>

                                <p>
                                	
                                	<?php if(!empty($sheetData[$id]['AF'])) {?>
                                	<a target="_blank" href="<?=$sheetData[$id]['AF']?>" class="btn btn-sm btn-primary btn-rounded">Open Link</a> <br>
                                	<?php } ?>
                                </p>
                                <hr>

                                <p class="h5 my-4"> Or Provide the link (a private/unlisted video, a google drive link, any link that is currently accessible) :   </p>

                                <p>
                                	<?=$sheetData[$id]['AG']?>

                                	<hr>

                                	<?php 

									


									preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $sheetData[$id]['AG'], $match);

									// echo "<pre>";
									// print_r($match[0]); 
									// echo "</pre>";

									foreach ($match[0] as $kv => $video) {
									

											if (strpos($video, 'yout') !== false) {
												echo linkifyYouTubeURLs($video);
											}
											else{

									?>
									<br>
									<hr style="width: 60px;">
									<h5>Other Video/File link:</h5>
									<a target="_blank" href="<?=$video?>" class="btn btn-sm btn-primary btn-rounded">Open Other link in new window</a> <br>

										
									<?php }
									}

									?>
									                                		
                                	</p>
                                <hr>

                                <p class="h5 my-4"> Detail of the production if it already has been performed in any other theatre festivals.:   </p>

                                <p>
                                	<p>
                                	<?=$sheetData[$id]['AH']?>

                                	<hr>

                                	<?php 



									preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $sheetData[$id]['AH'], $match);

									// echo "<pre>";
									// print_r($match[0]); 
									// echo "</pre>";

									foreach ($match[0] as $kv => $video) {
									

											if (strpos($video, 'yout') !== false) {
												echo linkifyYouTubeURLs($video);
											}
											else{

									?>
									<br>
									<hr style="width: 60px;">
									<h5>Other Video/File link:</h5>
									<a target="_blank" href="<?=$video?>" class="btn btn-sm btn-primary btn-rounded">Open Other link in new window</a> <br>

										
									<?php }
									}

									?></p>
                                </p>
                                <hr>

                                <p class="h5 my-4"> Upload Zip File:   </p>

                                <p>
                                	<?=$sheetData[$id]['AI']?>
                                	<?php if(!empty($sheetData[$id]['AI'])) {?>
                                	<a target="_blank" href="<?=$sheetData[$id]['AI']?>" class="btn btn-sm btn-primary btn-rounded">Open Link</a> <br>
                                	<?php } ?>
                                </p>

                                <hr>

                                <p class="h5 my-4"> Or Provide the link (a google drive link, any link that is currently accessible for download) : 
   </p>

                                <p>
                                	<?=$sheetData[$id]['AJ']?>

                                	<hr>

                                	<?php 



									preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $sheetData[$id]['AJ'], $match);

									// echo "<pre>";
									// print_r($match[0]); 
									// echo "</pre>";

									foreach ($match[0] as $kv => $video) {
									

											if (strpos($video, 'yout') !== false) {
												echo linkifyYouTubeURLs($video);
											}
											else{

									?>
									<br>
									<hr style="width: 60px;">
									<h5>Other Video/File link:</h5>
									<a target="_blank" href="<?=$video?>" class="btn btn-sm btn-primary btn-rounded">Open Other link in new window</a> <br>

										
									<?php }
									}

									?></p>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <!--/.Card-->

                    </div>
                    <!--Grid column-->

                    <!--Grid column-->
                    <div class="col-md-4 mb-4">

                       

                        <!--Card-->
                        <div class="card mb-4 wow fadeIn">

                            <div class="card-header">Production Details</div>

                            <!--Card content-->
                            <div class="card-body">
                            	
	                            	<div class="list-group-flush text-left">
									  <div class="list-group-item pl-0">
									    <p class="mb-0"><i class="far fa-image mr-2 grey p-1 white-text rounded-circle " aria-hidden="true"></i>Production Playwright : <?=$sheetData[$id]['D']?> </p>
									  </div>
									  <div class="list-group-item pl-0">
									    <p class="mb-0"> <i class="fas fa-briefcase mr-2 grey p-1 white-text rounded-circle" aria-hidden="true"></i>Translator/Adaptor (if any) : <?=$sheetData[$id]['E']?></p>
									  </div>
									  <div class="list-group-item pl-0">
									    <p class="mb-0"><i class="fas fa-anchor mr-2 grey p-1 white-text rounded-circle" aria-hidden="true"></i>Director  : <?=$sheetData[$id]['F']?></p>
									  </div>
									  <div class="list-group-item pl-0">
									    <p class="mb-0"><i class="fas fa-anchor mr-2 grey p-1 white-text rounded-circle" aria-hidden="true"></i>Duration (with/without intermission)  : <?=$sheetData[$id]['G']?></p>
									  </div>
									  <div class="list-group-item pl-0">
									    <p class="mb-0"><i class="fas fa-anchor mr-2 grey p-1 white-text rounded-circle" aria-hidden="true"></i>Language : <?=$sheetData[$id]['H']?></p>
									  </div>
									  <div class="list-group-item pl-0">
									    <p class="mb-0"><i class="fas fa-anchor mr-2 grey p-1 white-text rounded-circle" aria-hidden="true"></i>Last performance and venue (if not a new production) : <?=$sheetData[$id]['I']?></p>
									  </div>
									</div>
	                            
                            </div>

                        </div>
                        <!--/.Card-->

                        <!--Card-->
                        <div class="card mb-4 wow fadeIn">

                            <div class="card-header">Team Members[OnStage]</div>

                            <!--Card content-->
                            <div class="card-body">
	                            	<div class="list-group-flush text-left">
									  <div class="list-group-item pl-0">
									    <p class="mb-0"><i class="far fa-image mr-2 grey p-1 white-text rounded-circle " aria-hidden="true"></i>Male : <?=$sheetData[$id]['J']?> </p>
									  </div>
									  <div class="list-group-item pl-0">
									    <p class="mb-0"> <i class="fas fa-briefcase mr-2 grey p-1 white-text rounded-circle" aria-hidden="true"></i>Female : <?=$sheetData[$id]['K']?></p>
									  </div>
									  <div class="list-group-item pl-0">
									    <p class="mb-0"> <i class="fas fa-briefcase mr-2 grey p-1 white-text rounded-circle" aria-hidden="true"></i>Other : <?=$sheetData[$id]['L']?></p>
									  </div>
									  
									</div>
	                            
                            </div>
                            
                            <div class="card-header">Team Members[OffStage]</div>

                            <!--Card content-->
                            <div class="card-body">
                            	
	                            	<div class="list-group-flush text-left">
									  <div class="list-group-item pl-0">
									    <p class="mb-0"><i class="far fa-image mr-2 grey p-1 white-text rounded-circle " aria-hidden="true"></i>Male : <?=$sheetData[$id]['M']?> </p>
									  </div>
									  <div class="list-group-item pl-0">
									    <p class="mb-0"> <i class="fas fa-briefcase mr-2 grey p-1 white-text rounded-circle" aria-hidden="true"></i>Female : <?=$sheetData[$id]['N']?></p>
									  </div>
									  <div class="list-group-item pl-0">
									    <p class="mb-0"> <i class="fas fa-briefcase mr-2 grey p-1 white-text rounded-circle" aria-hidden="true"></i>Other : <?=$sheetData[$id]['O']?></p>
									  </div>
									  
									</div>
	                            
                            </div>

                        </div>
                        <!--/.Card-->
                         <!--Card : Dynamic content wrapper-->
                        <div class="card mb-4 text-center wow fadeIn">

                            <div class="card-header">Contact Info.</div>

                            <!--Card content-->
                            <div class="card-body">
                            	<div class="list-group-flush text-left">
								  <div class="list-group-item pl-0">
								    <p class="mb-0"><i class="far fa-image mr-2 grey p-1 white-text rounded-circle " aria-hidden="true"></i>Address : <?=$sheetData[$id]['Q']?> </p>
								  </div>
								  <div class="list-group-item pl-0">
								    <p class="mb-0"> <i class="fas fa-briefcase mr-2 mr-2 grey p-1 white-text rounded-circle" aria-hidden="true"></i>Telephone : <?=$sheetData[$id]['R']?></p>
								  </div>
								  <div class="list-group-item pl-0">
								    <p class="mb-0"><i class="fas fa-anchor mr-2 grey p-1 white-text rounded-circle" aria-hidden="true"></i>Mobile : <?=$sheetData[$id]['S']?></p>
								  </div>
								  <div class="list-group-item pl-0">
								    <p class="mb-0"><i class="fas fa-anchor mr-2 grey p-1 white-text rounded-circle" aria-hidden="true"></i>Website : <?=$sheetData[$id]['T']?></p>
								  </div>
								  <div class="list-group-item pl-0">
								    <p class="mb-0"><i class="fas fa-anchor mr-2 grey p-1 white-text rounded-circle" aria-hidden="true"></i>Email : <?=$sheetData[$id]['B']?></p>
								  </div>

								</div>
                            </div>

                        </div>
                        <!--/.Card : Dynamic content wrapper-->

                        <div class="card mb-4 text-center wow fadeIn">

                            <div class="card-header">Score Points</div>

                            <!--Card content-->
                            <div class="card-body">
                            	<div class="list-group-flush text-left">
								  <div class="list-group-item pl-0">
								    <p class="mb-0"> Add Scores <a class="btn btn-sm btn-round btn-primary"> 1 </a>
								    <a class="btn btn-sm btn-round btn-primary"> 2 </a>
								    <a class="btn btn-sm btn-round btn-primary"> 3 </a>
								    <a class="btn btn-sm btn-round btn-primary"> 4 </a>
								    <a class="btn btn-sm btn-round btn-primary"> 5 </a> </p>
								  </div>
								  

								</div>
                            </div>

                        </div>

                    </div>
                    <!--Grid column-->

                </div>
                <!--Grid row-->

            </section>
            <!--Section: Post-->

        </div>
    </main>
    <!--Main layout-->

    <!--Footer-->
    <footer class="page-footer text-center font-small mdb-color darken-2 mt-4 wow fadeIn">

        <!--Call to action-->
        <div class="pt-4">
            <a class="btn btn-outline-white" href="response.php?token=manaatakgarchu&response_id=<?=$id-1?>" target="_blank" role="button">
                <i class="waves-effect mdb-icon-copy fas fa-angle-double-left fa-2x"></i> Previous
            </a>
            <a class="btn btn-outline-white" href="response.php?token=manaatakgarchu&response_id=<?=$id+1?>" target="_blank" role="button">Next 
                <i class="waves-effect mdb-icon-copy fas fa-angle-double-right fa-2x"></i>
            </a>
        </div>
        <!--/.Call to action-->

        <hr class="my-4">

        <!-- Social icons -->
        <div class="pb-4">
            <a href="" target="_blank">
                <i class="fab fa-facebook-f mr-3"></i>
            </a>

            <a href="" target="_blank">
                <i class="fab fa-twitter mr-3"></i>
            </a>

            <a href="" target="_blank">
                <i class="fab fa-youtube mr-3"></i>
            </a>

            <a href="" target="_blank">
                <i class="fab fa-google-plus-g mr-3"></i>
            </a>

            <a href="" target="_blank">
                <i class="fab fa-dribbble mr-3"></i>
            </a>

            <a href="" target="_blank">
                <i class="fab fa-pinterest mr-3"></i>
            </a>

            <a href="" target="_blank">
                <i class="fab fa-github mr-3"></i>
            </a>

            <a href="" target="_blank">
                <i class="fab fa-codepen mr-3"></i>
            </a>
        </div>
        <!-- Social icons -->

        <!--Copyright-->
        <div class="footer-copyright py-3">
            © 2018/19 Copyright:
            <a href="" target="_blank"> Nitfest 2019 </a>
        </div>
        <!--/.Copyright-->

    </footer>
    <!--/.Footer-->

    <!-- SCRIPTS -->
    <!-- JQuery -->
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb.min.js"></script>
    <!-- Initializations -->
</body>

</html>



<?php
  } 
}
?>
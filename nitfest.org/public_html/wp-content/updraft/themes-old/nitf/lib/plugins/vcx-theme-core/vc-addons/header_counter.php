<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}


add_shortcode( 'vcx_header_counter', 'vcx_header_counter_function');

/**
 *  Title Section Short Code
 * @param $atts
 * @return string
 */


function vcx_header_counter_function($atts)
{
    extract(shortcode_atts(array(
        'vcx_count_date'       => '2019/12/15 0:0:0',
        'vcx_text_day'         => 'Days',
        'vcx_text_hour'        => 'Hours',
        'vcx_text_min'         => 'Minutes',
        'vcx_text_sec'         => 'Seconds',
        'counter_number'       => '29',
        'counter_post_text'    => 'November',
        'location'             => '23-27 September, 2018.',
        'location_icon'        => 'fa-calendar',
        'counter_type'         => 'simple',
        'bg_image'             => '',
        'circle_top_color'     => '#ec398b',
        'circle_bottom_color'  => '#554bb9',

    ), $atts));


    if(!empty ($atts['bg_image'])) {
        $bg_img_box = wp_get_attachment_image_src($atts['bg_image'], 'full');
    }

    $bg_style =  (!(empty($bg_img_box[0]))) ? 'style="background: url('.$bg_img_box[0].') top center no-repeat;"' : '';


    $circle_top_color   = vcx_theme_core_hex2rgb($circle_top_color);
    $circle_bottom_color = vcx_theme_core_hex2rgb($circle_bottom_color);


    $gradiant_color = 'background: -moz-linear-gradient(top,rgba('.$circle_top_color.',.9) 0,rgba('.$circle_bottom_color.',.9) 100%);
                        background: -webkit-gradient(linear,bottom top,bottom top,color-stop(0,rgba('.$circle_top_color.',.9)),color-stop(100%,rgba('.$circle_bottom_color.',.9)));
                        background: -webkit-linear-gradient(top,rgba('.$circle_top_color.',.9) 0,rgba('.$circle_bottom_color.',.9) 100%);
                        background: -o-linear-gradient(top,rgba('.$circle_top_color.',.9) 0,rgba('.$circle_bottom_color.',.9) 100%);
                        background: -ms-linear-gradient(top,rgba('.$circle_top_color.',.9) 0,rgba('.$circle_bottom_color.',.9) 100%);
                        background: linear-gradient(to top,rgba('.$circle_top_color.',.9) 0,rgba('.$circle_bottom_color.',.9) 100%);
                        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="rgba('.$circle_top_color.', .9)", endColorstr="rgba('.$circle_bottom_color.', 0.9)", GradientType=0);
                        ';


    $gradiant_style = 'style="'.$gradiant_color.'"';


    // Location
    $location_icon = !empty($location_icon) ? '<i class="fa '.esc_attr($location_icon).'"></i>' : '';
    $location_html = !empty($location) ? '<h3 class="location">'.$location_icon .' '. esc_html($location) .'</h3>' : '';

    // Counter Up
    $counter_html =(!empty ($counter_number)) ? '<h3 class="date"><b class="lgx-counter">'.intval($counter_number).'</b> <span>'.$counter_post_text.'</span></h3>' : '' ;

    //Countdown
    $countdown_html = '<div class="lgx-countdown-area-hc">
                        <div id="lgx-countdown-hc" class="lgx-countdown lgx-countdown-hc" data-vday="'.$vcx_text_day.'" data-vhour="'.$vcx_text_hour.'" data-vmin="'.$vcx_text_min.'" data-vsec="'.$vcx_text_sec.'" data-date="'. esc_html($vcx_count_date) .'"></div>
                    </div>';


    $output_circle = '<div class="lgx-banner-info-circle"  '.$gradiant_style.' > <!-- add gradiant_style here --> 
                            <div class="info-circle-inner" '.$bg_style.'>
                                '.$counter_html.'
                                 '. (!empty($vcx_count_date) ? $countdown_html  : '') .'
                            </div>
                        </div>';

    $output_simple = ' <div class="lgx-banner-info-date">                        
                        '.$counter_html.'
                        '.$location_html.'
                        '. (!empty($vcx_count_date) ? $countdown_html  : '') .'
                    </div>';
    
    $output = ($counter_type == 'circular') ? $output_circle : $output_simple;


    return '<div class="vcx-header-counter">'.$output.'</div>';
}



/**
 * Visual Composer
 */

if (class_exists('WPBakeryVisualComposerAbstract')) {
    vc_map(array(
        "name" => esc_html__("Header Counter", 'vcx-theme-core'),
        "base" => "vcx_header_counter",
        "class" => "",
        "description" => esc_html__("Display Header Counter Up.", 'vcx-theme-core'),
        "category" => esc_html__('Emeet', 'vcx-theme-core'),
        "params" => array(

            array(
                "type" 			=> "textfield",
                "admin_label"   => true,
                "heading" 		=> esc_html__("Counter Up Number", "vcx-theme-core"),
                "param_name" 	=> "counter_number",
                "value" 		=> '29',
            ),

            array(
                "type" 			=> "textfield",
                "admin_label"   => true,
                "heading" 		=> esc_html__("Counter Post Text", "vcx-theme-core"),
                "param_name" 	=> "counter_post_text",
                "value" 		=> 'November',
            ),

            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("Counter Type", 'vcx-theme-core'),
                "param_name" 	=> "counter_type",
                "value" 		=> array('Simple'=>'simple', 'Circle'=>'circular'),
            ),

            array(
                "type" 			=> "textfield",
                "admin_label"   => true,
                "heading" 		=> esc_html__("Location", "vcx-theme-core"),
                "param_name" 	=> "location",
                "value" 		=> '21 King Street, Duach, United State.',
                "description"   => esc_html__("Add Location Here for Simple type", "vcx-theme-core"),

            ),

            array(
                "type" 			=> "textfield",
                "admin_label"   => true,
                "heading" 		=> esc_html__("Location Icon", "vcx-theme-core"),
                "param_name" 	=> "location_icon",
                "value" 		=> 'fa-map-marker',
                "description"   => esc_html__(" Add Font Awesome Icon Class name Here. e.g. fa-map-marker", "vcx-theme-core"),

            ),

            array(
                "type" 			=> "textfield",
                "admin_label"   => true,
                "heading" 		=> esc_html__("Countdown Date", "vcx-theme-core"),
                "description"   => esc_html__("Important: Date Format Must be: Y/m/d . For Example: 2019/10/5 0:0:0", "vcx-theme-core"),
                "param_name" 	=> "vcx_count_date",
                "value" 		=> '2019/12/15 0:0:0',
            ),

            array(
                "type" 			=> "textfield",
                "admin_label"   => true,
                "heading" 		=> esc_html__("Countdown Day Text", "vcx-theme-core"),
                "param_name" 	=> "vcx_text_day",
                "value" 		=> 'Days',
            ),

            array(
                "type" 			=> "textfield",
                "admin_label"   => true,
                "heading" 		=> esc_html__("Countdown Hour Text", "vcx-theme-core"),
                "param_name" 	=> "vcx_text_hour",
                "value" 		=> 'Hours',
            ),

            array(
                "type" 			=> "textfield",
                "admin_label"   => true,
                "heading" 		=> esc_html__("Countdown Minute Text", "vcx-theme-core"),
                "param_name" 	=> "vcx_text_min",
                "value" 		=> 'Minutes',
            ),

            array(
                "type" 			=> "textfield",
                "admin_label"   => true,
                "heading" 		=> esc_html__("Countdown Seconds Text", "vcx-theme-core"),
                "param_name" 	=> "vcx_text_sec",
                "value" 		=> 'Seconds',
            ),

            array(
                "type" 			=> "attach_image",
                "heading" 		=> esc_html__("Upload Circle Background Image", "lgx-themential"),
                "param_name" 	=> "bg_image",
                "value" 		=> "",
            ),

            array(
                "type"          => "colorpicker",
                "heading"       => esc_html__("Circle Top Color", "vcx-theme-core"),
                "param_name"    => "circle_bottom_color",
                "value"         => "#554bb9",
            ),


            array(
                "type"          => "colorpicker",
                "heading"       => esc_html__("Circle Bottom Color", "vcx-theme-core"),
                "param_name"    => "circle_top_color",
                "value"         => "#ec398b",
            ),

        )
    ));
}


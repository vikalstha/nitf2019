��    7      �      �      �     �  	   �     �     �     �  
   �     �     �     �  	   �     �                      4   +  �   `  
   �     �     �                %  
   ,     7     >     F     Z     `     o     �  
   �     �     �     �     �     �     �     �     �     �     �            �        �     �  	   �     �     �     �     �       	   
  �                  "     4     G     Z     j     p     w  
   �     �     �     �     �     �  J   �  �   	     �	     �	     �	     �	     �	     �	  
   
     
     
     
     6
     ;
     Y
     s
     z
     �
  
   �
     �
     �
     �
     �
     �
       	        "  	   +     5  �   I            	        "     (     4     C     I     R   Active Filters Afternoon All Day Autocomplete Available Filters Checkboxes City Closed Collapse Filters Cost (%s) Country Day Dropdown Evening Event Category Events are considered free when cost field is: %s %s Expand an active filter to edit the label and choose from a subset of input types (dropdown, select, range slider, checkbox and radio). Filter Bar Filters Filters Default State Filters Layout Free Friday Horizontal Monday Morning Narrow Your Results Night No items match Only when set to zero Open Organizers Other Range Slider Reset Filters Saturday Select Select an Item Show Advanced Filters Show Filters Submit Sunday Tags The Events Calendar The settings below allow you to enable or disable front-end event filters. Uncheck the box to hide the filter. Drag and drop active filters to re-arrange them. Thursday Time Title: %s Tuesday Type: %s %s Type: %s %s %s Venues Vertical Wednesday PO-Revision-Date: 2017-01-27 16:27:27+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: GlotPress/1.0-alpha-1100
Project-Id-Version: Filter Bar
 Filtres Actifs Après-midi Toute la journée Saisie automatique Filtres Disponible Cases à cocher Ville Fermé Masquer les filtres Coût (%s) Pays Jour Menu-déroulant Soir Catégorie de l'événement Les événements sont considérés gratuit quand le champ Coût est: %s %s Agrandir un filtre actif pour modifier son nom et choisir son type (menu déroulant, selection, glissière, case à cocher et bouton radio). Barre des filtres Filtres État Par Défaut Des Filtres Apparence Des Filtres Gratuit Vendredi Horizontal Lundi Matin Filtrez vos résultats Nuit Aucun élément ne correspond Seulement si mit à zéro Ouvert Organisateurs Autres Glissière Remettre à zéro les filtres Samedi Sélectionner Choisissez un élément Afficher les filtres avancés Afficher les filtres Soumettre Dimanche Libellés The Events Calendar Les paramètres ci-dessous vous permette d'activer ou de désactiver les filtres de la face-avant. Décochez la case pour cacher le filtre. Cliquez et déplacez les filtres pour les réarranger. Jeudi Heure Titre: %s Mardi Type: %s %s Type: %s %s %s Lieux Vertical Mercredi 
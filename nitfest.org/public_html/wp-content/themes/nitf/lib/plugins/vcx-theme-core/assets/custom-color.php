<?php

add_action ('wp_head','vcx_style_hook_inHeader');



function vcx_style_hook_inHeader() {;


    global $emeet_options;

    if(isset($emeet_options['custom_color_en'])) {
        $custom_color_en = intval($emeet_options['custom_color_en']);

        $brand_color = (!empty($emeet_options['brand-color'])) ? $emeet_options['brand-color'] : '#554bb9' ;
        $brand_color2 = (!empty($emeet_options['brand-color2'])) ? $emeet_options['brand-color2'] : '#ec398b' ;
        $brand_color3 = (!empty($emeet_options['brand-color3'])) ? $emeet_options['brand-color3'] : '#efa506' ;
        $brand_color4 = (!empty($emeet_options['brand-color4'])) ? $emeet_options['brand-color4'] : '#00acee' ;

    }


    if ($custom_color_en != 1)  {
        return;
    }

    $brand_rgb  = vcx_theme_core_hex2rgb($brand_color);

    ?>
    <style type="text/css">

        h3 a:hover,
        .h3 a:hover {
            color: <?php echo $brand_color; ?>;
        }

        h4 a:hover,
        .h4 a:hover {
            color: <?php echo $brand_color; ?>;
        }

        .lgx-table thead tr,
        table thead tr {
            background: rgba(<?php echo $brand_rgb; ?>, 0.2);
        }

        .lgx-table tbody tr td,
        table tbody tr td,
        .lgx-table tbody tr th,
        table tbody tr th {
            background: rgba(<?php echo $brand_rgb; ?>, 0.05);
        }
        .lgx-table tbody tr td:hover,
        table tbody tr td:hover,
        .lgx-table tbody tr th:hover,
        table tbody tr th:hover {
            background: rgba(<?php echo $brand_rgb; ?>, 0.1);
        }
        .lgx-table tbody tr th,
        table tbody tr th {
            background: rgba(<?php echo $brand_rgb; ?>, 0.1);
        }

        a {
            color:  <?php echo $brand_color; ?>;
        }
        a:focus,
        a:hover,
        a.active {
            color: rgba(<?php echo $brand_rgb; ?>, 0.8);
        }

        blockquote footer {
            color: <?php echo $brand_color; ?>;
        }
        blockquote:after {
            color: <?php echo $brand_color; ?>;
        }
        blockquote:before {
            color: <?php echo $brand_color; ?>;
        }

        .lgx-social li {
            color: <?php echo $brand_color; ?>;
        }

        .lgx-social li a .fa-twitter {
            color: #1da1f2;
        }
        .lgx-social li a .facebook-f,
        .lgx-social li a .fa-facebook-f,
        .lgx-social li a .fa-facebook {
            color: #3b5998;
        }

        .lgx-social li:hover {
            background: <?php echo $brand_color; ?>;
            color: <?php echo $brand_color; ?>;
        }

        .lgx-social:hover > li {
            color: <?php echo $brand_color; ?>;
        }

        .lgx-social:hover > li:hover a {
            background: <?php echo $brand_color3; ?>;
        }

        .lgx-heading .heading span {
            background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 80%, <?php echo $brand_color3; ?> 80%);
            background-size: 100px 1.14em;
        }
        .lgx-heading .subheading {
            color: <?php echo $brand_color; ?>;
        }
        .lgx-heading-white .heading span {
            background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 80%, <?php echo $brand_color2; ?> 80%);
            background-size: 100px 1.14em;
        }
        .lgx-heading-white .subheading {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-subheading {
            color: <?php echo $brand_color; ?>;
        }
        .comment-reply-link,
        .comment-edit-link,
        .comment-form-submit,
        .wpcf7-submit,
        .lgx-submit,
        input[type=submit],
        .woocommerce #respond input#submit,
        .woocommerce a.button,
        .woocommerce button.button,
        .woocommerce input.button,
        .wcppec-checkout-buttons__button,
        .lgx-btn,
        .woocommerce-Address-title .edit,
        .lgx-btn-simple {
            background: <?php echo $brand_color; ?>;
        }
        .comment-reply-link:hover,
        .comment-edit-link:hover,
        .comment-form-submit:hover,
        .wpcf7-submit:hover,
        .lgx-submit:hover,
        input[type=submit]:hover,
        .woocommerce #respond input#submit:hover,
        .woocommerce a.button:hover,
        .woocommerce button.button:hover,
        .woocommerce input.button:hover,
        .wcppec-checkout-buttons__button:hover,
        .lgx-btn:hover,
        .woocommerce-Address-title .edit:hover,
        .lgx-btn-simple:hover {
            background: <?php echo $brand_color; ?>;
            -webkit-box-shadow: 0 14px 26px -12px rgba(50, 51, 51, 0.42), 0 4px 23px 0 rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(50, 51, 51, 0.2);
            box-shadow: 0 14px 26px -12px rgba(50, 51, 51, 0.42), 0 4px 23px 0 rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(50, 51, 51, 0.2);
        }
        .woocommerce #respond input#submit.alt:hover,
        .woocommerce a.button.alt:hover,
        .woocommerce button.button.alt:hover,
        .woocommerce input.button.alt:hover {
            background-color: <?php echo $brand_color3; ?>;
        }
        .panel-body .lgx-btn-simple {
            background: <?php echo $brand_color3; ?>;
        }

        .lgx-btn-brand2,
        .lgx-btn-red {
            background: <?php echo $brand_color2; ?>;
        }

        .lgx-btn-brand2:hover,
        .lgx-btn-red:hover {
            background: <?php echo $brand_color2; ?>;
            -webkit-box-shadow: 0 14px 26px -12px rgba(50, 51, 51, 0.42), 0 4px 23px 0 rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(50, 51, 51, 0.2);
            box-shadow: 0 14px 26px -12px rgba(50, 51, 51, 0.42), 0 4px 23px 0 rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(50, 51, 51, 0.2);
        }
        .lgx-btn-yellow {
            background: <?php echo $brand_color3; ?>;
        }
        .lgx-btn-yellow:hover {
            -webkit-box-shadow: 0 14px 26px -12px rgba(50, 51, 51, 0.42), 0 4px 23px 0 rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(50, 51, 51, 0.2);
            box-shadow: 0 14px 26px -12px rgba(50, 51, 51, 0.42), 0 4px 23px 0 rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(50, 51, 51, 0.2);
        }
        .lgx-btn-white {
            color: <?php echo $brand_color; ?> !important;
        }
        .mfp-wrap .mfp-arrow {
            background: <?php echo $brand_color3; ?>;
        }

        .lgx-header .lgx-navbar .lgx-nav .lgx-btn {
            background: <?php echo $brand_color2; ?>;
        }
        .lgx-header .lgx-navbar .lgx-nav .lgx-btn:hover {
            -webkit-box-shadow: 0 14px 26px -12px rgba(50, 51, 51, 0.42), 0 4px 23px 0 rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(50, 51, 51, 0.2);
            box-shadow: 0 14px 26px -12px rgba(50, 51, 51, 0.42), 0 4px 23px 0 rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(50, 51, 51, 0.2);
        }

        .lgx-header .lgx-navbar .lgx-nav li a:hover {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-header .lgx-navbar .lgx-nav .active a {
            color: <?php echo $brand_color3; ?>;
        }

        .lgx-header .lgx-navbar .lgx-nav .dropdown-menu {
            background: rgba(<?php echo $brand_rgb; ?>, 0.98);
        }
        .lgx-header .lgx-navbar .lgx-nav .dropdown-menu li a:focus {
            color: <?php echo $brand_color; ?>;
        }
        .lgx-header .lgx-nav-right .lgx-search a:hover {
            color: <?php echo $brand_color; ?>;
        }
        .navbar-default .navbar-nav > .open > a,
        .navbar-default .navbar-nav > .open > a:focus,
        .navbar-default .navbar-nav > .open > a:hover {
            color: <?php echo $brand_color3; ?> !important;
        }

        .header-top .header-top-inner .contact ul li i {
            color: <?php echo $brand_color; ?>;
        }
        .header-top .header-top-inner .contact ul .question-text {
            color: <?php echo $brand_color; ?>;
        }
        .header-top .header-top-inner .right-menu .login-register a {
            color: <?php echo $brand_color; ?>;
        }
        .header-top .header-top-inner .right-menu .login-register a:hover {
            background: <?php echo $brand_color; ?>;
        }

        .header-top .header-top-inner .right-menu ul li:hover a {
            color: <?php echo $brand_color; ?>;
        }
        .lgx-header.lgx-navbar-style-menu-container .lgx-header-position .lgx-navbar {
            background: <?php echo $brand_color; ?>;
        }
        .lgx-header.lgx-navbar-style-brand-tp .lgx-header-position {
            background: rgba(<?php echo $brand_rgb; ?>, 0.9);
        }
        .lgx-header .menu-onscroll .lgx-nav-right .lgx-search a:hover {
            color: <?php echo $brand_color; ?>;
        }
        .lgx-header .menu-onscroll .lgx-toggle {
            color: <?php echo $brand_color; ?>;
        }
        .lgx-header .menu-onscroll .lgx-navbar .lgx-nav .active a {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-header.lgx-navbar-style-brand-tp .lgx-header-position.menu-onscroll {
            background: <?php echo $brand_color; ?>;
        }
        .navbar-default .navbar-toggle span {
            background-color: <?php echo $brand_color; ?> !important;
        }
        .navbar-default .navbar-toggle:focus,
        .navbar-default .navbar-toggle:hover {
            background-color: <?php echo $brand_color; ?>;
        }
        .lgx-header .lgx-navbar .lgx-nav .dropdown-menu li a:hover {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-header .lgx-navbar .lgx-nav .dropdown-menu li a:focus {
            color: <?php echo $brand_color3; ?>;
        }
        .navbar-default .navbar-nav .open .dropdown-menu > .active > a,
        .navbar-default .navbar-nav .open .dropdown-menu > .active > a:focus,
        .navbar-default .navbar-nav .open .dropdown-menu > .active > a:hover {
            color: <?php echo $brand_color2; ?>;
        }
        .lgx-banner-info .subtitle {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-banner-info .sm-title b:nth-of-type(1) {
            color: <?php echo $brand_color2; ?>;
        }
        .lgx-banner-info .sm-title b:nth-of-type(2) {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-banner-info .sm-title b:nth-of-type(3) {
            color: <?php echo $brand_color4; ?>;
        }
        .lgx-banner-info .title span:nth-of-type(1),
        .lgx-banner-info .title span:nth-of-type(4) {
            background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 80%, <?php echo $brand_color2; ?> 80%);
        }
        .lgx-banner-info .title span:nth-of-type(2),
        .lgx-banner-info .title span:nth-of-type(5) {
            background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 80%, <?php echo $brand_color3; ?> 80%);
        }
        .lgx-banner-info .title span:nth-of-type(3),
        .lgx-banner-info .title span:nth-of-type(6) {
            background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 80%, <?php echo $brand_color4; ?> 80%);
        }
        .lgx-banner-info .title span:nth-of-type(4),
        .lgx-banner-info .title span:nth-of-type(7) {
            background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 80%, <?php echo $brand_color; ?> 80%);
        }
        .lgx-banner-info .title b {
            color: <?php echo $brand_color; ?>;
        }
        .lgx-banner-info .title b:nth-of-type(1) {
            color: <?php echo $brand_color2; ?>;
        }
        .lgx-banner-info .title b:nth-of-type(2) {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-banner-info .title b:nth-of-type(3) {
            color: <?php echo $brand_color4; ?>;
        }
        .lgx-banner-info .title b:nth-of-type(4) {
            color: #fff;
        }
        .lgx-banner-info .date i {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-banner-info .location i {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-banner-info .action-area .video-area a:hover i {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-header-color-dark .subtitle {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-header-color-dark .sm-title b:nth-of-type(1) {
            color: <?php echo $brand_color2; ?>;
        }
        .lgx-header-color-dark .sm-title b:nth-of-type(2) {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-header-color-dark .sm-title b:nth-of-type(3) {
            color: <?php echo $brand_color4; ?>;
        }
        .lgx-header-color-dark .title span:nth-of-type(1),
        .lgx-header-color-dark .title span:nth-of-type(4) {
            background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 80%, <?php echo $brand_color2; ?> 80%);
        }
        .lgx-header-color-dark .title span:nth-of-type(2),
        .lgx-header-color-dark .title span:nth-of-type(5) {
            background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 80%, <?php echo $brand_color3; ?> 80%);
        }
        .lgx-header-color-dark .title span:nth-of-type(3),
        .lgx-header-color-dark .title span:nth-of-type(6) {
            background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 80%, <?php echo $brand_color4; ?> 80%);
        }
        .lgx-header-color-dark .title span:nth-of-type(4),
        .lgx-header-color-dark .title span:nth-of-type(7) {
            background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 80%, <?php echo $brand_color; ?> 80%);
        }
        .lgx-header-color-dark .title b {
            font-weight: 900;
            color: <?php echo $brand_color; ?>;
        }
        .lgx-header-color-dark .title b:nth-of-type(1) {
            color: <?php echo $brand_color2; ?>;
        }
        .lgx-header-color-dark .title b:nth-of-type(2) {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-header-color-dark .title b:nth-of-type(3) {
            color: <?php echo $brand_color4; ?>;
        }
        .lgx-header-color-dark .date i {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-header-color-dark .location i {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-header-color-dark .action-area .video-area a:hover i {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-banner-info-circle .info-circle-inner .date {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-banner-info-date .date {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-banner-info-date .location i {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-banner-info-homeone .subtitle {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-banner-info-homeone .title span b:nth-child(1) {
            color: <?php echo $brand_color2; ?>;
        }
        .lgx-banner-info-homeone .title span b:nth-child(2) {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-banner-info-homeone .title span b:nth-child(3) {
            color: <?php echo $brand_color4; ?>;
        }
        .lgx-slider-center .lgx-banner-info .subtitle {
            color: <?php echo $brand_color; ?>;
        }
        .lgx-banner-info2 .subtitle {
            color: <?php echo $brand_color; ?>;
        }

        @media (max-width: 767px) {
            .lgx-about-circle .lgx-countdown {
                background: <?php echo $brand_color; ?>;
            }
        }
        .owl-theme .owl-dots .owl-dot.active span,
        .owl-theme .owl-dots .owl-dot:hover span {
            background: <?php echo $brand_color; ?>;
        }
        .lgx-about-threeimg:after {
            border: 16px solid <?php echo $brand_color; ?>;
        }
        .about-date-area .date {
            color: <?php echo $brand_color; ?>;
        }
        .lgx-about-content-area .lgx-heading .subheading {
            color: <?php echo $brand_color2; ?>;
        }
        .lgx-about-service .lgx-single-service-white .icon {
            color: <?php echo $brand_color2; ?>;
        }
        .lgx-single-speaker figure figcaption a {
            color: <?php echo $brand_color; ?>;
        }

        .lgx-single-speaker .social-group a:hover {
            color: <?php echo $brand_color3; ?>;
        }

        .lgx-single-speaker .speaker-info .title {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-single-speaker:hover {
            background: <?php echo $brand_color; ?>;
        }
        .lgx-single-speaker-default figure figcaption {
            background: rgba(<?php echo $brand_rgb; ?>, 0.9);
        }
        .lgx-single-speaker-default figure figcaption a {
            color: <?php echo $brand_color; ?>;
        }

        .lgx-single-speaker-default:hover {
            background: <?php echo $brand_color; ?>;
        }
        .lgx-single-speaker-circle figure figcaption a {
            color: <?php echo $brand_color; ?>;
        }
        .lgx-single-speaker-circle .social-group {
            background: <?php echo $brand_color; ?>;
        }
        .lgx-single-speaker-one figure figcaption a {
            color: <?php echo $brand_color; ?>;
        }
        .lgx-single-speaker-one .social-group {
            background: <?php echo $brand_color; ?>;
        }
        .lgx-single-speaker-one:hover .speaker-info .subtitle {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-single-speaker-two {
            background: rgba(239, 165, 6, 0.4);
        }
        .lgx-single-speaker-two figure {
            background: <?php echo $brand_color; ?>;
        }
        .lgx-single-speaker-two .social-group a:hover {
            background: <?php echo $brand_color; ?>;
        }
        .lgx-single-speaker-three .social-group a {
            color: <?php echo $brand_color; ?>;
        }
        .lgx-single-speaker-three .social-group a:hover {
            background: <?php echo $brand_color; ?>;
        }
        .lgx-counter-area i {
            color: <?php echo $brand_color; ?>;
        }

        .lgx-milestone-box .lgx-counter-area i {
            color: <?php echo $brand_color; ?>;
        }
        .lgx-tab .lgx-nav-colorful li:nth-child(1) {
            background: <?php echo $brand_color3; ?>;
        }
        .lgx-tab .lgx-nav-colorful li:nth-child(2) {
            background: #ff8a00;
        }
        .lgx-tab .lgx-nav-colorful li:nth-child(3) {
            background: #00b9ff;
        }
        .lgx-tab .lgx-nav-colorful li:nth-child(4) {
            background: #8dc63f;
        }
        .lgx-tab .lgx-nav-colorful li:nth-child(5) {
            background: #6ba229;
        }
        .lgx-tab .lgx-nav-colorful .active {
            background: <?php echo $brand_color; ?> !important;
        }
        .lgx-tab .nav-pills {
            background: <?php echo $brand_color; ?>;
        }
        .lgx-tab .nav-pills .active a h3 {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-tab .lgx-nav-background li {
            background: <?php echo $brand_color; ?>;
        }
        .lgx-tab .nav-pills.lgx-nav-background .active {
            background: <?php echo $brand_color3; ?>;
        }
        .lgx-tab .nav-pills.lgx-nav-background .active a h3 {
            color: <?php echo $brand_color; ?>;
        }
        .lgx-tab-content {
            background: rgba(<?php echo $brand_rgb; ?>, 0.8);
            border: 2px solid <?php echo $brand_color; ?>;
        }
        .lgx-panel .lgx-single-schedule .author img {
            border: 2px solid <?php echo $brand_color3; ?>;
        }
        .lgx-panel .lgx-single-schedule .schedule-info .time {
            background: rgba(239, 165, 6, 0.1);
        }
        .lgx-panel .lgx-single-schedule .schedule-info .time span {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-panel .lgx-single-schedule .schedule-info .author-info span {
            color: <?php echo $brand_color2; ?>;
        }
        .lgx-panel .panel-body .location strong {
            color: <?php echo $brand_color; ?>;
        }
        .lgx-panel .panel-body .location span {
            color: <?php echo $brand_color; ?>;
        }
        .lgx-panel:last-child {
            border-bottom: 1px solid transparent;
        }
        .sponso<?php echo $brand_color; ?>-heading {
            color: <?php echo $brand_color; ?>;
        }
        .sponso<?php echo $brand_color; ?>-heading.secound-heading {
            color: <?php echo $brand_color2; ?>;
        }
        .sponso<?php echo $brand_color; ?>-heading.third-heading {
            color: <?php echo $brand_color3; ?>;
        }
        .sponsors-area-border .single:hover {
            border: 5px solid <?php echo $brand_color; ?>;
        }
        .sponsors-area-color-tile .single:nth-child(odd) {
            background: <?php echo $brand_color; ?>;
        }
        .sponsors-area-colorfull .single {
            background: <?php echo $brand_color; ?>;
        }
        .sponsors-area-colorfull .single:nth-child(1) {
            background: #ff6600;
        }
        .sponsors-area-colorfull .single:nth-child(2) {
            background: #ff9400;
        }
        .sponsors-area-colorfull .single:nth-child(3) {
            background: #ffc600;
        }
        .sponsors-area-colorfull .single:nth-child(4) {
            background: #8cc700;
        }
        .sponsors-area-colorfull .single:nth-child(5) {
            background: #0fad00;
        }
        .sponsors-area-colorfull .single:nth-child(6) {
            background: #00a3c1;
        }
        .sponsors-area-colorfull .single:nth-child(7) {
            background: #0061b5;
        }
        .sponsors-area-colorfull .single:nth-child(8) {
            background: #6300a4;
        }
        .sponsors-area-colorfull .single:nth-child(9) {
            background: #ff0000;
        }
        .sponsors-area-colorfull .single:nth-child(10) {
            background: #ff6600;
        }
        .sponsors-area-colorfull .single:nth-child(11) {
            background: #ff9400;
        }
        .sponsors-area-colorfull .single:nth-child(12) {
            background: #ffc600;
        }
        .sponsors-area-colorfull .single:nth-child(13) {
            background: #8cc700;
        }
        .sponsors-area-colorfull .single:nth-child(14) {
            background: #0fad00;
        }
        .sponsors-area-colorfull .single:nth-child(15) {
            background: #00a3c1;
        }
        .sponsors-area-colorfull .single:nth-child(16) {
            background: #0061b5;
        }
        .sponsors-area-colorfull .single:nth-child(17) {
            background: #6300a4;
        }
        .sponsors-area-colorfull .single:nth-child(18) {
            background: #ff0000;
        }
        .sponsors-area-colorfull .single:nth-child(19) {
            background: #ff6600;
        }
        .sponsors-area-colorfull .single:nth-child(20) {
            background: #ff9400;
        }
        .sponsors-area-colorfull .single:nth-child(21) {
            background: #ffc600;
        }
        .sponsors-area-colorfull .single:nth-child(22) {
            background: #8cc700;
        }
        .sponsors-area-colorfull .single:nth-child(23) {
            background: #0fad00;
        }
        .sponsors-area-colorfull .single:nth-child(24) {
            background: #00a3c1;
        }
        .sponsors-area-colorfull .single:nth-child(25) {
            background: #0061b5;
        }
        .sponsors-area-colorfull .single:nth-child(26) {
            background: #6300a4;
        }
        .sponsors-area-colorfull .single:nth-child(27) {
            background: #ff0000;
        }
        .sponsors-area-colorfull .single:nth-child(28) {
            background: #ff6600;
        }
        .sponsors-area-colorfull .single:nth-child(29) {
            background: #ff9400;
        }
        .sponsors-area-colorfull .single:nth-child(30) {
            background: #ffc600;
        }
        .sponsors-area-colorfull .single:nth-child(31) {
            background: #8cc700;
        }
        .sponsors-area-colorfull .single:nth-child(32) {
            background: #0fad00;
        }
        .sponsors-area-colorfull .single:nth-child(33) {
            background: #00a3c1;
        }
        .sponsors-area-colorfull .single:nth-child(34) {
            background: #0061b5;
        }
        .sponsors-area-colorfull .single:nth-child(35) {
            background: #6300a4;
        }
        .sponsors-area-colorfull .single:nth-child(36) {
            background: #ff0000;
        }
        .sponsors-area-colorfull-border .single:nth-child(1) {
            border: 8px solid #ff6600;
        }
        .sponsors-area-colorfull-border .single:nth-child(2) {
            border: 8px solid #ff9400;
        }
        .sponsors-area-colorfull-border .single:nth-child(3) {
            border: 8px solid #ffc600;
        }
        .sponsors-area-colorfull-border .single:nth-child(4) {
            border: 8px solid #8cc700;
        }
        .sponsors-area-colorfull-border .single:nth-child(5) {
            border: 8px solid #0fad00;
        }
        .sponsors-area-colorfull-border .single:nth-child(6) {
            border: 8px solid #00a3c1;
        }
        .sponsors-area-colorfull-border .single:nth-child(7) {
            border: 8px solid #0061b5;
        }
        .sponsors-area-colorfull-border .single:nth-child(8) {
            border: 8px solid #6300a4;
        }
        .sponsors-area-colorfull-border .single:nth-child(9) {
            border: 8px solid #ff0000;
        }
        .sponsors-area-colorfull-border .single:nth-child(10) {
            border: 8px solid #ff6600;
        }
        .sponsors-area-colorfull-border .single:nth-child(11) {
            border: 8px solid #ff9400;
        }
        .sponsors-area-colorfull-border .single:nth-child(12) {
            border: 8px solid #ffc600;
        }
        .sponsors-area-colorfull-border .single:nth-child(13) {
            border: 8px solid #8cc700;
        }
        .sponsors-area-colorfull-border .single:nth-child(14) {
            border: 8px solid #0fad00;
        }
        .sponsors-area-colorfull-border .single:nth-child(15) {
            border: 8px solid #00a3c1;
        }
        .sponsors-area-colorfull-border .single:nth-child(16) {
            border: 8px solid #0061b5;
        }
        .sponsors-area-colorfull-border .single:nth-child(17) {
            border: 8px solid #6300a4;
        }
        .sponsors-area-colorfull-border .single:nth-child(18) {
            border: 8px solid #ff0000;
        }
        .sponsors-area-colorfull-border .single:nth-child(19) {
            border: 8px solid #ff6600;
        }
        .sponsors-area-colorfull-border .single:nth-child(20) {
            border: 8px solid #ff9400;
        }
        .sponsors-area-colorfull-border .single:nth-child(21) {
            border: 8px solid #ffc600;
        }
        .sponsors-area-colorfull-border .single:nth-child(22) {
            border: 8px solid #8cc700;
        }
        .sponsors-area-colorfull-border .single:nth-child(23) {
            border: 8px solid #0fad00;
        }
        .sponsors-area-colorfull-border .single:nth-child(24) {
            border: 8px solid #00a3c1;
        }
        .sponsors-area-colorfull-border .single:nth-child(25) {
            border: 8px solid #0061b5;
        }
        .sponsors-area-colorfull-border .single:nth-child(26) {
            border: 8px solid #6300a4;
        }
        .sponsors-area-colorfull-border .single:nth-child(27) {
            border: 8px solid #ff0000;
        }
        .sponsors-area-colorfull-border .single:nth-child(28) {
            border: 8px solid #ff6600;
        }
        .sponsors-area-colorfull-border .single:nth-child(29) {
            border: 8px solid #ff9400;
        }
        .sponsors-area-colorfull-border .single:nth-child(30) {
            border: 8px solid #ffc600;
        }
        .sponsors-area-colorfull-border .single:nth-child(31) {
            border: 8px solid #8cc700;
        }
        .sponsors-area-colorfull-border .single:nth-child(32) {
            border: 8px solid #0fad00;
        }
        .sponsors-area-colorfull-border .single:nth-child(33) {
            border: 8px solid #00a3c1;
        }
        .sponsors-area-colorfull-border .single:nth-child(34) {
            border: 8px solid #0061b5;
        }
        .sponsors-area-colorfull-border .single:nth-child(35) {
            border: 8px solid #6300a4;
        }
        .sponsors-area-colorfull-border .single:nth-child(36) {
            border: 8px solid #ff0000;
        }
        .lgx-single-news .single-news-info .vcx-news-date {
            color: <?php echo $brand_color2; ?>;
        }
        .lgx-single-news .meta-wrapper span a:hover {
            color: <?php echo $brand_color2; ?>;
        }
        .lgx-single-news .title a:hover {
            color: <?php echo $brand_color; ?>;
        }
        .lgx-registration-area .lgx-single-registration .lgx-btn {
            background: <?php echo $brand_color2; ?>;
        }
        .lgx-registration-area .lgx-single-registration .lgx-btn:hover {
            background: <?php echo $brand_color3; ?>;
        }
        .lgx-registration-area .lgx-single-registration .single-top .price {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-registration-area .lgx-single-registration .single-bottom ul li .fa-check {
            color: <?php echo $brand_color; ?>;
        }
        .lgx-registration-area .lgx-single-registration .single-bottom ul li .fa-times {
            color: <?php echo $brand_color; ?>;
        }
        .lgx-registration-area .lgx-single-registration:hover .single-top .title {
            color: <?php echo $brand_color; ?>;
        }
        .lgx-registration-area .lgx-single-registration:hover .single-top .price {
            color: <?php echo $brand_color3; ?>;
        }

        .lgx-registration-area-simple .lgx-single-registration .lgx-single-registration-inner {
            background: rgba(<?php echo $brand_rgb; ?>, 0.9);
        }
        .lgx-registration-area-simple .lgx-single-registration.recommended {
            background: <?php echo $brand_color; ?>;
        }
        .lgx-registration-area-special .lgx-single-registration .single-top .title {
            color: <?php echo $brand_color; ?>;
        }
        .lgx-registration-area-special .lgx-single-registration .single-top .price i {
            color: <?php echo $brand_color2; ?>;
        }
        .lgx-registration-area-colorful .lgx-single-registration:nth-child(1) {
            background: <?php echo $brand_color3; ?>;
        }
        .lgx-registration-area-colorful .lgx-single-registration:nth-child(1) .single-top .title {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-registration-area-colorful .lgx-single-registration:nth-child(2) {
            background: <?php echo $brand_color; ?>;
        }
        .lgx-registration-area-colorful .lgx-single-registration:nth-child(2) .single-top .title {
            color: <?php echo $brand_color; ?>;
        }
        .lgx-registration-area-colorful .lgx-single-registration:nth-child(3) {
            background: <?php echo $brand_color4; ?>;
        }
        .lgx-registration-area-colorful .lgx-single-registration:nth-child(3) .single-top .title {
            color: <?php echo $brand_color4; ?>;
        }
        .lgx-registration-area-colorful .lgx-single-registration:nth-child(4) {
            background: #6ba229;
        }
        .lgx-registration-area-colorful .lgx-single-registration:nth-child(4) .single-top .title {
            color: #6ba229;
        }
        .lgx-registration-area-colorful .lgx-single-registration .single-top .price {
            color: <?php echo $brand_color2; ?>;
        }
        .lgx-registration-area-colorful .recommended .lgx-btn {
            background: <?php echo $brand_color2; ?>;
        }
        .lgx-registration-area-colorful .recommended .lgx-btn:hover {
            background: <?php echo $brand_color3; ?>;
        }
        .lgx-registration-area .lgx-single-registration-christmas {
            background: <?php echo $brand_color; ?>;
        }
        .lgx-registration-area .lgx-single-registration-christmas .lgx-btn {
            background: <?php echo $brand_color2; ?>;
        }
        .lgx-registration-area .lgx-single-registration-christmas .lgx-btn:hover {
            background: <?php echo $brand_color3; ?>;
        }
        .lgx-registration-area .lgx-single-registration-christmas .single-top .price {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-registration-area .lgx-single-registration-christmas:nth-child(1) .single-top .title {
            color: <?php echo $brand_color; ?>;
        }
        .lgx-registration-area .lgx-single-registration-christmas:nth-child(1) .single-top .title a {
            color: inherit;
        }
        .lgx-registration-area .lgx-single-registration-christmas:nth-child(1) .single-top .price {
            color: <?php echo $brand_color3; ?>;
        }

        .lgx-registration-area .lgx-single-registration-christmas:hover .single-top .title {
            color: <?php echo $brand_color; ?>;
        }
        .lgx-registration-area .lgx-single-registration-christmas:hover .single-top .title a {
            color: inherit;
        }
        .lgx-registration-area .lgx-single-registration-christmas:hover .single-top .price {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-registration-area .recommended .lgx-btn {
            background: <?php echo $brand_color; ?>;
        }
        .lgx-registration-area .recommended .lgx-btn:hover {
            background: <?php echo $brand_color3; ?>;
        }
        .lgx-registration-area .recommended .single-top .title {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-registration-area .recommended .single-top .price i {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-registration-area .lgx-single-registration.recommended:hover .single-bottom ul li .fa-check {
            color: <?php echo $brand_color; ?>;
        }
        .lgx-registration-area-colorful .lgx-single-registration.recommended .single-top .price {
            color: <?php echo $brand_color2; ?>;
        }
        .lgx-registration-area-colorful .lgx-single-registration.recommended .single-top .price i {
            color: <?php echo $brand_color; ?>;
        }
        .vcx-registration-area input.wpcf7-form-control:focus,
        .vcx-registration-area input.form-control:focus {
            color: <?php echo $brand_color; ?>;
        }
        .vcx-registration-area select.wpcf7-select option,
        .vcx-registration-area select.lgx-select option {
            background: <?php echo $brand_color; ?>;
        }
        .vcx-registration-area .wpcf7-form-control.wpcf7-submit,
        .vcx-registration-area .wpcf7-form-control.lgx-submit,
        .vcx-registration-area .wpcf7-submit,
        .vcx-registration-area .lgx-submit {
            background: <?php echo $brand_color3; ?>;
            color: <?php echo $brand_color; ?>;
        }
        .vcx-registration-area .wpcf7-form-control.wpcf7-submit:hover,
        .vcx-registration-area .wpcf7-form-control.lgx-submit:hover,
        .vcx-registration-area .wpcf7-submit:hover,
        .vcx-registration-area .lgx-submit:hover {
            background: <?php echo $brand_color2; ?>;
        }
        .lgx-registration-simple .lgx-inner {
            background: rgba(<?php echo $brand_rgb; ?>, 0.9);
        }
        .lgx-inner-countdown {
            background: <?php echo $brand_color; ?>;
        }
        .lgx-countdown .lgx-days {
            color: #fff200;
        }
        .lgx-countdown .lgx-hr {
            color: #ff8a00;
        }
        .lgx-countdown .lgx-min {
            color: #00b9ff;
        }
        .lgx-countdown .lgx-sec {
            color: #fff;
        }
        .section-countdown-style-squre .lgx-countdown .lgx-days {
            background: <?php echo $brand_color2; ?>;
        }
        .section-countdown-style-squre .lgx-countdown .lgx-hr {
            background: <?php echo $brand_color3; ?>;
        }
        .section-countdown-style-squre .lgx-countdown .lgx-min {
            background: <?php echo $brand_color4; ?>;
        }
        .section-countdown-style-squre .lgx-countdown .lgx-sec {
            background: #8478fd;
        }
        .lgx-gallery-single figure figcaption {
            background: rgba(<?php echo $brand_rgb; ?>, 0.9);
        }
        .lgx-gallery-single:hover figure figcaption .lgx-hover-link .lgx-vertical a:hover {
            color: <?php echo $brand_color3; ?>;
        }
        blockquote.lgx-testi-single .vcx-review-lead {
            color: <?php echo $brand_color; ?>;
        }
        blockquote.lgx-testi-single .author .rate-one i:nth-child(1) {
            color: #ffaf0f;
        }
        blockquote.lgx-testi-single .author .rate-two i:nth-child(1),
        blockquote.lgx-testi-single .author .rate-two i:nth-child(2) {
            color: #ffaf0f;
        }
        blockquote.lgx-testi-single .author .rate-three i:nth-child(1),
        blockquote.lgx-testi-single .author .rate-three i:nth-child(2),
        blockquote.lgx-testi-single .author .rate-three i:nth-child(3) {
            color: #ffaf0f;
        }
        blockquote.lgx-testi-single .author .rate-four i:nth-child(1),
        blockquote.lgx-testi-single .author .rate-four i:nth-child(2),
        blockquote.lgx-testi-single .author .rate-four i:nth-child(3),
        blockquote.lgx-testi-single .author .rate-four i:nth-child(4) {
            color: #ffaf0f;
        }
        blockquote.lgx-testi-single .author .rate-five i:nth-child(1),
        blockquote.lgx-testi-single .author .rate-five i:nth-child(2),
        blockquote.lgx-testi-single .author .rate-five i:nth-child(3),
        blockquote.lgx-testi-single .author .rate-five i:nth-child(4),
        blockquote.lgx-testi-single .author .rate-five i:nth-child(5) {
            color: #ffaf0f;
        }
        .lgx-owltestimonial-box blockquote.lgx-testi-single .author .rate i {
            color: #ffaf0f;
        }
        .lgx-owltestimonial-box blockquote.lgx-testi-single:hover {
            background: <?php echo $brand_color; ?>;
        }
        input:focus,
        input:hover,
        textarea:focus,
        textarea:hover,
        .form-control:focus,
        .form-control:hover,
        .wpcf7-form-control:focus,
        .wpcf7-form-control:hover {
            border: 1px solid <?php echo $brand_color; ?>;
        }
        .wpcf7-submit:focus,
        .wpcf7-submit:hover {
            border: 0 solid <?php echo $brand_color; ?>;
        }
        .lgx-modal {
            background: rgba(<?php echo $brand_rgb; ?>, 0.8);
        }
        .lgx-modal .modal-dialog .modal-content .modal-header .close {
            color: <?php echo $brand_color3; ?>;
        }
        .fa-play {
            background: <?php echo $brand_color; ?>;
        }
        .ripple-block:hover .fa-play,
        .ripple-block:active .fa-play,
        .ripple-block:focus .fa-play {
            color: <?php echo $brand_color3; ?>;
            background: <?php echo $brand_color; ?>;
        }
        .ripple {
            background: rgba(<?php echo $brand_rgb; ?>, 0.5);
        }
        .ripple-block:hover .ripple,
        .ripple-block:active .ripple,
        .ripple-block:focus .ripple {
            background: rgba(<?php echo $brand_rgb; ?>, 0.5);
        }
        .lgx-subscribe-form .form-control {
            background: rgba(<?php echo $brand_rgb; ?>, 0.8);
            color: <?php echo $brand_color; ?>;
        }
        .lgx-subscribe-form .form-control:focus {
            border-color: <?php echo $brand_color; ?>;
        }
        .lgx-subscribe-form .lgx-btn {
            background: <?php echo $brand_color3; ?>;
        }
        .lgx-subscribe-form .lgx-btn:hover {
            background: <?php echo $brand_color2; ?>;
        }
        .lgx-social-footer li {
            color: <?php echo $brand_color; ?> !important;
        }
        .lgx-social-footer li:hover {
            background: <?php echo $brand_color; ?>;
            color: <?php echo $brand_color; ?>;
        }
        .lgx-social-footer:hover > li {
            color: <?php echo $brand_color; ?>;
        }
        .lgx-social-footer:hover > li:hover a {
            background: <?php echo $brand_color2; ?>;
        }
        .lgx-footer-single address i {
            color: <?php echo $brand_color; ?>;
        }
        .lgx-footer-single .lgx-address-info i {
            color: <?php echo $brand_color; ?>;
        }
        .lgx-footer-single .date {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-footer-single .map-link i {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-footer-single .map-link:hover {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-footer-single .opening-time-single span i {
            color: <?php echo $brand_color; ?>;
        }
        .lgx-footer-bottom p span,
        .lgx-footer-bottom p a {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-footer-bottom .lgx-copyright a:hover {
            color: <?php echo $brand_color3; ?>;
        }
        .widget-footer-bottom .lgx-social-footer li:hover {
            background: <?php echo $brand_color; ?>;
            color: <?php echo $brand_color; ?>;
        }
        .widget-footer-bottom .lgx-social-footer:hover > li {
            color: <?php echo $brand_color; ?>;
        }
        .widget-footer-bottom .lgx-social-footer:hover > li:hover a {
            background: <?php echo $brand_color2; ?>;
        }
        .lgx-banner-inner .breadcrumb li a i {
            color: <?php echo $brand_color2; ?>;
        }
        .lgx-banner-inner .breadcrumb .active {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-banner-inner .breadcrumb > li + li:before {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-post-wrapper .lgx-single-speacial:hover .speacial-info .title:hover,
        .lgx-page-wrapper .lgx-single-speacial:hover .speacial-info .title:hover {
            color: <?php echo $brand_color; ?>;
        }
        .lgx-post-wrapper .speaker-info .subtitle {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-post-wrapper article header .subtitle {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-post-wrapper article header .menu-price {
            color: <?php echo $brand_color; ?>;
        }
        .lgx-post-wrapper article header .hits-area .date a i {
            color: <?php echo $brand_color; ?>;
        }
        .lgx-post-wrapper article header .hits-area .date i {
            color: <?php echo $brand_color; ?>;
        }
        .lgx-post-wrapper article footer .title {
            color: <?php echo $brand_color; ?>;
        }
        aside .widget .widget-title {;
            color: <?php echo $brand_color; ?>;
        }
        aside .widget ul li a:hover {
            color: <?php echo $brand_color; ?>;
        }
        aside .widget_tag_cloud .tagcloud a:hover {
            background: <?php echo $brand_color; ?>;
        }
        .widget_product_search .woocommerce-product-search button {
            background: <?php echo $brand_color; ?>;
        }
        .widget_product_search .woocommerce-product-search button:hover {
            background: <?php echo $brand_color; ?>;
        }
        .lgx-page-wrapper article .lgx-single-news .featu<?php echo $brand_color; ?>-post {
            background: <?php echo $brand_color2; ?>;
        }
        ul.page-numbers li .current {
            background: <?php echo $brand_color; ?>;
        }
        ul.page-numbers li:hover {
            background: <?php echo $brand_color; ?>;
        }
        .page-links a:hover,
        .page-links span:hover,
        .page-links a:focus,
        .page-links span:focus,
        .page-links a:active,
        .page-links span:active {
            background: <?php echo $brand_color; ?>;
        }
        .page-links a span {
            color: <?php echo $brand_color; ?>;
        }
        .page-links span,
        .page-links .current {
            background: <?php echo $brand_color; ?>;
        }
        .post-navigation .nav-links .nav-previous:hover,
        .post-navigation .nav-links .nav-next:hover {
            -webkit-box-shadow: 0 14px 26px -12px rgba(50, 51, 51, 0.42), 0 4px 23px 0 rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(50, 51, 51, 0.2);
            box-shadow: 0 14px 26px -12px rgba(50, 51, 51, 0.42), 0 4px 23px 0 rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(50, 51, 51, 0.2);
        }
        .lgx-footer caption {
            color: <?php echo $brand_color3; ?>;
        }
        .lgx-footer select {
            background: <?php echo $brand_color; ?>;
        }
        .lgx-footer .tag-cloud-link:hover {
            background: <?php echo $brand_color; ?>;
        }
        .lgx-footer .lgx-footer-single ul li a:hover {
            color: <?php echo $brand_color3; ?>;
        }
        article footer #comments .edit-link a {
            background: <?php echo $brand_color2; ?>;
        }
        article footer #comments .edit-link:hover a {
            background: <?php echo $brand_color2; ?>;
            -webkit-box-shadow: 0 14px 26px -12px rgba(50, 51, 51, 0.42), 0 4px 23px 0 rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(50, 51, 51, 0.2);
            box-shadow: 0 14px 26px -12px rgba(50, 51, 51, 0.42), 0 4px 23px 0 rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(50, 51, 51, 0.2);
        }
        article footer #comments #respond .comment-notes .requi<?php echo $brand_color; ?> {
            color: <?php echo $brand_color; ?>;
        }
        article footer #comments #respond form label .requi<?php echo $brand_color; ?> {
            color: <?php echo $brand_color; ?>;
        }
        #comments .comment-navigation .nav-links .nav-previous a:hover,
        #comments .comment-navigation .nav-links .nav-next a:hover {
            background: <?php echo $brand_color; ?>;
        }
        .woocommerce #respond input#submit.alt,
        .woocommerce a.button.alt,
        .woocommerce button.button.alt,
        .woocommerce input.button.alt {
            background-color: <?php echo $brand_color; ?>;
        }
        .woocommerce a.checkout-button,
        .woocommerce #payment #place_order {
            background: <?php echo $brand_color2; ?> !important;
        }
        .woocommerce nav.woocommerce-pagination ul.page-numbers li a,
        .woocommerce nav.woocommerce-pagination ul.page-numbers li span {
            color: <?php echo $brand_color; ?>;
        }
        .woocommerce nav.woocommerce-pagination ul.page-numbers li .current {
            background: <?php echo $brand_color; ?>;
        }
        .logged-in .woocommerce-MyAccount-navigation {
            background: <?php echo $brand_color; ?>;
        }
        .logged-in .woocommerce-MyAccount-navigation ul li.is-active {
            background: <?php echo $brand_color3; ?>;
        }
        .woocommerce .form-row input.input-text:hover,
        .woocommerce .form-row input.input-text:focus {
            border: 1px solid <?php echo $brand_color; ?>;
        }  .woocommerce form .form-row .input-text:hover,
           .woocommerce-page form .form-row .input-text:hover,
           .woocommerce form .form-row .input-text:focus,
           .woocommerce-page form .form-row .input-text:focus {
               border: 1px solid <?php echo $brand_color; ?>;
           }
        #add_payment_method table.cart td.actions .coupon .input-text:hover,
        .woocommerce-cart table.cart td.actions .coupon .input-text:hover,
        .woocommerce-checkout table.cart td.actions .coupon .input-text:hover,
        #add_payment_method table.cart td.actions .coupon .input-text:focus,
        .woocommerce-cart table.cart td.actions .coupon .input-text:focus,
        .woocommerce-checkout table.cart td.actions .coupon .input-text:focus {
            border: 1px solid <?php echo $brand_color; ?>;
        }
        .woocommerce div.product p.price,
        .woocommerce div.product span.price {
            color: <?php echo $brand_color3; ?>;
        }
        .woocommerce span.onsale {
            background-color: <?php echo $brand_color2; ?>;
        }
        .woocommerce ul.products li.product .price {
            color: <?php echo $brand_color3; ?>;
        }
        .woocommerce a.added_to_cart {
            color: <?php echo $brand_color2; ?>;
        }
        @media (max-width: 767px) {
            /************** 767 GLOBAl USE***********/
            .lgx-info-circle .info-circle-inner #lgx-countdown .lgx-days {
                color: #fd56a3;
            }

            .lgx-info-circle .info-circle-inner #lgx-countdown .lgx-hr {
                color: #9e1881;
            }

            .lgx-info-circle .info-circle-inner #lgx-countdown .lgx-min {
                color: <?php echo $brand_color4; ?>;
            }

            .lgx-info-circle .info-circle-inner #lgx-countdown .lgx-sec {
                color: #8478fd;
            }
        }

    </style>
<?php }
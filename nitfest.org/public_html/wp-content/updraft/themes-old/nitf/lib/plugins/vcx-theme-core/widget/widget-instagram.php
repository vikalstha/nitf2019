<?php

add_action('widgets_init', 'vcx_theme_core_instagram_load_widgets');
function vcx_theme_core_instagram_load_widgets(){
	register_widget('vcx_theme_core_instagram_Widget');
}


class vcx_theme_core_instagram_Widget extends WP_Widget {

	function __construct(){
		$widget_ops = array('classname' => 'lgx-edu-plus-instagram-widget', 'description' => esc_html__('Instagram Feed','vcx-theme-core') );
		$control_ops = array('id_base' => 'vcx_theme_core_instagram-widget');
		parent::__construct('vcx_theme_core_instagram-widget', esc_html__('Emeet: Instagram Widget','vcx-theme-core'), $widget_ops, $control_ops);
	}

	function widget($args, $instance){
		extract($args); 
		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : esc_html__( 'Recent Posts','vcx-theme-core' );

		/** This filter is documented in wp-includes/default-widgets.php */
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );


		echo $before_widget;
 		if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		} 
		?> 
		<!-- start coding  -->   
			<div id="instafeed" class="lgx-instafeed"></div>
  

			<script>

				(function($) {
				    if($('#instafeed').length){
				    var userFeed = new Instafeed({
				        get: 'user',
				        userId: "<?php echo $instance['u_id']; ?>",
				        clientId: "<?php echo $instance['client_id']; ?>",
				        accessToken: "<?php echo $instance['access_token']; ?>",
				        resolution: 'standard_resolution',
				        template: '<a href="{{link}}" target="_blank" id="{{id}}"><img src="{{image}}" /></a>',
				        sortBy: 'most-recent',
				        limit: <?php echo $instance['limits']; ?>,
				        links: false
				    });
				    userFeed.run();
				}
				})(jQuery);

			</script>
		<!-- start code here -->

		<?php
		echo $after_widget;
	}


	function update($new_instance, $old_instance){
		$instance = $old_instance; 
		$instance['title'] = sanitize_text_field( $new_instance['title']); 
		$instance['u_id'] = sanitize_text_field( $new_instance['u_id']); 
		$instance['client_id'] = sanitize_text_field( $new_instance['client_id']); 
		$instance['access_token'] = sanitize_text_field( $new_instance['access_token']); 
		$instance['limits'] = sanitize_text_field( $new_instance['limits']); 
		return $instance;
	}

	function form($instance)
	{
		$defaults = array( 'title' => '', 'u_id' => '', 'client_id' => '', 'access_token' => '', 'limits' => '6' );
		$instance = wp_parse_args((array) $instance, $defaults); 
		$uniqid = 'id-'.mt_rand(0,10000).mt_rand(50,10000); ?>  
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title','vcx-theme-core'); ?>:</label>
			<input type="text" class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" value="<?php echo $instance['title']; ?>" />
		</p>  
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('u_id')); ?>"><?php esc_html_e('User ID','vcx-theme-core'); ?>:</label>
			<input type="text" class="widefat" id="<?php echo esc_attr($this->get_field_id('u_id')); ?>" name="<?php echo esc_attr($this->get_field_name('u_id')); ?>" value="<?php echo $instance['u_id']; ?>" />
		</p>  
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('client_id')); ?>"><?php esc_html_e('Client ID','vcx-theme-core'); ?>:</label>
			<input type="text" class="widefat" id="<?php echo esc_attr($this->get_field_id('client_id')); ?>" name="<?php echo esc_attr($this->get_field_name('client_id')); ?>" value="<?php echo $instance['client_id']; ?>" />
		</p>  
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('access_token')); ?>"><?php esc_html_e('Access Token','vcx-theme-core'); ?>:</label>
			<input type="text" class="widefat" id="<?php echo esc_attr($this->get_field_id('access_token')); ?>" name="<?php echo esc_attr($this->get_field_name('access_token')); ?>" value="<?php echo $instance['access_token']; ?>" />
		</p>  
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('limits')); ?>"><?php esc_html_e('Limits','vcx-theme-core'); ?>:</label>
			<input type="text" class="widefat" id="<?php echo esc_attr($this->get_field_id('limits')); ?>" name="<?php echo esc_attr($this->get_field_name('limits')); ?>" value="<?php echo $instance['limits']; ?>" />
		</p>  
	<?php
	}
}



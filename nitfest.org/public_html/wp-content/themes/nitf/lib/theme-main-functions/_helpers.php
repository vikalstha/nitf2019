<?php
/**
 * Created by PhpStorm.
 * User: logichunt
 * Time: 1:18 AM
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/************************************************************************
 * //  Custom Excerpt
 *************************************************************************/

function emeet_excerpt($count){
	//$permalink = get_permalink($postid);
	$excerpt = get_the_content();
	$excerpt = strip_tags($excerpt);
	$excerpt = substr($excerpt, 0, $count);
	$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
	//$excerpt = $excerpt.'... <a href="'.$permalink.'">more</a>';
	return $excerpt;
}

/************************************************************************
 * //Custom Excerpt End
 *************************************************************************/


/**=====================================================================
 * wp event point Pagination
 =====================================================================*/

if ( ! function_exists( 'emeet_pagination' ) ){


    function emeet_pagination() {
		// Don't print empty markup if there's only one page.
		if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
			return;
		}
		$paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
		$pagenum_link = html_entity_decode( get_pagenum_link() );
		$query_args   = array();
		$url_parts    = explode( '?', $pagenum_link );
		if ( isset( $url_parts[1] ) ) {
			wp_parse_str( $url_parts[1], $query_args );
		}
		$pagenum_link = remove_query_arg( array_keys( $query_args ), $pagenum_link );
		$pagenum_link = trailingslashit( $pagenum_link ) . '%_%';
		$format  = $GLOBALS['wp_rewrite']->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
		$format .= $GLOBALS['wp_rewrite']->using_permalinks() ? user_trailingslashit( 'page/%#%', 'paged' ) : '?paged=%#%';
		// Set up paginated links.
		$links = paginate_links( array(
			'base'      => $pagenum_link,
			'format'    => $format,
			'total'     => $GLOBALS['wp_query']->max_num_pages,
			'current'   => $paged,
			'mid_size'  => 3,
    		'show_all'  => False,
			'add_args'  => array_map( 'urlencode', $query_args ),
			'prev_text' => '<i class="fa fa-angle-double-left" aria-hidden="true"></i>',
			'next_text' => '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
			'type'      => 'list',
            'before_page_number' => '<b>',
            'after_page_number' => '</b>'
		) );

		if ( $links ) :
		 printf(esc_html__('%s','emeet'),$links);
		endif;
	}
}


if ( ! function_exists( 'themearth_posts_pagination' ) ):

    function themearth_posts_pagination() { ?>
        <?php

        global $query;
        if ($query->max_num_pages > 1) :
            $big   = 999999999;  // Need an unlikely integer
            $items = paginate_links(array(
                'base'      => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                'format'    => '?paged=%#%',
                'prev_next' => TRUE,
                'current'   => max( 1, get_query_var( 'paged' ) ),
                'total'     => $query->max_num_pages,
                'mid_size'  => 3,
                'show_all'  => False,
                'type'      => 'array',
                'prev_text' => '<i class="fa fa-angle-double-left" aria-hidden="true"></i>',
                'next_text' => '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
                'before_page_number' => '<b>',
                'after_page_number' => '</b>'
            ) );

            $pagination = '<ul class="page-numbers"><li>';
            $pagination .= join( "</li><li>", (array) $items );
            $pagination .= "</li></ul>";

            echo $pagination;

        endif;

        return;
    }

endif;


/**=====================================================================
 * wp event post views
 =====================================================================*/
function emeet_getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);


    if($count==0) {
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0";
    }else{
    	return $count;
    }
}
function emeet_setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

/**=====================================================================
 * Comment form field order change
=====================================================================*/

add_action( 'comment_form_after_fields', 'emeet_form_order_textarea' );
add_action( 'comment_form_logged_in_after', 'emeet_form_order_textarea' );

function emeet_form_order_textarea()
{
    echo '<p class="comment-form-comment"><textarea id="comment" name="comment" placeholder="' . esc_attr__( "Enter Comment Here...", "emeet" ) . '" cols="45" rows="8" aria-required="true"></textarea></p>';
}

/**=====================================================================
 *  Comment args change
=====================================================================*/

add_filter( 'comment_form_defaults', 'emeet_comment_form_allowed_tags' );
function emeet_comment_form_allowed_tags( $defaults ) {      $defaults['comment_field'] = '';
	$defaults['label_submit'] =  esc_html__( 'Submit','emeet' );
	return $defaults;

}

/**=====================================================================
 *  BreadCrumb
=====================================================================*/
function emeet_breadcrumb(){


	global $post,$emeet;

	if(isset($emeet['blog-title'])){
		$emeet_blog_title=  $emeet['blog-title'];
	}else{
		$emeet_blog_title=  esc_html__('Blog','emeet');
	}

	if(is_front_page() && is_home()){
		echo esc_html($emeet_blog_title);

	}elseif(is_home() || is_page()){
		if(is_page()){
		    if(''!=get_post_meta($post->ID,'__lgx__page_banner_title',true)){
		        $emeet_ptitle = get_post_meta($post->ID,'__lgx__page_banner_title',true);
			}else{
				$emeet_ptitle =  get_the_title();
			}
		}else{
			$emeet_ptitle =  $emeet_blog_title;

		}
	  printf( esc_html__('%s','emeet'),$emeet_ptitle);

	}elseif(is_single()){
		if(isset($emeet['single-title']) && (''!=$emeet['single-title'])){
			printf(  $emeet['single-title'] );
		}else{
			the_title();
		}
	}elseif(is_search()){
		if(isset($emeet['srch-title']) && (''!=$emeet['srch-title'])){
			printf( $emeet['srch-title'] );
		}else{
			printf( get_search_query()  );
		}
	}elseif(is_category() || is_tag()) {
		if(isset($emeet['archv-title']) && (''!=$emeet['archv-title'])){
			printf(esc_html__('%s','emeet'),$emeet['archv-title']);
		}else{
			single_cat_title("", true);
		}
	}elseif(is_archive()){

 		if ( class_exists('WooCommerce' ) ){
 			if(is_shop() || is_product_category() || is_product_tag() ){
	 			if(isset($emeet['shop-title']) && (''!=$emeet['shop-title'])){
					printf($emeet['shop-title']);
				}else{
	 				woocommerce_page_title();
	 			}
 			}else{ echo get_the_date('F Y'); }
 		}else{
 			if(isset($emeet['archv-title']) && (''!=$emeet['archv-title'])){
				printf($emeet['archv-title']);
			}else{
 				echo get_the_date('F Y');
 			}
		}
	}elseif(is_404()){ esc_html_e('404 Error','emeet');}
	else{


	    the_title();

	}
}



/**=====================================================================
 *  Menu Function
=====================================================================*/
function emeet_main_menu(){
	wp_nav_menu( array(
		'theme_location'    => 'mainmenu',
		'depth'             => 4,
		'container'         => false,
		'menu_id'        	=> 'lgx-nav',
		'menu_class'        => 'nav navbar-nav lgx-nav',
		'fallback_cb'       => 'emeet_menu',
		'walker'			=> new emeet_navwalker()
	));
}

/**=====================================================================
 *  Fall back Menu Function
=====================================================================*/
if(is_user_logged_in()):
	function emeet_menu() {
		?>
	    <ul id="main-menu" class="nav navbar-nav lgx-nav sm sm-blue">
	    	<li><a href="<?php echo esc_url(admin_url('nav-menus.php')); ?>"><?php esc_html_e( 'Add Menu', 'emeet' ); ?></a></li>
		</ul>
		<?php
	}
endif;

/**=====================================================================
 *  User Custom Field
=====================================================================*/
add_action( 'show_user_profile', 'emeet_extra_profile_fields' );
add_action( 'edit_user_profile', 'emeet_extra_profile_fields' );

function emeet_extra_profile_fields( $user ) { ?>

	<h3><?php esc_html_e('Extra profile information','emeet'); ?></h3>

	<table class="form-table">

		<tr>
			<th><label for="user_position"><?php esc_html_e('Position','emeet'); ?></label></th>

			<td>
				<input type="text" name="user_position" id="user_position" value="<?php echo esc_attr( get_the_author_meta( 'user_position', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description"><?php esc_html_e('Please enter your position here.','emeet'); ?></span>
			</td>
		</tr>
		<tr>
			<th><label for="company_name"><?php esc_html_e('Company Name','emeet'); ?></label></th>

			<td>
				<input type="text" name="company_name" id="company_name" value="<?php echo esc_attr( get_the_author_meta( 'company_name', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description"><?php esc_html_e('Please enter your company name here.','emeet'); ?></span>
			</td>
		</tr>
		<tr>
			<th><label for="company_url"><?php esc_html_e('Company Url','emeet'); ?></label></th>

			<td>
				<input type="text" name="company_url" id="company_url" value="<?php echo esc_attr( get_the_author_meta( 'company_url', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description"><?php esc_html_e('Please enter your company url here.','emeet'); ?></span>
			</td>
		</tr>
		<tr>
			<th><label for="follows_url"><?php esc_html_e('Follow','emeet'); ?></label></th>

			<td>
				<input type="text" name="follows_url" id="follows_url" value="<?php echo esc_attr( get_the_author_meta( 'follows_url', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description"><?php esc_html_e('Please enter your social url here.','emeet'); ?></span>
			</td>
		</tr>

	</table>
<?php }


/**=====================================================================
 *  User Custom Field Data SAve
=====================================================================*/
add_action( 'personal_options_update', 'emeet_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'emeet_save_extra_profile_fields' );

function emeet_save_extra_profile_fields( $user_id ) {

	if ( !current_user_can( 'edit_user', $user_id ) )
		return false;

	update_user_meta( $user_id, 'user_position', $_POST['user_position'] );
	update_user_meta( $user_id, 'company_name', $_POST['company_name'] );
	update_user_meta( $user_id, 'company_url', $_POST['company_url'] );
	update_user_meta( $user_id, 'follows_url', $_POST['follows_url'] );
}

/**=====================================================================
 *  hexa to rgb color converter
=====================================================================*/
function emeet_hex2rgb($hex) {
   $hex = str_replace("#", "", $hex);
   if(strlen($hex) == 3) {
      $r = hexdec($hex[0].$hex[0]);
      $g = hexdec($hex[1].$hex[1]);
      $b = hexdec($hex[2].$hex[2]);
   } else {
      $r = hexdec($hex[0].$hex[1]);
      $g = hexdec($hex[2].$hex[3]);
      $b = hexdec($hex[4].$hex[5]);
   }
   return implode(', ',array($r, $g, $b));
}


/**=====================================================================
 *  body class added
=====================================================================*/

add_filter( 'body_class', 'emeet_custom_class' );

function emeet_custom_class( $classes ) {
    if (  is_home() ) {
        $classes[] = 'page page-template';
    }
    return $classes;
}


// remove empty p tag
add_filter( 'the_content', 'emeet_remove_ptag' );
function emeet_remove_ptag( $content ) {
    $array = array(
        '<p>['    => '[',
        ']</p>'   => ']',
        ']<br />' => ']'
    );
    return strtr( $content, $array );
}



function emeet_update_comment_fields_placeholder( $fields ) {

    $commenter = wp_get_current_commenter();
    $req       = get_option( 'require_name_email' );
    $label     = $req ? '*' : ' ' . __( '(optional)', 'emeet' );
    $aria_req  = $req ? "aria-required='true'" : '';

    $fields['author'] =
        '<p class="comment-form-author">
			<input id="author" name="author" type="text" placeholder="' . esc_attr__( "Your Name", "emeet" ) . '" value="' . esc_attr( $commenter['comment_author'] ) .
        '" size="30" ' . $aria_req . ' />
		</p>';

    $fields['email'] =
        '<p class="comment-form-email">
			<input id="email" name="email" type="email" placeholder="' . esc_attr__( "Your Email", "emeet" ) . '" value="' . esc_attr( $commenter['comment_author_email'] ) .
        '" size="30" ' . $aria_req . ' />
		</p>';

    $fields['url'] =
        '<p class="comment-form-url">
			<input id="url" name="url" type="url"  placeholder="' . esc_attr__( "Website", "emeet" ) . '" value="' . esc_attr( $commenter['comment_author_url'] ) .
        '" size="30" />
			</p>';

    return $fields;
}
add_filter( 'comment_form_default_fields', 'emeet_update_comment_fields_placeholder' );



// define the comment_form_submit_button callback

function emeet_filter_comment_form_submit_button( $submit_button, $args ) {
    // make filter magic happen here...
    $submit_before = '<div class="comment-form-submit">';
    $submit_after = '</div>';
    return $submit_before . $submit_button . $submit_after;
};

// add the filter
add_filter( 'comment_form_submit_button', 'emeet_filter_comment_form_submit_button', 10, 2 );
<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

add_shortcode( 'lgx_travel_info', 'lgx_travel_info_function');


/**
 * Short Code
 * @param $atts
 * @return string
 */

function lgx_travel_info_function($atts, $content = null ) {

    extract(shortcode_atts(array(
        'info_style' 	=>	'default',
        'info_title' 	=>	'Venue',
        'post_title' 	=>	'Docklands Convention Centre 58 Wurundjeri Way Dablin, 3000',
        'travel_image' 	=>	'',
        'info_alignment' 	=>	'center',
        'info_content' 	=>	'Etiam ultricies gravida metus. Phasellus consectetur, metus sit amet facilisis varius, diam ex tincidunt eros, pulvinar fringilla massa justo eu justo. Suspendisse hendrerit laoreet diam ac accumsan. Sed et ipsum elit. Vestibulum nunc ligula, scelerisque vel tristique et, semper ac lacus.',
        'is_container' => true,
        'color_type'    =>'default'
    ), $atts));

    $image_url = '';
    if(!empty ($atts['travel_image'])) {
        $img = wp_get_attachment_image_src($atts['travel_image']);
        $image_url = $img[0];
    }

    $info_title = (!empty($info_title)) ? '<h3 class="title">'.$info_title.'</h3>' : '';
    $post_title = (!empty($post_title)) ? '<p class="info">'.$post_title.'</p>' : '';
    $info_content = (!empty($info_content)) ? '<p class="vcx-travel-content">'.$info_content.'</p>' : '';


    $output = '<div class="lgx-travelinfo-single vcx-travel-'.$info_style.'  text-'.$info_alignment.' travel-color-'.$color_type.'">
                       <div class="lgx-travel-inner">
                          '.(!empty($image_url) ? '<img  class="travel-info-thumb" src="'.esc_url($image_url).'" alt="'.esc_html__('Location', 'vcx-theme-core').'"/>' : '') .'
                            '.$info_title.'
                            '.$post_title.'
                            '.$info_content.'
                        </div>
                    </div>';

    return $output;

}


/**
 * Visual Composer
 */

if (class_exists('WPBakeryVisualComposerAbstract')) {
    vc_map(array(
        "name" => esc_html__("Travel Info", 'vcx-theme-core'),
        "base" => "lgx_travel_info",
        "class" => "",
        "description" => esc_html__("Display all of the Travel Information", 'vcx-theme-core'),
        "category" => esc_html__('Emeet', 'vcx-theme-core'),
        "params" => array(
            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("Alignment", 'vcx-theme-core'),
                "param_name" 	=> "info_alignment",
                "value" 		=> array('Center'=>'center','Left'=>'left','Right'=>'right'),
            ),
            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("Style", 'vcx-theme-core'),
                "param_name" 	=> "info_style",
                "value" 		=> array('Default'=>'default','Left Right'=>'left-right'),
            ),

            array(
                "type" 			=> "attach_image",
                "heading" 		=> esc_html__("Upload Image", "lgx-themential"),
                "param_name" 	=> "travel_image",
                "value" 		=> "",
            ),
            array(
                "type" 			=> "textfield",
                "heading" 		=> esc_html__("Title", "lgx-themential"),
                "param_name" 	=> "info_title",
                "value" 		=> "Venue",
            ),

            array(
                "type" 			=> "textfield",
                "heading" 		=> esc_html__("Post Title", "lgx-themential"),
                "param_name" 	=> "post_title",
                "value" 		=> "Docklands Convention Centre 58 Wurundjeri Way Dablin, 3000",
            ),

            array(
                "type"       => "textarea",
                "holder"        => "div",
                "heading" 		=> esc_html__("Content", "lgx-themential"),
                "param_name" 	=> "info_content",
                "value" 		=> "Etiam ultricies gravida metus. Phasellus consectetur, metus sit amet facilisis varius, diam ex tincidunt eros, pulvinar fringilla massa justo eu justo. Suspendisse hendrerit laoreet diam ac accumsan. Sed et ipsum elit. Vestibulum nunc ligula, scelerisque vel tristique et, semper ac lacus.",
                // 'admin_label' => true,
            ),

            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("Content Color", 'vcx-theme-core'),
                "param_name" 	=> "color_type",
                "value" 		=> array('Default'=>'default','White'=>'white'),
            )

        )

    ));
}
<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Emeet
 */

get_header(); ?>
<?php get_template_part('header/post-banner'); ?>
    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            <div id="lgx-post-wrapper" class="lgx-post-wrapper lgx-single-item">
                <div class="container">
                    <?php
                    while ( have_posts() ) : the_post();

                        get_template_part( 'post-format/content-single', 'speaker'  );

                    endwhile; // End of the loop.
                    ?>
                </div>
            </div>
        </main><!-- #main -->
    </div><!-- #primary -->

<?php get_footer();

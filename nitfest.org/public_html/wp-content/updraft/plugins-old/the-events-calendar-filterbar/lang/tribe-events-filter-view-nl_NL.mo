��    ?                            *  	   9     C     K     X  
   j     u     z     �  	   �     �     �     �     �     �  4   �  �   �  
   �     �     �     �     �     �  
   �     �     �     �     �     �          "  
   '     2     8     E     S     \     c     r     �     �     �     �     �     �  �   �     k     t  	   y  �   �          "     .  &   =  +   d     �     �  	   �  -   �  0   �  0   		  �   :	  !   .
     P
     `
     g
     p
     �
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
     �
  I   �
  �   F  
   0     ;     C     c     y     �     �     �     �     �     �     �  '   �                     &     3     A  	   J     T     g     �     �  	   �     �     �     �  �   �  	   �     �  	   �  �   �     s     {     �  5   �     �     �  	   �     �     �     �        "%s" or empty or set to zero Active Filters Afternoon All Day Autocomplete Available Filters Checkboxes City Closed Collapse Filters Cost (%s) Country Day Dropdown Evening Event Category Events are considered free when cost field is: %s %s Expand an active filter to edit the label and choose from a subset of input types (dropdown, select, range slider, checkbox and radio). Filter Bar Filters Filters Default State Filters Layout Free Friday Horizontal Monday Morning Narrow Your Results Night No items match Only when set to zero Open Organizers Other Range Slider Reset Filters Saturday Select Select an Item Show Advanced Filters Show Filters State/Province Submit Sunday Tags The Events Calendar The settings below allow you to enable or disable front-end event filters. Uncheck the box to hide the filter. Drag and drop active filters to re-arrange them. Thursday Time Title: %s To begin using The Events Calendar: Filter Bar, please install the latest version of <a href="%s" class="thickbox" title="%s">The Events Calendar</a>. Tuesday Type: %s %s Type: %s %s %s Used as the identifier for free Events Used as the identifier for free EventsFree Venues Vertical Wednesday additional fields filter logic settingLogic: additional fields filter logic settingMatch all additional fields filter logic settingMatch any PO-Revision-Date: 2017-01-27 16:27:27+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/1.0-alpha-1100
Project-Id-Version: Filter Bar
 "%s" of leeg of ingesteld als nul Actieve filters Middag Hele dag Automatisch aanvullen Beschikbare filters Checkboxen Stad Gesloten Klap filters in Kosten (%s) Land Dag Dropdown Avond Evenementencategorie Evenementen worden beschouwd als gratis wanneer het kosten veld is: %s %s Je kan een actief filter vergroten met het pijltje aan de rechterkant. Je kan hier het label (de benaming) van het filter wijzigen en kiezen uit diverse invoeropties (bijvoorbeeld via een dropdown, keuzevakjes, radiobuttons en meer). Filter Bar Filters Standaard status van de filters Layout van de filters Gratis Vrijdag Horizontaal Maandag Ochtend Beperk de resultaten Nacht Geen items komen overeen Alleen wanneer deze is ingesteld op nul Open Organisatoren Overige informatie Marge slider Reset filters Zaterdag Selecteer Selecteer een item Toon uitgebreide filters Toon filters Staat/provincie Toevoegen Zondag Tags The Events Calendar De instellingen die je hieronder vindt, maken het mogelijk om filters in de frontend in- of uit te schakelen. Schakel het vinkje uit om het filter te verbergen. Je kan ook actieve filters verslepen om de volgorde te veranderen. Donderdag Tijd Titel: %s Om gebruik te kunnen maken van The Events Calendar: Filter Bar, installeer alsjeblieft de laatste versie van <a href="%s" class="thickbox" title="%s">The Events Calendar</a>. Dinsdag Type: %s %s Type: %s %s %s Gebruikt als de identificatie voor gratis evenementen Gratis Locaties Verticaal Woensdag Logica: Overeenkomen met alle Overeenkomen met elke 
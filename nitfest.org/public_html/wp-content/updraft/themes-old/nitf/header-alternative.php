<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<?php global $emeet_options; ?>

<head>

    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php if ( ! ( function_exists( 'has_site_icon' ) && has_site_icon() ) ) : ?>
        <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon.ico" type="image/x-icon"/>
    <?php endif; ?>
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>


<!-- Pre Loader -->
<?php if(isset($emeet_options['pre_loader_en']) && $emeet_options['pre_loader_en']==1):  ?>
    <div id="vcx-preloader"></div>
<?php endif;  ?>


<div id="lgxpage<?php the_ID() ?>" class="lgxsite lgxpage<?php the_ID()?>">




    <div id="content" class="site-content site-content-alternative">
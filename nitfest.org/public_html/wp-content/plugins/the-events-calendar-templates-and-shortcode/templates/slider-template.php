<?php 
   $size='large';
    $ev_post_img='';
    $feat_img_url = wp_get_attachment_image_src( get_post_thumbnail_id(),$size);
    if($feat_img_url[0]){
        $ev_post_img=$feat_img_url[0];
    }else{
        $ev_post_img=ECT_PLUGIN_URL."images/event-template-bg.png";
        }

if($template=="slider-view"){

 $events_html.='<div id="event-'.get_the_ID().'" class="'.$event_type.' '.$post_parent.'"><div  class="ect-slider-content ect-slider-clearfix">';
     $bg_styles="background-image:url('$ev_post_img');background-size:cover;background-position:center center;";
     
    $events_html.='<div class="ect-slider-img ect-slider-pull-right" style="'.$bg_styles.'"></div>';

 $events_html.='<div class="ect-slider-con pull-left">
            <div class="ect-slider-con-outer">
                <div class="ect-slider-middle-con">
                    <div class="ect-slider-con-header">';

 $events_html.='<h3 class="slider-ev-day">'.tribe_get_start_date( null, false, 'd' ).'</h3>
            <div class="ect-slider-date">
            <h5 class="slider-ev-mo">'.tribe_get_start_date( null, false, 'F' ).'</h5>
            <span>'.$ev_time.'</span>
             </div></div>
<h3 class="ect-slider-day-define"><span class="ev-mo">'.tribe_get_start_date( null, false, 'l' ).'</span></h3>';

 $events_html.='<div class="ect-slider-main-content">
<h2 class="slider-list-title">'.$event_title.'</h2>';

      if (tribe_has_venue()) {
            $events_html.=$venue_details_html;
            }else{
            $events_html.='';
            }  
  $events_html.=$ev_cost ;
$events_html.=$event_content;
 $events_html.='</div>
     </div></div>
   </div></div></div>';

   }elseif($template=="carousel-view"){

    $events_html.='<div id="event-'.get_the_ID().'" class="'.$event_type.' '.$post_parent.'">
    <div class="ect-carousel-block">
        <div class="ect-carousel-block-inner">
            <div class="ect-carousel-image-box">
                <div class="ect-carousel-image-outer">
                    <a class="" href="'.tribe_get_event_link().'" title="'.get_the_title() .'" rel="bookmark"><img src="'.$ev_post_img.'" alt=""></a>
                </div>';

  $events_html.='<div class="ect-carousel-date-caption">
                  <h3 class="slider-ev-day">'.tribe_get_start_date( null, false, 'd' ).'</h3>
                  <div class="mnth">
                      <p>'.tribe_get_start_date( null, false, 'F' ).'</p>
                      <p>'.tribe_get_start_date( null, false, 'Y' ).'</p>

                  </div>
                  <div class="ect-slider-tm">'.$ev_time.'</div>
              </div>
          </div>';

   $events_html.='<div class="ect-carousel-event-caption">
                <h3>'.$event_title.'</h3>';
          if (tribe_has_venue()) {
            $events_html.=$venue_details_html;
            }else{
            $events_html.='';
            }
              $events_html.=$ev_cost ;
    $events_html.='<a class="url" href="'.tribe_get_event_link().'" title="'.get_the_title() .'" rel="bookmark">'.__('Find out more »','the-events-calendar').'</a>';
    $events_html.='</div></div></div></div> ';

   }
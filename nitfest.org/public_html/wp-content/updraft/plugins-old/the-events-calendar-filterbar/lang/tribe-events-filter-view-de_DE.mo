��    A      $      ,      ,     -     J  	   Y     c     k     x  
   �     �     �     �  	   �     �     �     �     �     �  4   �  �        �  
   �     �     �     �     �     �  
   �                    '     -     <     R  
   W     b     h     u     �     �     �     �     �     �     �     �     �     �       �        �     �  	   �  �   �     d     l     x  &   �  +   �     �     �  	   �  -   �  0   "	  0   S	  �   �	  #   x
     �
  
   �
     �
     �
     �
  
   �
     �
  	   �
       
             #     '     5     ;  Q   S  �   �     M     l     y     �     �     �     �  
   �     �     �     �     �      �          2     ;     H     X     _     t     |     �     �     �  +   �     �  
                    �   3  
   �       	     �        �     �     �  >   �  	   "     ,     ?     H     Q     X     d   "%s" or empty or set to zero Active Filters Afternoon All Day Autocomplete Available Filters Checkboxes City Closed Collapse Filters Cost (%s) Country Day Dropdown Evening Event Category Events are considered free when cost field is: %s %s Expand an active filter to edit the label and choose from a subset of input types (dropdown, select, range slider, checkbox and radio). Featured Events Filter Bar Filters Filters Default State Filters Layout Free Friday Horizontal Monday Morning Narrow Your Results Night No items match Only when set to zero Open Organizers Other Range Slider Reset Filters Saturday Select Select an Item Show Advanced Filters Show Filters Show featured events only State/Province Submit Sunday Tags The Events Calendar The settings below allow you to enable or disable front-end event filters. Uncheck the box to hide the filter. Drag and drop active filters to re-arrange them. Thursday Time Title: %s To begin using The Events Calendar: Filter Bar, please install the latest version of <a href="%s" class="thickbox" title="%s">The Events Calendar</a>. Tuesday Type: %s %s Type: %s %s %s Used as the identifier for free Events Used as the identifier for free EventsFree Venues Vertical Wednesday additional fields filter logic settingLogic: additional fields filter logic settingMatch all additional fields filter logic settingMatch any PO-Revision-Date: 2017-03-23 00:29:14+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/1.0-alpha-1100
Project-Id-Version: Filter Bar
 "%s" oder leer oder auf Null setzen Aktive Filter Nachmittag Ganztags Autovervollständigen Verfügbare Filter Checkboxen Stadt Verbergen Filter verbergen Preis (%s) Land Tag Aufklappmenü Abend Veranstaltungskategorie Veranstaltungen werden als kostenlos angezeigt, wenn das Eintrittsfeld %s %s ist. Den aktiven Filter aufklappen, um den Titel zu bearbeiten und einen geeigneten Eingabetyp zu wählen (Dropdown Menü, Auswahl Menü, Slider, Checkbox und Radiobutton). Hervorgehobene Veranstaltungen Filterleiste Filter Filter Standardauswahl Filter Darstellung gratis Freitag horizontal Montag Morgen Ergebnisse einschränken Nacht Keine passenden Treffer gefunden nur wenn es "0" (Null) enthält Anzeigen Veranstalter Weitere Angaben Slider Filter zurücksetzen Samstag Auswahl Wähle ein Objekt Erweiterte Filter anzeigen Filter anzeigen Nur hervorgehobene Veranstaltungen anzeigen Bundesland/Provinz/Kanton Abschicken Sonntag Tags The Events Calendar Die folgenden Einstellungen aktivieren und deaktivieren die Veranstaltungsfilter im Frontend. Den Haken entfernen, um die Filter zu verbergen. Die Reihenfolge kann per Drag & Drop angepasst werden. Donnerstag Uhrzeit Titel: %s Um The Events Calendar: Filter Bar nutzen zu können, installiere bitte die neuste Version von <a href="%s" class="thickbox" title="%s">The Events Calendar</a>.  Dienstag Auswahl: %s %s Verwende: %s %s %s Wird als Kennzeichen für kostenlose Veranstaltungen verwendet Kostenlos Veranstaltungsorte vertikal Mittwoch Logik: Matche alle Matche beliebige 
<!DOCTYPE html>

<html <?php language_attributes(); ?>>



<?php global $emeet_options; ?>



<head>



    <meta charset="<?php bloginfo( 'charset' ); ?>">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php if ( ! ( function_exists( 'has_site_icon' ) && has_site_icon() ) ) : ?>

        <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon.ico" type="image/x-icon"/>

    <?php endif; ?>

    <link rel="profile" href="http://gmpg.org/xfn/11">

    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">



    <?php wp_head(); ?>



</head>



<body <?php body_class(); ?>>



<?php



$emeet_logo = '';



$emeet_opt = new LgxFrameworkOpt();

$emeet_logo = $emeet_opt->emeet_logo();



$emeet_addcls_header = '';

if ( is_user_logged_in() ) {

    $emeet_addcls_header = 'hdrtop';

}



// Menu Variations From Theme Options



// Menu Style



$emeet_nav_option_style  = $emeet_options['main-menu-style'];

$emeet_nav_meta_style    = get_post_meta(get_the_ID(),'__vcx__menu_style',true);

$emeet_nav_style        = ( ($emeet_nav_meta_style == 'theme-opt') ? $emeet_nav_option_style : $emeet_nav_meta_style );

$emeet_nav_style        = ( !empty($emeet_nav_style)  ? $emeet_nav_style : 'default');





// Menu Width



$emeet_nav_option_width  = $emeet_options['main-menu-width'];

$emeet_nav_meta_width    = get_post_meta(get_the_ID(),'__vcx__menu_width',true);

$emeet_nave_width        = ( ($emeet_nav_meta_width == 'theme-opt') ? $emeet_nav_option_width : $emeet_nav_meta_width );

$emeet_nave_width        = ( !empty($emeet_nave_width)  ? $emeet_nave_width : 'lgx-container');





// Menu Position Type

$emeet_nav_option_type   = $emeet_options['main-menu-type'];

$emeet_nav_meta_type     = get_post_meta(get_the_ID(),'__vcx__menu_type',true);

$emeet_nave_type        = ( ($emeet_nav_meta_type == 'theme-opt') ? $emeet_nav_option_type : $emeet_nav_meta_type );

$emeet_nave_type        = ( !empty($emeet_nave_type)  ? $emeet_nave_type : 'fixed');





// Menu Color

$emeet_nav_option_color  = $emeet_options['main-menu-color'];

$emeet_nav_meta_color = get_post_meta(get_the_ID(),'__vcx__menu_color',true);

$emeet_nav_color        = ( ($emeet_nav_meta_color == 'theme-opt') ? $emeet_nav_option_color : $emeet_nav_meta_color );

$emeet_nav_color        = ( !empty($emeet_nav_color)  ? $emeet_nav_color : 'white');





?>



<!-- Pre Loader -->

<?php if(isset($emeet_options['pre_loader_en']) && $emeet_options['pre_loader_en']==1):  ?>

    <div id="vcx-preloader"></div>

<?php endif;  ?>





<div id="lgxpage<?php the_ID() ?>" class="lgxsite lgxpage<?php the_ID()?>">





    <!--HEADER-->

    <header>

    <div id="lgx-header" class="lgx-header">

        <div class="lgx-header-position lgx-header-position-white lgx-header-position-fixed lgx-header-background-tr"> 

            <!--lgx-header-position-fixed lgx-header-position-white lgx-header-fixed-container lgx-header-fixed-container-gap lgx-header-position-white-->

            <div class="lgx-container-fluid"> <!--lgx-container-->

                <nav class="navbar navbar-default lgx-navbar">

                    <div class="navbar-header">

                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">

                            <span class="sr-only">Toggle navigation</span>

                            <span class="icon-bar"></span>

                            <span class="icon-bar"></span>

                            <span class="icon-bar"></span>

                        </button>

                        <div class="lgx-logo">

                            <a href="/" class="lgx-scroll">

                                <img src="https://nitfest.org/assets/img/logo.png" alt="NIT Festival Logo"/>

                            </a>

                        </div>

                    </div>

                    <div id="navbar" class="navbar-collapse collapse">

                        <?php emeet_main_menu(); ?>

                    </div><!--/.nav-collapse -->

                </nav>

            </div>

            <!-- //.CONTAINER -->

        </div>

    </div>

</header>



 <!--HEADER END-->





    <!--BANNER-->



    <!--BANNER END-->







    <div id="content" class="site-content">
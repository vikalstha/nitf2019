<?php

/**
 * Social Area Widget
 *
 * @link       http://logichunt.com
 * @since      1.0.0
 *
 * @package    Lgx_Social_Area
 * @subpackage Lgx_Social_Area/widget
 *
 * @author     LogicHunt <info.logichunt@gmail.com>
 */


class Vcx_Social_Widget extends WP_Widget {


	public function __construct() {
		parent::__construct('Vcx_Social_Widget', 'Emeet: Social Area', array('description' => __('Widget for display Social Area ', 'vcx-theme-core')));
	}



	public function form($instance) {

		// Default Value
		$title           = isset($instance['title']) ? $instance['title'] : __('Social Connection', 'vcx-theme-core');
		$sub_title       = isset($instance['sub_title']) ? $instance['sub_title'] : __('You should connect social area for Any update', 'vcx-theme-core');
		$url_facebook    = isset($instance['url_facebook']) ? $instance['url_facebook'] : '';
		$url_twitter     = isset($instance['url_twitter']) ? $instance['url_twitter'] : '';
		$url_instagram   = isset($instance['url_instagram']) ? $instance['url_instagram'] : '';
		$url_vimeo       = isset($instance['url_vimeo']) ? $instance['url_vimeo'] : '';
		$url_behance     = isset($instance['url_behance']) ? $instance['url_behance'] : '';
		$url_dribbble    = isset($instance['url_dribbble']) ? $instance['url_dribbble'] : '';
		$url_youtube     = isset($instance['url_youtube']) ? $instance['url_youtube'] : '';



		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'vcx-theme-core'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
		</p>

        <p>
            <label for="<?php echo $this->get_field_id('sub_title'); ?>"><?php _e('Lead Text:', 'vcx-theme-core'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('sub_title'); ?>" name="<?php echo $this->get_field_name('sub_title'); ?>" type="text" value="<?php echo esc_attr($sub_title); ?>" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('url_facebook'); ?>"><?php _e('Facebook URL:', 'vcx-theme-core'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('url_facebook'); ?>" name="<?php echo $this->get_field_name('url_facebook'); ?>" type="text" value="<?php echo esc_attr($url_facebook); ?>" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('url_twitter'); ?>"><?php _e('Twitter URL:', 'vcx-theme-core'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('url_twitter'); ?>" name="<?php echo $this->get_field_name('url_twitter'); ?>" type="text" value="<?php echo esc_attr($url_twitter); ?>" />
        </p>


        <p>
            <label for="<?php echo $this->get_field_id('url_instagram'); ?>"><?php _e('Instagram URL:', 'vcx-theme-core'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('url_instagram'); ?>" name="<?php echo $this->get_field_name('url_instagram'); ?>" type="text" value="<?php echo esc_attr($url_instagram); ?>" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('url_vimeo'); ?>"><?php _e('Vimeo URL:', 'vcx-theme-core'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('url_vimeo'); ?>" name="<?php echo $this->get_field_name('url_vimeo'); ?>" type="text" value="<?php echo esc_attr($url_vimeo); ?>" />
        </p>


        <p>
            <label for="<?php echo $this->get_field_id('url_behance'); ?>"><?php _e('Behance URL:', 'vcx-theme-core'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('url_behance'); ?>" name="<?php echo $this->get_field_name('url_behance'); ?>" type="text" value="<?php echo esc_attr($url_behance); ?>" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('url_dribbble'); ?>"><?php _e('Dribbble URL:', 'vcx-theme-core'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('url_dribbble'); ?>" name="<?php echo $this->get_field_name('url_dribbble'); ?>" type="text" value="<?php echo esc_attr($url_dribbble); ?>" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('url_youtube'); ?>"><?php _e('Youtube URL:', 'vcx-theme-core'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('url_youtube'); ?>" name="<?php echo $this->get_field_name('url_youtube'); ?>" type="text" value="<?php echo esc_attr($url_youtube); ?>" />
        </p>


        <?php
	}



	public function update($new_instance, $old_instance) {

		// Set Instance
		$instance                   = array();
		$instance['title']          = strip_tags($new_instance['title']);
		$instance['sub_title']      = strip_tags($new_instance['sub_title']);
		$instance['url_facebook']   = esc_url($new_instance['url_facebook']);
		$instance['url_twitter']    = esc_url($new_instance['url_twitter']);
		$instance['url_instagram']  = esc_url($new_instance['url_instagram']);
		$instance['url_vimeo']      = esc_url($new_instance['url_vimeo']);
		$instance['url_behance']    = esc_url($new_instance['url_behance']);
		$instance['url_dribbble']   = esc_url($new_instance['url_dribbble']);
		$instance['url_youtube']    = esc_url($new_instance['url_youtube']);

		return $instance;
	}


	public function widget($args, $instance) {
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);

		$sub_title = !empty($instance['sub_title']) ? '<p class="text footer-lead">'.$instance['sub_title'] .'</p>': '';

		// Widget Output
		echo $before_widget;

		echo (!empty($title) ? $before_title . $title . $after_title : '');

        $social_list = '<ul class="list-inline lgx-social-footer">';
        $social_list .= (!empty($instance['url_facebook']))  ?  '<li><a href="'.esc_url($instance['url_facebook']).'"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>': '';
        $social_list .= (!empty($instance['url_twitter']))   ?  '<li><a href="'.esc_url($instance['url_twitter']).'"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>': '';
        $social_list .= (!empty($instance['url_instagram'])) ?  '<li><a href="'.esc_url($instance['url_instagram']).'"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>': '';
        $social_list .= (!empty($instance['url_vimeo']))     ?  '<li><a href="'.esc_url($instance['url_vimeo']).'"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>': '';
        $social_list .= (!empty($instance['url_behance']))  ?  '<li><a href="'.esc_url($instance['url_behance']).'"><i class="fa fa-behance" aria-hidden="true"></i></a></li>': '';
        $social_list .= (!empty($instance['url_dribbble']))  ?  '<li><a href="'.esc_url($instance['url_dribbble']).'"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>': '';
        $social_list .= (!empty($instance['url_youtube']))  ?  '<li><a href="'.esc_url($instance['url_youtube']).'"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>': '';

        $social_list .= '</ul>';

       echo $sub_title . $social_list;

		echo $after_widget;
	}
}



// Register Foo_Widget widget
add_action( 'widgets_init', function() { register_widget( 'Vcx_Social_Widget' ); } );
<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

add_shortcode( 'lgx_sp_image', 'lgx_sp_image_function');


/**
 * Short Code
 * @param $atts
 * @return string
 */

function lgx_sp_image_function($atts, $content = null ) {

    extract(shortcode_atts(array(
        'sp_image' 	=>	'',
    ), $atts));

    $image_url = '';
    if(!empty ($atts['sp_image'])) {
        $img = wp_get_attachment_image_src($atts['sp_image'], 'full');
        $image_url = $img[0];
    }


    $output = !empty($image_url) ? '<div class="lgx-about-img-sp vcx-special-image"><img  class="vcx-sp-thumb" src="'.esc_url($image_url).'" alt="'.esc_html__('Special Image', 'vcx-theme-core').'"/></div>' : '';

    return $output;

}


/**
 * Visual Composer
 */

if (class_exists('WPBakeryVisualComposerAbstract')) {
    vc_map(array(
        "name" => esc_html__("Special Single Image", 'vcx-theme-core'),
        "base" => "lgx_sp_image",
        "class" => "",
        "description" => esc_html__("Display Special  Image", 'vcx-theme-core'),
        "category" => esc_html__('Emeet', 'vcx-theme-core'),
        "params" => array(

            array(
                "type" 			=> "attach_image",
                "heading" 		=> esc_html__("Upload Image", "lgx-themential"),
                "param_name" 	=> "sp_image",
                "value" 		=> "",
            )
        )

    ));
}
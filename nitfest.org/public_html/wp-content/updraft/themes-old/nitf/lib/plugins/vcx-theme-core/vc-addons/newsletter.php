<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

add_shortcode( 'vcx_news_letter', 'vcx_news_letter_function');


/**
 * Short Code
 * @param $atts
 * @return string
 */

function vcx_news_letter_function($atts, $content = null ) {

    extract(shortcode_atts(array(
        'mw_id' 	=>	'',
        'style'     => 'default',
        'title'     => 'Join Newsletter'

    ), $atts));

    $mc4wp_form = do_shortcode('[mc4wp_form id="'.intval($mw_id).'"]');

    $title = (!empty($title)) ? '<h3 class="subscriber-title">'.esc_html($title).'</h3>' : '';



    $output = '<div class="lgx-subscriber-area lgx-subs-style-'.$style.'">
                    '.$title.'
                    <div class="lgx-subscribe-form">
                        '.$mc4wp_form.'                               
                    </div> 
                </div>';
    return $output;

}




/**
 * Visual Composer
 */

if (class_exists('WPBakeryVisualComposerAbstract')) {
    vc_map(array(
        "name" => esc_html__("News Letter", 'vcx-theme-core'),
        "base" => "vcx_news_letter",
        "class" => "",
        "description" => esc_html__("Display News Letter", 'vcx-theme-core'),
        "category" => esc_html__('Emeet', 'vcx-theme-core'),
        "params" => array(
            array(
                "type"          => "textfield",
                "heading"       => esc_html__("Form ID", "vcx-theme-core"),
                "description"   => esc_html__("Please create form in MailChimp for WP and insert from ID here.", 'vcx-theme-core'),
                "param_name"    => "mw_id",
                "value"         => "",
                "admin_label"   => true,
            ),

            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("Style", 'vcx-theme-core'),
                "param_name" 	=> "style",
                "value" 		=> array('Default'=>'default', 'Inline'=>'inline'),
            ),

            array(
                "type"       => "textfield",
                "heading" 		=> esc_html__("Title", "vcx-theme-core"),
                "description"   => esc_html__("Add Title Here", 'vcx-theme-core'),
                "param_name" 	=> "title",
                "value" 		=> "Join Newsletter",
            ),

        )

    ));
}
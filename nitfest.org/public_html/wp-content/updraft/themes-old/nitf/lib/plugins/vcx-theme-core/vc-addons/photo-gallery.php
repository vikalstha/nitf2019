<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

add_shortcode( 'vcx_photo_gallery', 'vcx_photo_gallery_function');


/**
 * Short Code
 * @param $atts
 * @return string
 */

function vcx_photo_gallery_function($atts) {
    $number 	= -1;
    $order_by	= 'date';
    $order		= 'DESC';

    extract(shortcode_atts(array(
        'gallery_type'  => 'list',
        'number' 		=> -1,
        'order_by'		=> 'date',
        'order'			=> 'DESC',
        'style'			=> 'one',
        'gap_type'		=> 'nogap',
        'row_item'		=> '3',
        'sp_image' 	=>	'',
    ), $atts));

    global $post;

    // Basic Query
    $args = array(
        'post_type'      => array( 'gallery' ),
        'post_status'		=> 'publish',
        'posts_per_page'	=> esc_attr($number),
        'order'				=> $order,
        'orderby'			=> $order_by
    );

    $image_url = plugins_url() .'/vcx-theme-core/assets/img/icon.png';
    if(!empty ($atts['sp_image'])) {
        $img = wp_get_attachment_image_src($atts['sp_image'], 'full');
        $image_url = $img[0];
    }



    $data = new WP_Query($args);
    ob_start(); ?>




    <div id="lgx-photo-gallery" class="lgx-gallery lgx-gallery-<?php echo esc_attr($gap_type); ?>">
        <div class="row">
            <div class="col-xs-12">
                 <div  <?php echo ($gallery_type == 'slider') ? 'class="lgx-owlgallery" ' : 'class="lgx-gallery-area lgx-gallery-col-'.esc_attr($row_item).' lgx-gallery-area-'.esc_attr($style).'"' ; ?> >
                    <?php
                    if ( $data->have_posts() ) :
                        while ( $data->have_posts() ) :
                            $data->the_post();
                            $id = $post->ID;

                            $thumb_url = '';
                            if ( has_post_thumbnail( $post->ID ) ) {
                                $thumb_url2 = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ),true);
                                $thumb_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'eplano-gallery-medium', true);
                                $thumb_url = $thumb_url[0];
                                $thumb_url2 = $thumb_url2[0];
                            }
                            ?>

                            <div class="item">
                                <div  class="lgx-gallery-single" >
                                    <figure>
                                        <img title="<?php echo get_the_title(); ?>" src="<?php echo $thumb_url; ?>" alt="<?php echo get_the_title(); ?>"/>
                                        <figcaption class="lgx-figcaption">
                                            <div class="lgx-hover-link">
                                                <div class="lgx-vertical">
                                                    <a class="lgx-single-photo" title="<?php echo get_the_title(); ?>" href="<?php echo $thumb_url2; ?>">
                                                        <img src="<?php echo esc_url($image_url) ;?>" alt="<?php echo get_the_title(); ?>">
                                                    </a>
                                                </div>
                                            </div>
                                        </figcaption>
                                    </figure>
                                </div>
                            </div>
                        <?php
                        endwhile;
                    endif;
                    wp_reset_postdata();// Restore original Post Data
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?php
    return ob_get_clean();
}



/**
 * Visual Composer
 */

if (class_exists('WPBakeryVisualComposerAbstract')) {
    vc_map(array(
        "name" => esc_html__("Photo Gallery", 'vcx-theme-core'),
        "base" => "vcx_photo_gallery",
        "class" => "",
        "description" => esc_html__("Display Photo Gallery", 'vcx-theme-core'),
        "category" => esc_html__('Emeet', 'vcx-theme-core'),
        "params" => array(
            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("Gallery Type", 'vcx-theme-core'),
                "param_name" 	=> "gallery_type",
                "value" 		=> array('List'=>'list','Slider'=>'slider'),
            ),

            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("List Style", 'vcx-theme-core'),
                "param_name" 	=> "style",
                "value" 		=> array(
                        'One'=>'one',
                        'Two'=>'two',
                        'Three'=>'three',
                        'Four'=>'four',
                        'Five'=>'five',
                        'Six'=>'six',
                        'Inline'=>'inline',
                    ),
            ),

            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("Gap Type", 'vcx-theme-core'),
                "param_name" 	=> "gap_type",
                "value" 		=> array(
                    'No Gap'=>'nogap',
                    'Gap'=>'gap',
                ),
            ),

            array(
                "type" 			=> "textfield",
                "heading" 		=> esc_html__("Number of items", 'vcx-theme-core'),
                "param_name" 	=> "number",
                "value" 		=> -1,
            ),

            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("OderBy", 'vcx-theme-core'),
                "param_name" 	=> "order_by",
                "value" 		=> array('Select'=>'','Date'=>'date','Title'=>'title','Modified'=>'modified','Author'=>'author','Random'=>'rand'),
            ),

            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("Order", 'vcx-theme-core'),
                "param_name" 	=> "order",
                "value" 		=> array('Select'=>'','DESC'=>'DESC','ASC'=>'ASC'),
            ),

            array(
                "type" 			=> "attach_image",
                "heading" 		=> esc_html__("Upload Icon Image", "lgx-themential"),
                "param_name" 	=> "sp_image",
                "value" 		=> "",
            ),

            array(
                "type" 			=> "dropdown",
                "heading" 		=> esc_html__("List Items Per Row ", 'vcx-theme-core'),
                "param_name" 	=> "row_item",
                "value" 		=> array('Three'=>'3','Four'=>'4','Five'=>'5',),
            ),


        )

    ));
}
<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Emeet
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('lgx-post-loop'); ?>>

    <div class="lgx-single-news single-loop-inner">

        <?php if ( has_post_thumbnail() ) : ?>
        <figure>
            <div class="lgx-featured-wrap lgx-post-img">
                <a href="<?php the_permalink(); ?>">
                    <?php the_post_thumbnail('emeet-blog'); ?>
                </a>
            </div>
            </figure>
        <?php endif; ?>

        <div class="single-news-info">
            <div class="meta-wrapper">
                <span class="vcx-news-date"> <?php echo date_i18n(get_option('date_format'), false, false);; ?> </span>
                <span class="vcx-post-author"><?php esc_html_e('By', 'emeet') ?> <?php the_author(); ?></span>
            </div>
            <h2 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            <div class="vcx-excerpt"><?php the_excerpt(); ?></div>
            <a class="lgx-btn lgx-btn-white lgx-btn-sm" href="<?php the_permalink(); ?>"><span><?php esc_html_e('Read More', 'emeet') ?></span></a>
        </div>
        <?php  if ( is_sticky() ) {
            printf( '<span class="featured-post">%s</span>', esc_html__( 'Featured', 'emeet' ) );
        }; ?>
    </div><!--// Single -->



</article>